<?php 
require_once ('wp-load.php');
require_once ('wp-admin/includes/admin.php');

$post_ids = range(66417, 70019);
$query = new WP_Query([
    'post_type' => 'hemeroteca',
    'post__in' => $post_ids,
    'posts_per_page' => -1
]);

while ($query->have_posts()) : $query->the_post();
    $defaults = array(
        'post_title' => get_the_title(),
        'post_status' => 'publish',
        'post_type' => 'attachment',
        'post_mime_type' => 'image/jpeg'
    );
    $featuredId = wp_insert_post($defaults);
    $url =  'images/' . get_the_date('Y/m/') . get_the_title() . '.jpg';
    update_post_meta( $featuredId, 'amazonS3_info', ['bucket' => 'el9nou', 'key' => $url, 'region' => 'eu-west-1'] );
    update_post_meta( get_the_ID(), '_thumbnail_id', $featuredId );

    $url = 'statics/portades_velles/' . get_the_date("Ymd") . '001.pdf';
    update_post_meta( get_the_ID(), 'portada_vella', $url );

endwhile;

die();