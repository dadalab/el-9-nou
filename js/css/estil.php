<?php header("Content-type: text/css"); ?>
html,body{
	padding:0;
	margin:0;
	height:100%;
	width:100%;
	background-color:#fff;
	//background-image: url(<?php echo $camiweb_sencer ?>../imatges/fons.jpg);
	background-repeat: repeat-x;
	font-family:arial;
	font-size:9pt;
	color:#000;
}
h1,h2,h3,h4{
	padding:0;
    margin:0;
}

#error {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #FF0000;
	text-decoration: none;
	padding-left:5px;
}
/**** DESPLEGABLE ****/
#eleccions, #eleccions:visited, #eleccions:hover {
  cursor: pointer;
  display: inline-block;
  position: relative;
  font-size: 16px;
  color: #000000;
  width: 240px;
  height: 40px;
  line-height: 16px;
    margin-bottom:16px;
    margin-right:6px;
    margin-top:2px;
    padding:10px;
}

/**** CAPES ****/
#capa_1 {
	position:absolute;
	width:990px;
	height:109px;
	z-index:1;
}

/***** BOTONS *****/
/* menu inferior cap�alera */
#botons, .botons{ 
	height:23px;
	background-color:#4d4d4d;
	color:#fff;
	font-size:11px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:10px;
	padding-right:10px;
	text-decoration: none;
	margin-left: 10px;
	border-right-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #fff;
	border-right-color: #fff;
	border-bottom-color: #fff;
	border-left-color: #fff;
	line-height: 16px;
	padding-top: 3px;
	padding-bottom: 4px;
}
#botons:hover, #botons:focus, .botons:hover, .botons:focus{
	background-color:#ddd;
	color:#000;
}
.botons_sec{ 
	height:23px;
	background-color:#fff;
	color:#000;
	font-size:12px;
	font-style: normal;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:10px;
	padding-right:10px;
	text-decoration: none;
	margin-left: 10px;
	border-right-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #fff;
	border-right-color: #fff;
	border-bottom-color: #fff;
	border-left-color: #fff;
	line-height: 16px;
	padding-top: 3px;
	padding-bottom: 4px;
}
/* menu superior cap�alera */
#botons2,.botons2{
    height:22px;
	color:#fff;
	font-size:11px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: bottom;
	padding-left:10px;
	padding-right:10px;
	text-decoration: none;
	margin-left: 10px;
	border-top-width: 0px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #fff;
	border-right-color: #fff;
	border-bottom-color: #fff;
	border-left-color: #fff;
	line-height: 16px;
	padding-top: 3px;
	padding-bottom: 3px;
}

#botons2:hover, #botons2:focus,.botons2:hover,.botons2:focus{
	background-color:#ddd;
	color:#000;
}
#botons2_sec,.botons2_sec{
    height:22px;
	color:#000;
	background-color:#fff;
	font-size:11px;
	font-style: normal;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: bottom;
	padding-left:10px;
	padding-right:10px;
	text-decoration: none;
	margin-left: 10px;
	border-top-width: 0px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #fff;
	border-right-color: #fff;
	border-bottom-color: #fff;
	border-left-color: #fff;
	line-height: 16px;
	padding-top: 3px;
	padding-bottom: 3px;
}
/* bot� cerca cap�alera */
#botcerca, .botcerca{ 
	height:17px;
	background-color:#4d4d4d;
	color:#fff;
	font-size:11px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-right: 10px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #ddd;
	border-right-color: #ddd;
	border-bottom-color: #ddd;
	border-left-color: #ddd;
	line-height: 12px;
	padding-top: 0px;
	padding-bottom: 5px;
}
#botcerca:hover, #botcerca:focus, .botcerca:hover, .botcerca:focus{
	background-color:#DDDDDD;
	color:#000;
}
#bot_modifica{
	height:16px;
	border:1px solid #ddd;
	background-color:#4d4d4d;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
}
#bot_modifica:hover, #bot_modifica:focus{
	background-color:#FF9900;
}
#bottanca{ 
	height:15px;
	background-color:#fff;
	color:#4d4d4d;
	font-size:11px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	border-top-width: 0px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #ddd;
	border-right-color: #000;
	border-bottom-color: #ddd;
	border-left-color: #000;
	line-height: 12px;
	padding-top: 1px;
	padding-bottom: 3px;
}
#bottanca:hover, #bottanca:focus{
	background-color:#DDDDDD;
	color:#000;
}
#botons_pub{
	width:16px;
	height:16px;
	border:1px solid #ddd;
	background-color:#4d4d4d;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:0px;
	text-decoration: none;
}
#botons_pub:hover, #botons_pub:focus{
	width:16px;
	height:16px;
	border:1px solid #ddd;
	background-color:#FF9900;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:0px;
	text-decoration: none;
}
#bot_form{
	height:20px;
	border:1px solid #ddd;
	background-color:#4d4d4d;
    text-decoration:none;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
    padding-right:10px;
    padding-left:10px;
    padding-top:3px;
    padding-bottom:3px;
    line-height:12px;
}
#bot_form:hover, #bot_form:focus{
	background-color:#FF9900;
}
#bot_form_a a{
	height:20px;
	border:1px solid #ddd;
	background-color:#4d4d4d;
    text-decoration:none;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
    padding-right:10px;
    padding-left:10px;
    padding-top:3px;
    padding-bottom:3px;
    line-height:12px;
}
#bot_form_a a:hover, #bot_form_a a:focus, #boto_galeta:hover{
	background-color:#FF9900;
}
#boto_galeta{
	height:20px;
	border:1px solid #ddd;
	background-color:#4d4d4d;
    text-decoration:none;
	color:#fff;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
    padding-right:10px;
    padding-left:10px;
    padding-top:3px;
    padding-bottom:3px;
    line-height:12px;
}

#diari_osona {
  background-image:url('../imatges/diari_osona1.gif');
  background: url('../imatges/diari_osona1.gif') no-repeat center top;
  width:200px;
  height:70px;
  display:block;
  border : none;
  text-align:center;
  color : transparent;
  cursor: pointer;
}
#diari_osona:hover {
  border-width:1px;
  border-style:solid;
  border-color:#000000;
}
#diari_valles {
  background-image:url('../imatges/diari_valles1.gif');
  background: url('../imatges/diari_valles1.gif') no-repeat center top;
  width:200px;
  height:70px;
  display:block;
  border : none;
  text-align:center;
  color : transparent;
  cursor: pointer;
}
#diari_valles:hover {
  border-width:1px;
  border-style:solid;
  border-color:#000000;
}
#diari_intro {
	padding:25;
    text-align:center;
    width:300px;
}
/****** TEXTOS *******/
.text_data {
	font-family: Arial, Helvetica, sans-serif;
	text-align: right;
	font-size:12px;
	font-style: normal;
	font-weight: normal;
	padding-right:10px;
    color: #fff;
}
.text_credit1 {
	height:16px;
	color:#000;
	font-size:9px;
	font-style: normal;
	font-weight: normal;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:10px;
	padding-right:10px;
	text-decoration: none;
	margin-left: 0px;
	border-top-width: 0px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #fff;
	border-right-color: #494949;
	border-bottom-color: #fff;
	border-left-color: #494949;
	line-height: 16px;
	padding-top: 2px;
	padding-bottom: 2px;
}
.text_credit1:hover, .text_credit1:focus, .text_credit2:hover, .text_credit2:focus {
	color:#545454;
}
.text_credit2 {
	height:16px;
	color:#000;
	font-size:9px;
	font-style: normal;
	font-weight: normal;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-right:10px;
	padding-left:10px;
	text-decoration: none;
	margin-left: 0px;
	border-top-width: 0px;
	border-left-width: 1px;
	border-bottom-width: 0px;
	border-right-width: 0px;
	border-top-style: none;
	border-left-style: solid;
	border-bottom-style: none;
	border-right-style: none;
	border-top-color: #fff;
	border-left-color: #494949;
	border-bottom-color: #fff;
	border-right-color: #494949;
	line-height: 16px;
	padding-top: 2px;
	padding-bottom: 2px;
}

.text_redaccio, #text_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-bottom: 4px;
	padding-top: 10px;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.text_redaccio2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
}
.text_raco {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #FFFFFF;
	text-align: left;
}
.avantitol_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
    font-weight: normal;
	color: #545454;
	text-align: left;
	padding-top: 0px;
	padding-bottom: 5px;
}
.subtitol_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
    font-weight: bold;
	color: #545454;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 10px;
}
.titol_r1 a, .titol_r1{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (28+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.titol_r1_gran a, .titol_r1_gran{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (36+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 10px;
	padding-bottom: 10px;
	font-weight: bold;
	text-decoration: none;
}
.titol_r2 a,.titol_r2{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (20+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.titol_r3 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (20+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.titol_r4 a, .titol_r4{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 5px;
	margin-bottom: 10px;
	font-weight: bold;
	text-decoration: none;
}
.titol_r1 a:hover, .titol_r1 a:focus, .titol_r1_gran a:hover, .titol_r1_gran a:focus,
.titol_r2 a:hover, .titol_r2 a:focus, .titol_r3 a:hover, .titol_r3 a:focus,
.titol_r4 a:hover, .titol_r4 a:focus, .opinio_titol2 a:hover, .opinio_titol2 a:focus {
	color: #bc0044;
}
.titol_raco {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
	color: #ffffff;
	text-align: left;
	padding-top: 0px;
	padding-bottom: 10px;
	font-weight: bold;
	text-decoration: none;
}
.titol_raco2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
	color: #ffffff;
	text-align: left;
	padding-top: 10px;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.titol_raco2:hover, .titol_raco2:focus {
    color: #b4b4b4;
}
.datahora_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (11+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 0px;
}
.firma_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: italic;
	color: #000000;
	text-align: left;
	padding-right: 4px;
	font-weight: bold;
	display: inline;
}
.entradeta_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
    font-weight: normal;
	color: #545454;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 0px;
	line-height: <?php echo (20+$tam_txt) ?>px;
}
.link_noticia {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (11+$tam_txt) ?>px;
	font-style: normal;
	color: #bc0044;
	text-align: left;
	margin-top: 8px;
	padding-bottom: 0px;
	line-height: <?php echo (11+$tam_txt) ?>px;
}
.link_noticia:hover, .link_noticia:focus {
	color:#545454;
}
.peufoto_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (11+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
	text-align: left;
	padding-top: 2px;
	padding-left: 5px;
	padding-right: 5px;
	padding-bottom: 8px;
	border: 0px;
}
.autorfoto_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (8+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
	text-align: left;
	margin-top: 3px;
	padding-top: 0px;
	padding-left: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	border-top-style: solid;
	border-top-width: 1px;
	font-weight: bold;
	text-transform: uppercase;
}
.complement1 {
	border:0;
	background-color:#4d4d4d;
	text-align: left;
	vertical-align: middle;
	margin-top: 1px;
	padding-left: 5px;
	padding-right: 5px;
	padding-top: 3px;
	padding-bottom: 3px;
}
.complement1 a{
	color:#fff;
	font-size:<?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	vertical-align: middle;
	text-decoration: none;
    background-color:#4d4d4d;
}
.complement1 a:hover{
	color:#ccc;
}
.linkvideo_redaccio {
	height:9px;
	color:#000000;
	font-size:<?php echo (9+$tam_txt) ?>px;
	font-style: normal;
	font-weight: bold;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-right: 2px;
	border-top-width: 0px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #fff;
	border-right-color: #494949;
	border-bottom-color: #fff;
	border-left-color: #494949;
	line-height: <?php echo (9+$tam_txt) ?>px;
	padding-top: 0px;
	padding-bottom: 0px;
}
.linkvideo_redaccio:hover, .linkvideo_redaccio:focus {
	color:#bc0044;
}
.data_redaccio {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (10+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: right;
	font-weight: bold;
}
.text_video1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
	color: #FFFFFF;
	text-align: center;
	font-weight: bold;
	text-decoration: none;
}
.text_video1:hover {
	color:#CCCCCC;
}

.tvcarta_index a{
	background-color:#333399;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #4d4d4d;
}
.tvcarta_index a:hover, .tvcarta_index a:focus {
	background-color:#ddd;
	color:#000;
}
.tvcarta_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:11px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #4d4d4d;
}
.tvcarta_dvd a{
	background-color:#000;
	color:#FF9900;
	font-size:10px;
	font-style: normal;
	font-weight: bold;
	padding-top: 2px;
	padding-bottom: 0px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	text-decoration: none;
    float: left;
	width: 634px;
	height: 16px;
    border: 1px solid #000;
}
.tvcarta_dvd a:hover {
	background-color:#ddd;
	color:#000;	
}
.fotogaleria_index a{
	background-color:#4d4d4d;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #4d4d4d;
}
.fotogaleria_index a:hover, .fotogaleria_index a:focus, .fotogaleria_indexfoto a:hover,
.fotogaleria_indexfoto a:focus, .fotogaleria_boto a:hover, .fotogaleria_boto a:focus, .fotogaleria_boto2 a:hover, .fotogaleria_boto2 a:focus,
 .el9tv_index a:hover, .el9tv_index a:focus, .opinio_index a:hover, .opinio_index a:focus {
	background-color:#ddd;
	color:#000;
}
.fotogaleria_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:11px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #4d4d4d;
}
.fotogaleria_titol{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (20+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.fotogaleria_titol2{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
	text-align: left;
	padding-left: 4px;
	font-weight: bold;
	text-decoration: none;
}
.fotogaleria_foto {
	background-color:#4d4d4d;
	cursor:pointer;
	vertical-align: middle;
	border: 1px solid #4d4d4d;
	margin: 2px;
}
.fotogaleria_foto:hover, .fotogaleria_foto:focus {
	background-color:#4d4d4d;
	border: 3px solid #bc0044;
	margin: 0px;
}
.fotogaleria_indexfoto a{
	background-color:#3c5773;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin: 2px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 15px;
	border: 1px solid #fff;
}
.fotogaleria_indexfoto2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:11px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin: 2px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 15px;
	border: 1px solid #4d4d4d;
}
.fotogaleria_boto a{
	background-color:#3c5773;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin: 4px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 100px;
	border: 1px solid #fff;
	font-weight: bold;
}
.fotogaleria_boto2 a{
	background-color:#3c5773;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin: 4px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	border: 1px solid #fff;
	font-weight: bold;
}
.agexpo_titol{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (20+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-bottom: 5px;
    padding-left: 5px;
	font-weight: bold;
	text-decoration: none;
}
.agexpo_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #4d4d4d;
	padding-bottom: 6px;
	padding-top: 6px;
    margin-left: 5px;
    margin-right: 5px;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.classificats_index a{
	background-color:#3c5773;
	color:#fff;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 140px;
	border: 1px solid #4d4d4d;
}
.classificats_index a:hover, .classificats_index a:focus {
	background-color:#ddd;
	color:#000;
}
.classificats_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:12px;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 140px;
	border: 1px solid #4d4d4d;
}
.classificats_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #4d4d4d;
	padding-bottom: 6px;
	padding-top: 6px;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.classificats_avis {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
	color: #FF0000;
	text-align: left;
	padding-bottom: 6px;
	padding-top: 6px;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.el9tv_titol {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.el9tv_titol2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
	text-align: left;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.el9tv_titol3 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 5px;
	margin-bottom: 10px;
	font-weight: bold;
	text-decoration: none;
}
.el9tv_titol3 a:hover, .el9tv_titol3 a:focus {
	color: #bc0044;
}
#el9tv_titol4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (16+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
	text-align: left;
	padding: 5px;
	font-weight: bold;
	text-decoration: none;
    background-color: #000;
}
.el9tv_foto {
	background-color:#4d4d4d;
	cursor:pointer;
	vertical-align: middle;
	border: 1px solid #000;
	margin: 2px;
}
.el9tv_foto:hover, .el9tv_foto:focus {
	background-color:#4d4d4d;
	border: 3px solid #fff;
	margin: 0px;
}
.el9tv_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000;
	text-align: left;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.el9tv_textblanc {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (10+$tam_txt) ?>px;
	font-style: normal;
	font-weight: bold;
	color: #fff;
	text-align: left;
	padding-bottom: 8px;
	padding-left: 2px;
	line-height: <?php echo (18+$tam_txt) ?>px;
}
.el9tv_index a{
	background-color:#3c5773;
	color:#fff;
	font-size: 11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
	margin: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 15px;
	border: 1px solid #000;
}
.el9tv_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size: 11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
	margin: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 15px;
	border: 1px solid #4d4d4d;
}
.text_index_o a{
	background-color:#9f0038;
	color:#fff;
	font-size:<?php echo (11+$tam_txt) ?>px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 140px;
	border: 1px solid #4d4d4d;
}
.text_index_v a{
	background-color:#007450;
	color:#fff;
	font-size:<?php echo (11+$tam_txt) ?>px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 140px;
	border: 1px solid #4d4d4d;
}
.text_index_o a:hover, .text_index_o a:focus, .text_index_v a:hover, .text_index_v a:focus {
	background-color:#ddd;
	color:#000;
}
.contactes1 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #4d4d4d;
	text-align: left;
	padding-top: 5px;
	margin-bottom: 10px;
	text-decoration: none;
	line-height: <?php echo (20+$tam_txt) ?>px;
	cursor:pointer;
}
.contactes1 a:hover, .contactes1 a:focus {
	color: #000;
}
.rss1 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-decoration: none;
	font-weight: bold;
	cursor: pointer;
	vertical-align: middle;
	display: inline;
}
.rss1 a:hover, .rss1 a:focus {
	color: #000;
}
.rss2 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	color: #3c5773;
	text-decoration: none;
	cursor:pointer;
	margin-right:10px;
	padding-left:8px;
	padding-right:8px;
	padding-top: 8px;
	padding-bottom: 1px;
	border: 1px solid #4d4d4d;
	vertical-align: middle;
	line-height: <?php echo (55+$tam_txt) ?>px;
}
.rss2 a:hover, .rss2 a:focus {
	color: #000;
}

.promo_tipus1 a {
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (15+$tam_txt) ?>px;
    text-decoration: none;
	cursor:pointer;
 	padding-left:5px;
	padding-right:5px;
    vertical-align: middle;
}
.promo_tipus1 a:hover, .promo_tipus1 a:focus {
	color: #4d4d4d;
}
.promo_titol1 {
	color: #fff;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (14+$tam_txt) ?>px;

}
.promo_titol1 a {
	color: #000;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (14+$tam_txt) ?>px;
    text-decoration: none;
	cursor: pointer;
    font-weight: bold;
}
.promo_titol1 a:hover, .promo_titol1 a:focus {
	color: #4d4d4d;
}
.promo_text1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (11+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
}
.promo_preu1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (24+$tam_txt) ?>px;
	font-style: normal;
	color: #FF9900;
	text-align: left;
    font-weight: bold;
    padding-top: 3px;
}
.promo_titol2 {
	color: #FF9900;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (24+$tam_txt) ?>px;
    font-weight: bold;
    padding-top: 10px;
    padding-bottom: 10px;
}
.promo_text2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-bottom: 4px;
	padding-top: 10px;
    line-height: 20px;
}
.promo_club {
	color:#000;
	font-size:9px;
	font-style: normal;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
}
.promo_preu2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: normal;
	color: #FF9900;
    font-weight: bold;
}
.ofe_titol1 {
	color: #FFF;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (14+$tam_txt) ?>px;
    text-decoration: none;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 3px;
    padding-bottom: 3px;
}
.ofe_titol2 {
	color: #FF9900;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (22+$tam_txt) ?>px;
    font-weight: bold;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    
}
.ofe_text2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
    padding-left: 10px;
    padding-right: 10px;
	padding-bottom: 4px;
	padding-top: 0px;
    line-height: 15px;
}
.ofe_dia {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
    padding-left: 10px;
    padding-right: 10px;
	padding-bottom: 4px;
	padding-top: 10px;
    line-height: 14px;
    font-weight: bold;
}
.ofe_base {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #FFFFFF;
	text-align: left;
    padding-left: 10px;
    padding-right: 10px;
	padding-bottom: 4px;
	padding-top: 4px;
    line-height: 20px;
}
a#ofe_entrar{
	background-color: #b20070;
	color: #fff;
	font-size:12px;
	font-weight:bold;
	line-height:25px;
	text-align: center;
	padding-top: 5px;
	padding-bottom: 4px;
    padding-left: 3px;
    padding-right: 3px;
	width:170px;
	margin-top:15px;
    margin-bottom:15px;
	text-decoration:none;
	display:block;
	vertical-align:middle;
    
    cursor:pointer;
}
a#ofe_entrar:hover{
	background-color: #f90;
}
.ofe_import {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (9+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: right;
	padding-bottom: 4px;
	padding-top: 4px;
    padding-right: 10px;
    line-height: 20px;
}

.reg_titol {
	color: #FF9900;
	font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
	font-size: <?php echo (26+$tam_txt) ?>px;
    font-weight: bold;
    padding-bottom: 10px;
    
}
.reg_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-top: 10px;
    line-height: 15px;
}
.reg_base {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
    background-color: #000;
	color: #FFFFFF;
	text-align: left;
    padding-left: 10px;
    padding-right: 10px;
	padding-bottom: 4px;
	padding-top: 5px;
    line-height: 20px;
	margin-top:15px;
    margin-bottom:5px;
	text-decoration:none;
	display:block;
	vertical-align:middle;
}

.titol_cerca a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
	text-align: left;
	padding-top: 2px;
	text-decoration: none;
}
.titol_cerca a:hover, .titol_cerca a:focus {
	color: #bc0044;
}
.titol_cerca2{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (13+$tam_txt) ?>px;
	font-style: normal;
	color: #3c5773;
    background-color:#FFFF00;
	text-align: left;
	padding-top: 2px;
	text-decoration: none;
}
.data_cerca {
	color:#000;
	font-size:10px;
	font-style: normal;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
    font-weight: bold;
}
.ctl_index a{
	background-color:#3c5773;
	color:#fff;
	font-size:12px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 199px;
	border: 1px solid #4d4d4d;
}
.ctl_index a:hover, .ctl_index a:focus {
	background-color:#ddd;
	color:#000;
}
.ctl_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:12px;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 199px;
	border: 1px solid #4d4d4d;
}
.publi_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #fff;
}
.publi_text2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000;
}

.espai_obert1 a{
	background-color:#3c5773;
	color:#fff;
	font-size: 16px;
    font-weight: bold;
	cursor: pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
	vertical-align: middle;
	text-decoration: none;
	padding-top: 5px;
	padding-bottom: 6px;
    float: left;
    width: 635px;
	border: 1px solid #4d4d4d;
}

.espai_obert1 a:hover, .espai_obert1 a:focus {
	background-color:#ddd;
	color:#000;
}
.opinio_index a{
	background-color:#CCCC99;
	color:#000;
	font-size:11px;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #999966;
}

.opinio_index2 {
	background-color:#fff;
	color:#4d4d4d;
	font-size:11px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	vertical-align: middle;
	padding-left:5px;
	padding-right:5px;
	text-decoration: none;
	margin-top: 1px;
	padding-top: 2px;
	padding-bottom: 3px;
	float: left;
	width: 170px;
	border: 1px solid #999966;
}
.opinio_titol{
	font-family: Georgia, "Times New Roman", Times,serif;
	font-size: <?php echo (20+$tam_txt) ?>px;
	font-style: italic;
	color: #999966;
	text-align: left;
	padding-bottom: 5px;
	text-decoration: none;
}
.opinio_autor{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	color: #000;
	text-align: left;
	padding-bottom: 5px;
	font-weight: bold;
	text-decoration: none;
}
.opinio_carrec{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	color: #000000;
	text-align: left;
	padding-right: 4px;
	display: inline;
}
.opinio_titol2 a{
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (14+($tam_txt*2)) ?>px;
	font-style: normal;
	color: #999966;
	text-align: left;
	line-height: 26px;
	margin-bottom: 1px;
	font-weight: bold;
	text-decoration: none;
}
.opinio_titol3 {
	font-family: Georgia, "Times New Roman", Times,serif;
	font-size: <?php echo (14+$tam_txt) ?>px;
	font-style: italic;
	color: #666633;
	text-align: left;
	padding-top: 10px;
	padding-bottom: 5px;
	text-decoration: none;
}
.opinio_lateral {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (12+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-top: 10px;
	padding-bottom: 5px;
	text-decoration: none;
}
.opinio_lateral2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (10+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	padding-top: 7px;
	text-decoration: none;
}
.opinio_data {
	font-family: Arial, Helvetica, sans-serif;
	font-size: <?php echo (10+$tam_txt) ?>px;
	font-style: normal;
	color: #000000;
	text-align: left;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #4d4d4d;
	padding-bottom: 3px;
	padding-top: 2px;
	line-height: <?php echo (12+$tam_txt) ?>px;
}
/****** FORMULARIS *******/

.formulari form input, #formulari_form_input, .formulari_form_input, #camp_data, #camp_data2{
	border: none;
	background-color:#dddddd;
	padding-left:2px;
	padding-right:2px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
    cursor: text;
}

.formulari form textarea, #formulari_form_textarea,.formulari_form_textarea{
	border:none;
	background-color:#dddddd;
	padding-left:2px;
	padding-right:2px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
    cursor: text;
}

.formulari form select, #formulari_form_select, .formulari_form_select{
	border:none;
	background-color:#dddddd;
	padding-left:2px;
	padding-right:2px;
	font-family: Arial, Helvetica, sans-serif;
    font-size: 16px;
    color: #000000;
    width: 240px;
    height: 40px;
}

.formulari form label, #formulari_form_label, .formulari_form_label{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: bold;
	color: #000000;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
}
.formulari form label2, #formulari_form_label2, .formulari_form_label2{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: bold;
	color: #000000;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
	text-align: right;
}
.formulari form label3, #formulari_form_label3, .formulari_form_label3{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: bold;
	color: #000000;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
	text-align: left;
}
.formulari form label4, #formulari_form_label4, .formulari_form_label4{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: bold;
	color: #ffffff;
	padding-left:3px;
	padding-right:3px;
	text-decoration: none;
}
/*** GRAFISMES ***/
#filet_ver,.filet_ver {
	background-color: #FFF;
	background-image: url(../imatges/filet_ver.gif);
	background-repeat: repeat-y;
}
#filet_hor,.filet_hor {
	background-color: #FFF;
	background-image: url(../imatges/filet_hor.gif);
	background-repeat: repeat-x;
}
#filet_hor_punts,.filet_hor_punts {
	background-color: #FFF;
	background-image: url(../imatges/filet_hor_punts2.gif);
	background-repeat: repeat-x;
}
#filet_hor_punts3,.filet_hor_punts3 {
	background-color: #FFF;
	background-image: url(../imatges/filet_hor_punts3.gif);
	background-repeat: repeat-x;
}
/*** CEL.LA AMB ESPAI BLANC RESERVAT ***/
#reservat_blanc,.reservat_blanc {
	padding: 10px;
}
#columna_blanc_dret {
	padding-right:10px;
	vertical-align: top;
}
#columna_blanc_dret2 {
	padding-right:10px;
	vertical-align: top;
}
#taula_basica,.taula_basica {
	margin: 0px;
	padding: 0px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	width: 100%;
}
#taula_moduls {
    margin:10px 0 0 10px;font-size:11px;font-weight:normal;
    border-color: #fff;
}

#foto_flotant table,.foto_flotant table {
  float: right;
  margin: 0 0 10px 10px;
  padding-top: 10px;
}
#foto_flotant2 table {
  float: right;
  margin: 10px 0 5px 20px;
  padding-top: 0px;
}
#foto_blanc_superior {
  padding-top: 8px;
}
/*** ELEMENTS PUBLICITARIS ****/
.publicitat {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 8px;
	font-style: normal;
	color: #666666;
	text-align: left;
	font-weight: bold;
	padding-left: 5px;
	vertical-align: middle;
	background-color:#CCCCCC;
}
.publicitat2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-style: normal;
	color: #FFFFFF;
	text-align: left;
	font-weight: bold;
	padding-left: 0px;
	vertical-align: middle;
	text-align: center;
	background-color:#666666;
}
.publicitat3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-style: normal;
	color: #FFFFFF;
	text-align: left;
	font-weight: bold;
	padding-left: 0px;
	vertical-align: middle;
	text-align: center;
	background-color:#FFFFFF;
}
/*** ESTILS MOOTOLS ****/
#acordio {
	margin:0px 0px 15px auto;
	color:#000000;
}
#acordio2 {
	margin:0px 0px 15px auto;
	color:#000000;
}
div.toggler {
	cursor: pointer;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	background: #ff9900;
	margin-bottom: 3px;
	padding: 4px 10px 3px;
	text-decoration: none;
	font-weight: bold;
}
div.toggler2 {
	cursor: pointer;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	background: #cccc99;
	margin-bottom: 3px;
	padding: 4px 10px 3px;
	text-decoration: none;
	font-weight: bold;
}
div.element p, div.element h4 {
	margin:0px;
	padding-top:4px;
}
div.element2 p, div.element2 h4 {
	margin:0px;
	padding-top:4px;
}

#vertical_slide {
	background: #D0C8C8;
	color: #8A7575;
	padding: 0px;
	font-weight: bold;
}
h3.section {
	margin-top: 1em;
}

#capa_fons1 {
	position: fixed;
	z-index:240;
	top: 0px;
	left: 0px;
	background-color:#000;
	filter:alpha(opacity=50);
	-moz-opacity: 0.50;
	opacity: 0.50;
	height:100%;
	width:100%;
	border: 0px;
	visibility: visible;
	text-align: center;
	vertical-align: middle;
}
#capa2_fons1 {
	position: fixed;
	z-index:245;
	top: 0px;
	left: 0px;
	height:100%;
	width:100%;
	border: 0px;
	visibility: visible;
	text-align: center;
	vertical-align: middle;
}

* html #capa_fons1 { /*\*/position: absolute; top: expression((0 + (ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); left: expression((0 + (ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }
#capa_fons > #capa_fons1 { position: fixed; top: 0px; left: 0px; visibility: visible;}
* html #capa2_fons1 { /*\*/position: absolute; top: expression((0 + (ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); left: expression((0 + (ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }
#capa2_fons > #capa2_fons1 { position: fixed; top: 0px; left: 0px; visibility: visible;}

#capa_foto1,.capa_foto1 {
	position: relative;
	z-index:10;
	top: 0px;
	left: 0px;
	border: 0px;
	visibility: visible;
}
#capa_video1{
	position: absolute;
	z-index:15;
	top: 0px;
	left: 0px;
	background-color:#000;
	filter:alpha(opacity=60);
	-moz-opacity: 0.60;
	opacity: 0.60;
	height: 20px;
	width:90px;
	border: 0px;
	visibility: visible;
	text-align: center;
	vertical-align: middle;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFFFFF;
	text-decoration: none;
	line-height: 18px;
	padding-top: 2px;
}
#capa_video1:hover{
	position: absolute;
	z-index:15;
	top: 0px;
	left: 0px;
	background-color:#000;
	filter:alpha(opacity=90);
	-moz-opacity: 0.90;
	opacity: 0.90;
	height: 20px;
	width:90px;
	border: 0px;
	visibility: visible;
	text-align: center;
	vertical-align: middle;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFFFFF;
	text-decoration: none;
	line-height: 18px;
	padding-top: 2px;
}
#capa_video2,.capa_video2{
	position: absolute;
	z-index:15;
	bottom: 10px;
	left: 10px;
	background-color:#000;
	filter:alpha(opacity=80);
	-moz-opacity: 0.80;
	opacity: 0.80;
	height: 25px;
	width:30px;
	border: 0px;
	visibility: visible;
}
#capa_video2:hover,.capa_video2:hover{
	filter:alpha(opacity=95);
	-moz-opacity: 0.95;
	opacity: 0.95;
}
/**** LLISTES ESTADISTIQUES ****/
.llista1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #4d4d4d;
	text-decoration: none;
	border-bottom-width: thin;
	border-bottom-style: solid;
	border-bottom-color: #4d4d4d;
    padding-right: 10px;
    vertical-align: bottom;
}
.llista2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: normal;
	color: #000000;
	text-decoration: none;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #999999;
    padding-right: 10px;
    vertical-align: bottom;
}
.llista2 a, #llista2_a{
	color:#FF9900;
	font-size:10px;
	font-style: normal;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-decoration: none;
    padding-right: 10px;
    vertical-align: bottom;
}
.llista2 a:hover, #llista2_a:hover{
	color:#4d4d4d;
	font-size:10px;
	font-style: normal;
	cursor:pointer;
	font-family: Arial, Helvetica, sans-serif;
	text-decoration: none;
    padding-right: 10px;
    vertical-align: bottom;
}
/******* BANNER PESTANYA LATERAL AMB FORMA DE BOTO ******/
#sidebar,#sidebar2{
	border: 1px solid #9FC54E;
	-moz-border-radius-topright: 5px;
	-webkit-border-top-right-radius: 5px;
	-moz-border-radius-bottomright: 5px;
	-webkit-border-bottom-right-radius: 5px;
	-moz-box-shadow:
		0px 2px 5px rgba(000,000,000,0.8),
		inset 0px 0px 1px rgba(255,255,255,0.7);
	-webkit-box-shadow:
		0px 2px 5px rgba(000,000,000,0.8),
		inset 0px 0px 1px rgba(255,255,255,0.7);
	box-shadow:
		0px 2px 5px rgba(000,000,000,0.8),
		inset 0px 0px 1px rgba(255,255,255,0.7);
	text-decoration: none;
	font-size: 9px;
	letter-spacing:-1px;
	font-family: verdana, helvetica, arial, sans-serif;
	color:#fff;
	padding: 0px;
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
	filter: alpha(opacity=90);
	-moz-opacity:0.9;
	-khtml-opacity: 0.9;
	opacity: 0.9;
}
#sidebar2:hover {
    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	-moz-opacity:1.0;
	-khtml-opacity: 1.0;
	opacity: 1.0;
}

#cap_el9nou {
    width:410px;
    height:41px;
    display:block;
    border: none;
    text-decoration:none;
    font-size:0pt;
    margin-left:18px;
    background-color:#fff;
    position:relative;
    padding:0px;
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	-moz-opacity:0;
	-khtml-opacity: 0;
	opacity: 0;
}
#cap_el9nou:hover {
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)";
	filter: alpha(opacity=20);
	-moz-opacity:0.2;
	-khtml-opacity: 0.2;
	opacity: 0.2;
}