<?php
require('../llibreries/func_navega.inc');
require('../llibreries/variables.inc.php');
$q=new DB_navega;$q2=new DB_navega;$q3=new DB_navega;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../css/estil.php?tam_txt=1" rel="stylesheet" type="text/css" media="all"/>
<title>Resultats poble per poble</title>
</head>
<body>

<?php
echo "<table border='0' cellpadding='0' cellspacing='3'><tr><td><b>Osona:</b>&nbsp;</td><td>";
echo "<form action='".$PHP_SELF."' method='post'><select name='pob_triat' onChange='submit()'>";
echo "<option value=''>tria un poble</option>";
$query="select * from ea_pob where pob_com='1' order by pob_nom";
$q->query($query);
while ($q->next_record()) {
  $pob_cod=$q->f("pob_cod");
  $pob_nom=stripslashes($q->f("pob_nom"));
  if ($pob_triat==$pob_cod) {
	$seleccionat="selected";
	$nom_poble_triat=$pob_nom;
  }
  else $seleccionat="";
  echo "<option value='".$pob_cod."' ".$seleccionat.">".$pob_nom."</option>";
}
echo "</select></form></td></tr>";
echo "<tr><td><b>Ripoll�s:&nbsp;</b></td><td>";
echo "<form action='".$PHP_SELF."' method='post'><select name='pob_triat' onChange='submit()'>";
echo "<option value=''>tria un poble</option>";
$query="select * from ea_pob where pob_com='2' order by pob_nom";
$q->query($query);
while ($q->next_record()) {
  $pob_cod=$q->f("pob_cod");
  $pob_nom=stripslashes($q->f("pob_nom"));
  if ($pob_triat==$pob_cod) {
	$seleccionat="selected";
	$nom_poble_triat=$pob_nom;
  }
  else $seleccionat="";
  echo "<option value='".$pob_cod."' ".$seleccionat.">".$pob_nom."</option>";
}
echo "</select></form></td></tr></table>";
if ($pob_triat>0) {
  ### CARREGA RESULTATS EXISTENTS
  $query="select * from ea_res left join ea_par on res_par=par_cod where res_pob=$pob_triat order by res_vact desc";
  $q->query($query);
  $i=0;
  $vots_act2=array();
  $vots_ant2=array();
  $codi_partit2=array();
  $nom_partit2=array();
  while ($q->next_record()) {
    $res_tip=$q->f("res_tip");
	if ($res_tip=="1") {
	  $b_act2=$q->f("res_vact");
	  $b_ant2=$q->f("res_vant");
	}
	if ($res_tip=="2") {
	  $n_act2=$q->f("res_vact");
	  $n_ant2=$q->f("res_vant");
	}
    if ($res_tip=="3") {
	  $cens_act2=$q->f("res_vact");
	  $cens_ant2=$q->f("res_vant");
	} 
    if ($res_tip=="4") {
	  $ve_act2=$q->f("res_vact");
	  $ve_ant2=$q->f("res_vant");
	}  
    if ($res_tip<"1") {
	  $vots_act2[$i]=0;
	  $vots_ant2[$i]=0;
	  $vots_act2[$i]=$q->f("res_vact");
	  $vots_ant2[$i]=$q->f("res_vant");
	  $codi_partit2[$i]=$q->f("res_par");
	  $nom_partit2[$i]=stripslashes($q->f("par_nom"));
	  $i++;
	}
  }
  ?>
  <br />
	    <table width="300" cellspacing="2" cellpadding="2">
            <tr><td colspan="5"><font size='4'><b><?php echo $nom_poble_triat ?></b></font></td></tr>
			<tr><td>&nbsp;</td><td colspan="2" bgcolor="#000000" align="center"><font color="#FFFFFF">Any <?php echo "<b>".$ele_any_act."</b>"; ?></font></td><td colspan="2" bgcolor="#000000" align="center"><font color="#FFFFFF">Any <?php echo "<b>".$ele_any_ant."</b>"; ?></td></tr>
            <?php if ($cens_act2>0) echo "<tr><td bgcolor='#666666'><font color='#FFFFFF'><b>&nbsp;Participaci�</td><td colspan='4' bgcolor='#EEEEEE'>&nbsp;".round(($ve_act2*100)/$cens_act2)." %</td></tr>"; ?>
            <tr><td bgcolor="#666666"><font color="#FFFFFF"><b>&nbsp;Cens</font></td><td colspan="2" bgcolor="#EEEEEE">&nbsp;<?php echo $cens_act2 ?></td><td colspan="2" bgcolor="#EEEEEE">&nbsp;<?php echo $cens_ant2 ?></td></tr>
            <tr><td bgcolor="#666666"><font color="#FFFFFF"><b>&nbsp;Vots&nbsp;emesos</font></td><td align='right' bgcolor="#EEEEEE"><?php echo $ve_act2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($cens_act2>0) echo round(($ve_act2*100)/$cens_act2)."%" ?>&nbsp;</td><td align='right' bgcolor="#EEEEEE"><?php echo $ve_ant2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($cens_ant2>0) echo round(($ve_ant2*100)/$cens_ant2)."%" ?>&nbsp;</td></tr>
            <tr><td bgcolor="#666666"><font color="#FFFFFF"><b>&nbsp;Blancs</font></td><td align='right' bgcolor="#EEEEEE"><?php echo $b_act2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($ve_act2>0) echo round(($b_act2*100)/$ve_act2)."%" ?>&nbsp;</td><td align='right' bgcolor="#EEEEEE"><?php echo $b_ant2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($ve_ant2>0) echo round(($b_ant2*100)/$ve_ant2)."%" ?>&nbsp;</td></tr>
            <tr><td bgcolor="#666666"><font color="#FFFFFF"><b>&nbsp;Nuls</font></td><td align='right' bgcolor="#EEEEEE"><?php echo $n_act2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($ve_act2>0) echo round(($n_act2*100)/$ve_act2)."%" ?>&nbsp;</td><td align='right' bgcolor="#EEEEEE"><?php echo $n_ant2 ?></td><td align='right' bgcolor="#EEEEEE">&nbsp;<?php if ($ve_ant2>0) echo round(($n_ant2*100)/$ve_ant2)."%" ?>&nbsp;</td></tr>
		  <?php 
		  for ($i=0;$i<sizeof($nom_partit2);$i++) {
			if ($nom_partit2[$i]=="Altres") {
              $altres_txt="<tr><td bgcolor='#CCCCCC'><b>&nbsp;".$nom_partit2[$i]."</td><td align='right' bgcolor='#EEEEEE'>".$vots_act2[$i]."</td><td align='right' bgcolor='#EEEEEE'>&nbsp;";if ($ve_act2>0) $altres_txt.=round(($vots_act2[$i]*100)/$ve_act2);$altres_txt.="%&nbsp;</td><td align='right' bgcolor='#EEEEEE'>".$vots_ant2[$i]."</td><td align='right' bgcolor='#EEEEEE'>&nbsp;";if ($ve_ant2>0) $altres_txt.=round(($vots_ant2[$i]*100)/$ve_ant2);$altres_txt.="%&nbsp;</td></tr>";
			}
			else {
              echo "<tr><td bgcolor='#CCCCCC'><b>&nbsp;".$nom_partit2[$i]."</td><td align='right' bgcolor='#EEEEEE'>".$vots_act2[$i]."</td><td align='right' bgcolor='#EEEEEE'>&nbsp;";if ($ve_act2>0) echo round(($vots_act2[$i]*100)/$ve_act2);echo"%&nbsp;</td><td align='right' bgcolor='#EEEEEE'>".$vots_ant2[$i]."</td><td align='right' bgcolor='#EEEEEE'>&nbsp;";if ($ve_ant2>0) echo round(($vots_ant2[$i]*100)/$ve_ant2);echo "%&nbsp;</td></tr>";
            }
		  } 
		  echo $altres_txt;
		  ?>
        </table>
        <?php
}
?>

</body>
</html>