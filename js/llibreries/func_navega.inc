<?php
    require("db_mysql.inc");
        // Funcions utils
        ### torna tag MP4 segons l'amplada
		function mp4_630($valor)
		{
			if (strlen($valor)<10) return "<iframe id='iframe_chapter' src='http://el9tv.xiptv.cat/embed/".$valor."?width=608&amp;iframe_width=630&amp;share=&amp;height=342&amp;iframe_height=388' name='iframe_chapter' width='630' height='388' frameborder='0' scrolling='no'></iframe>";
            else return $valor;
		}
		function mp4_370($valor)
		{
			if (strlen($valor)<10) return "<iframe id='iframe_chapter' src='http://el9tv.xiptv.cat/embed/".$valor."?width=348&amp;iframe_width=370&amp;share=&amp;height=196&amp;iframe_height=242' name='iframe_chapter' width='370' height='242' frameborder='0' scrolling='no'></iframe>";
            else return $valor;
		}
		function mp4_220($valor)
		{
			if (strlen($valor)<10) return "<iframe id='iframe_chapter' src='http://el9tv.xiptv.cat/embed/".$valor."?width=248&amp;iframe_width=220&amp;share=&amp;height=140&amp;iframe_height=186' name='iframe_chapter' width='220' height='186' frameborder='0' scrolling='no'></iframe>";
            else return $valor;
		}

		### Posa punt de milers
		function miler($valor)
		{
		  return number_format($valor, 0, ',', '.');
		}
		### Crea data texte
		function data_txt() {
		  $nom_del_mes=array("","de gener de","de febrer de","de mar� de","d'abril de","de maig de","de juny de","de juliol de","d'agost de","de setembre de","d'octubre de","de novembre de","de desembre de");
		  echo date("j")." ".$nom_del_mes[date("n")]." ".date("Y");
		}
		### Limita longitud text
		function longitud_txt($valor1,$valor2) {
		  if (strlen($valor1)>$valor2) {
		    $valor1=substr($valor1,0,$valor2);
            for ($i=strlen($valor1);$i>0;$i--) {
		      if (substr($valor1,$i,1)==" ") break;
		    }
			$valor1=substr($valor1,0,$i)."...";
		  }
		  return $valor1;
		}
		// FILTRA TEXTE CAMP AMB BARRA D'EINES
		function rteSafe($strText) {
	      //returns safe code for preloading in the RTE
	      $tmpString = $strText;
	
	      //convert all types of single quotes
	      $tmpString = str_replace(chr(145), chr(39), $tmpString);
	      $tmpString = str_replace(chr(146), chr(39), $tmpString);
	      $tmpString = str_replace("'", "&#39;", $tmpString);
	
	      //convert all types of double quotes
	      $tmpString = str_replace(chr(147), chr(34), $tmpString);
	      $tmpString = str_replace(chr(148), chr(34), $tmpString);
          //	$tmpString = str_replace("\"", "\"", $tmpString);
	
	      //replace carriage returns & line feeds
	      $tmpString = str_replace(chr(10), " ", $tmpString);
	      $tmpString = str_replace(chr(13), " ", $tmpString);
	
	      return $tmpString;
        }
		function treu_blancs($cadena) {
          $cadena=str_replace(" ","",$cadena);
		  return $cadena;
		}
		
	    ####	 CONVERTEIX CARACTERS NO ESTANDARDS
		function elimina_accents($cadena){
		  $limit_paraules=8;
		  $num_paraula=0;
		  $cadena3="";
		  $cadena=strtolower($cadena);
		  $cadena=str_replace("\"","",$cadena);
		  $cadena=str_replace("\.","",$cadena);
		  $cadena=str_replace("\,","",$cadena);
		  $cadena=str_replace("\:","",$cadena);
		  $cadena=str_replace("\;","",$cadena);
		  $cadena2=explode(" ",$cadena);
		  for ($r=0;$r<sizeof($cadena2);$r++) {
		    if (strlen($cadena2[$r])>2 || ctype_digit($cadena2[$r])) {
              $abuscar = "�����������������������������������������������������`'%��";
              $atornar = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn_____ao";
			  if ($num_paraula==0) $cadena3.="/".strtr($cadena2[$r],$abuscar,$atornar);
			  else $cadena3.="-".strtr($cadena2[$r],$abuscar,$atornar);
			  $num_paraula++;
			}
			if ($num_paraula==$limit_paraules) break;
		  }
          return $cadena3;
        }

	    function tradcars($valor) {
		  for ($ii=0;$ii<strlen($valor);$ii++) {
		    $cc=ord(substr($valor,$ii,1));
		    if (!(($cc>96 && $cc<123) || ($cc>64 && $cc<91) || ($cc>47 && $cc<58))) $valor=substr($valor,0,$ii)."_".substr($valor,($ii+1),(strlen($valor)-($ii+1)));
		  }
          return $valor;
		}
				
		function comprova_mail($valor)
		{
		  $long_mail=(strlen($valor)-1);
		  $pos_arroba=strpos($valor,"@");
		  $pos_punt=strrpos($valor,".");
		  if ($pos_arroba>0 && $pos_punt>0 && $pos_punt>($pos_arroba+1) && $pos_arroba<$long_mail && $pos_punt<$long_mail) return "S";
		  else return "N";
        }
		function comprova_web($valor)
		{
		  $long_web=(strlen($valor)-1);
		  $pos_blanc=strpos($valor," ");
		  $pos_punt=strrpos($valor,".");
		  if ($pos_blanc<1 && $pos_punt>0 && $pos_punt<$long_web && (substr($valor,0,7)=="http://" || substr($valor,0,8)=="https://" || substr($valor,0,6)=="mailto")) return "S";
		  else return "N";
        }
		
		function comprova_dir($valor)
		{
		  $valor=stripslashes($valor);
		  $valor = str_replace(chr(92),chr(47),$valor);
		  if (substr(trim($valor),-1,1)!=chr(47)) $directori=trim($valor).chr(47);
		  else $directori=trim($valor);
		  return $directori;
		}
		
		function comprova_res($valor)
		{
		  $valor=str_replace(",",".",$valor);
		  $valor=str_replace(".","",$valor);
		  if (!ctype_digit($valor)) return "N";
		  else return "S";
		}
		function comprova_res2($valor)
		{
		  $valor=str_replace("\%","",$valor);
		  $valor=str_replace(",",".",$valor);
		  $valor=str_replace("\.","",$valor);
		  if (!ctype_digit($valor)) return "N";
		  else return "S";
		}
		
		function posa_barres($valor)
		{
		  $valor=str_replace("\?","\\?",$valor);
		  return $valor;
		}
		
        function cometes($valor)
        {
                return addslashes(stripslashes($valor));
        }		
		
        function cometes1($valor)
        {
                return (str_replace("\"","\�\�",$valor));
        }        
        function cometes_($valor)
        {
                return (addslashes(stripslashes(cometes1($valor))));
        }
		function cometes3($valor)
        {
		        $valor=str_replace(chr(39),chr(180),$valor);
				$valor=str_replace(chr(146),chr(180),$valor);
				$valor=str_replace(chr(147),chr(34),$valor);
				$valor=str_replace(chr(148),chr(34),$valor);
				$valor=str_replace(chr(13).chr(10)," ",$valor);
				$valor=str_replace("<br>","<br />",$valor);
				$valor=str_replace("<br /><br />","<br />",$valor);
                return $valor;
        }
		
		function cometes4($valor)
		{
		        $valor=str_replace("\"","�",$valor);
				$valor=str_replace("\'","�",$valor);
				$valor=str_replace("<br>","\n",$valor);
				$valor=str_replace("<br />","\n",$valor);
				$valor=str_replace(">","",$valor);
				$valor=str_replace("<","",$valor);
				$valor=stripslashes($valor);
				return $valor;
		}
		function cometes5($valor)
		{
		        $valor=str_replace("\"","�",$valor);
				$valor=str_replace("'","�",$valor);
				$valor=str_replace(">","",$valor);
				$valor=str_replace("<","",$valor);
				$valor=str_replace(";","",$valor);
				$valor=str_replace(chr(13).chr(10)," ",$valor);
				$valor=addslashes(stripslashes($valor));
				return $valor;
		}
		function datadma($databd)    // converteix AAAA-MM-DD > DD-MM-AAAA
		{
		  $databd=substr($databd,8,2)."-".substr($databd,5,2)."-".substr($databd,0,4);
		  if (substr($databd,0,2)<1) $databd="";
		  return $databd;
		}
		function dataamd($databd)    // converteix DD-MM-AAAA > AAAA-MM-DD
		{
		  $databd=substr($databd,6,4)."-".substr($databd,3,2)."-".substr($databd,0,2);
		  if (substr($databd,0,2)<19) $databd="";
		  return $databd;
		}
		
		function hora_punts($horabd)    // converteix HHMM > HH:MM
		{
		  if (substr($horabd,0,2)<1 && substr($horabd,2,2)<1) $horabd="";
		  else $horabd=abs(substr($horabd,0,2)).":".substr($horabd,2,2);
		  return $horabd;
		}
		
        function apostrof($valor)
        {
                return (str_replace("\'","�",$valor));
        }
        function apostrof2($valor)
        {
                return (str_replace("'","�",$valor));
        } 
        function apostrof3($valor)
        {
                return (str_replace("'","&rsquo;",str_replace("�","&rsquo;",$valor)));
        } 
        function enter2br($valor)
        {
			    $valor=str_replace("�","&rsquo;",$valor);
                return (str_replace("<br>","<br />",str_replace(chr(13),"<br />",str_replace(chr(10),"",$valor))));
        }
		function enter3br($valor)
        {
                return (str_replace("<br>","<br />",$valor));
        }
        function enter2p($valor)
        {
		        $valor=trim($valor);
                return (str_replace(chr(13),"</p><p>",str_replace(chr(10),"",$valor)));
        }

        function enter2nl($valor)
        {
                return (str_replace(chr(13),"\\n",str_replace(chr(10),"",$valor)));
        }
        
        function renc($valor) 
        { 
                return (addslashes(strtr(stripslashes($valor),"'","\"")));
        }

        function rdec($valor) 
        { 
                return (enter2br(str_replace("\"","\\".chr(34),strtr(stripslashes($valor),"'","\""))));
        }

        function rdec2($valor) 
        { 
                return (enter2nl(str_replace("\"","\\".chr(34),strtr(stripslashes($valor),"'","\""))));
        }

        function iso2spanish($fecha)
        {
                $fecha_trozos=explode("-",$fecha);
                $fecha_spanish=sprintf("%02d/%02d/%04d", $fecha_trozos[2], $fecha_trozos[1], $fecha_trozos[0]);
                return $fecha_spanish;
        }

        function spanish2iso($fecha)
        {
                $fecha_trozos=explode("/",$fecha);
                $fecha_iso=sprintf("%04d-%02d-%02d", $fecha_trozos[2], $fecha_trozos[1], $fecha_trozos[0]);
                return $fecha_iso;
        }

        function timestamp2spanish($fecha)
        {
                $fecha_spanish=$fecha;
                $anyo=substr($fecha,0,4);
                $mes=substr($fecha,4,2);
                $dia=substr($fecha,6,2);
                $fecha_spanish="$dia/$mes/$anyo";
                return $fecha_spanish;
        }
        
        function timestamp2spanishfechahora($fecha)
        {
                $anyo=substr($fecha,0,4);
                $mes=substr($fecha,4,2);
                $dia=substr($fecha,6,2);
                $hora=substr($fecha,8,2);
                $minuto=substr($fecha,10,2);
                $fechahora_spanish="$dia/$mes/$anyo $hora:$minuto";
                return $fechahora_spanish;
        }

        function spanish2timestamp($fecha)
        {
                $fecha_trozos=explode("/",$fecha);
                $fecha_timestamp=$fecha_trozos[2].$fecha_trozos[1].$fecha_trozos[0]."000000";
                return $fecha_timestamp;
        }

        function generatimestamp()
        {
                $fecha_timestamp=date("YmdHis");
                return $fecha_timestamp;
        }

function valida_nif($cif) {
//Copyright �2005-2011 David Vidal Serra. Bajo licencia GNU GPL.
//Este software viene SIN NINGUN TIPO DE GARANTIA; para saber mas detalles
//puede consultar la licencia en http://www.gnu.org/licenses/gpl.txt(1)
//Esto es software libre, y puede ser usado y redistribuirdo de acuerdo
//con la condicion de que el autor jamas sera responsable de su uso.
//Returns: 1 = NIF ok, 2 = CIF ok, 3 = NIE ok, -1 = NIF bad, -2 = CIF bad, -3 = NIE bad, 0 = ??? bad
         $cif = strtoupper($cif);
         for ($i = 0; $i < 9; $i ++)
         {
                  $num[$i] = substr($cif, $i, 1);
         }
//si no tiene un formato valido devuelve error
         if (!preg_match('/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/', $cif))
         {
                  return 0;
         }
//comprobacion de NIFs estandar
         if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $cif))
         {
                  if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 0, 8) % 23, 1))
                  {
                           return 1;
                  }
                  else
                  {
                           return -1;
                  }
         }
//algoritmo para comprobacion de codigos tipo CIF
         $suma = $num[2] + $num[4] + $num[6];
         for ($i = 1; $i < 8; $i += 2)
         {
                  $suma += substr((2 * $num[$i]),0,1) + substr((2 * $num[$i]), 1, 1);
         }
         $n = 10 - substr($suma, strlen($suma) - 1, 1);
//comprobacion de NIFs especiales (se calculan como CIFs o como NIFs)
         if (preg_match('/^[KLM]{1}/', $cif))
         {
                  if ($num[8] == chr(64 + $n) || $num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 1, 8) % 23, 1))
                  {
                           return 1;
                  }
                  else
                  {
                           return -1;
                  }
         }
//comprobacion de CIFs
         if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $cif))
         {
                  if ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1))
                  {
                           return 2;
                  }
                  else
                  {
                           return -2;
                  }
         }
//comprobacion de NIEs
         if (preg_match('/^[XYZ]{1}/', $cif))
         {
                  if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $cif), 0, 8) % 23, 1))
                  {
                           return 3;
                  }
                  else
                  {
                           return -3;
                  }
         }
//si todavia no se ha verificado devuelve error
         return 0;
}


        class DB_navega extends DB_Sql {
                var $classname = "DB_navega";
                var $Host     = "localhost";
                var $Database = "prosa";
                var $User     = "prosa";
                var $Password = "pro999";

                function haltmsg($msg) {
                        /*printf("</td></table><b>Database error:</b> %s<br>\n", $msg);
                        printf("<b>SQL Error</b>: %s (%s)<br>\n",
                        $this->Errno, $this->Error);
                        printf("Aviseu a l�administrador del sistema, ");
                        printf("hi ha hagut un error del programa.<br>\n");*/
						printf("<br />Heu utilitzat car�cters incompatibles amb el camp d�entrada<br><br />");
                        }
                }

?>
