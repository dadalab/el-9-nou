<?php
  #### CARREGA VARIABLES GENERALS
  include('variables.inc.php');
  if (!isset($java_txt)) $java_txt="";
  if (!isset($body_txt)) $body_txt="";
  if (!isset($body_preload_txt)) $body_preload_txt="";
  if (!isset($nivell_pri)) $nivell_pri = 1;  // (1=USUARI I SUPERIORS / 2=DIRECCIO I SUP. / 3=ADMINISTRADOR)

  #### CONTROL ACCES PER IP
  mysql_pconnect(localhost,"prosa","pro999");
  if ($REMOTE_ADDR!="80.64.35.29" && $REMOTE_ADDR!="80.64.33.66" && $REMOTE_ADDR!="213.96.59.195" && $REMOTE_ADDR!="80.64.33.70" && $REMOTE_ADDR!="80.64.32.11" && $REMOTE_ADDR!="88.12.2.67") {
  $result=mysql_db_query("prosa","select i.* from al_ip i where i.ip_pri>='$nivell_pri'");
  while ($camp=mysql_fetch_array($result)) {
    $ip_ip2=stripslashes($camp["ip_ip"]);
	$troba_ip=strpos($REMOTE_ADDR,$ip_ip2);
	if (strlen($troba_ip)>0) break;
  }
  mysql_free_result($result);
  if (strlen($troba_ip) == "0") {        // SI LA IP NO ESTA PERMES DEMANARA CONTROL D'USUARIS 
   
    #### CONTROL ACCES PER USUARI
    if(!isset($PHP_AUTH_USER)){
      Header("WWW-Authenticate: Basic realm=\"PROSA\"");
      Header("HTTP/1.0 401 Unauthorized");
      echo "Usuari no autoritzat";
      exit;
    } 
    else 
    {
	  $result=mysql_db_query("prosa","select u.* from al_usu u where u.usu_log='$PHP_AUTH_USER' and u.usu_pas=PASSWORD('$PHP_AUTH_PW') and u.usu_pri >= '$nivell_pri'");
      $nr=mysql_num_rows($result);
      mysql_free_result($result);	 
      if ($nr==0) {     // SI NO TROBA REGISTRES COINCIDENTS SURT DEL PROGRAMA
        Header("WWW-Authenticate: Basic realm=\"PROSA\"");
        Header("HTTP/1.0 401 Unauthorized");
        echo "Usuari no autoritzat";
        exit;
      }
    }
  }
  }

if (!$titol_capcalera) $titol_capcalera = "";       ### TITOL PER OMISSIO SI NO ES DEFINEIX
### COMPROVA NAVEGADOR = Firefox
$tipus_nav=$_SERVER['HTTP_USER_AGENT'];
$firefox_tf=strpos($tipus_nav,"Firefox");
$chrome_tf=strpos($tipus_nav,"Chrome");
if ($firefox_tf===false && $chrome_tf===false) echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>NAVEGA - Premsa d'Osona</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language="JavaScript" type="text/javascript" src="../editor/html2xhtml.js"></script>
<?php if ($editor_petit=="S") echo "<script language='JavaScript' type='text/javascript' src='../editor/richtext_breu.js'></script>";
else echo "<script language='JavaScript' type='text/javascript' src='../editor/richtext.js'></script>";
if ($calendari_sn=="S") echo "<script language=\"JavaScript\" type=\"text/javascript\" src=\"../llibreries/popcalendar.js\"></script>";
if ($refresca_pagina>0) echo "<meta http-equiv='refresh' content='".$refresca_pagina.";URL=".$PHP_SELF.$url2."'>"; ?>

<link href="../css/estil_admin.css" rel="stylesheet" type="text/css" />

<script LANGUAGE="JavaScript">
<!--

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function NewWindow(mypage,myname,w,h,scroll,pos){
if(pos=="random"){LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
if(pos=="center"){LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
else if((pos!="center" && pos!="random") || pos==null){LeftPosition=0;TopPosition=20}
settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=yes,toolbar=no,resizable=no';

win=window.open("../llibreries/blanc.htm",myname,settings);
win.close();
win=window.open(mypage,myname,settings);
}

function boto_form(boto_form_triat) {
  document.demana.nom_boto_form.value=boto_form_triat;
  document.demana.submit();
}
function boto_form2(boto_form_triat) {
  document.demana.nom_boto_form2.value=boto_form_triat;
  document.demana.submit();
}


function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function obrecomp (nom,amplegal,algal) {
  datgal = 'width=' + amplegal + ',height=' + algal + ',left=0,top=0,scrollbars=yes,resize=no,location=0,status=0,menubar=0';
  window.open(nom,'',datgal)
}
<?php
########## INSERCIO DE FUNCIONS JAVA DE LA PAGINA QUE CRIDA LA CAPCALERA
echo $java_txt;
?>

//-->
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
function obre_arxiu(variable) {
  open (variable)
}

function confirmar(text1) {
  confirma = confirm("Segur que vols eliminar el bloc: "+text1+" ?");
  if (confirma == 1) {
    return true;
  } else {
    return false;
  }  
}
function confirmar_foto(text1) {
  confirma = confirm("Segur que vols eliminar l'assignaci� de foto: "+text1+" ?");
  if (confirma == 1) {
    return true;
  } else {
    return false;
  }  
}
function eliminar_arxiu() {
  confirma = confirm("Segur que vols eliminar l�arxiu adjunt?");
  if (confirma == 1) {
    return true;
  } else {
    return false;
  }  
}
function eliminar_comptador() {
  confirma = confirm("Segur que vols buidar el comptador?");
  if (confirma == 1) {
    return true;
  } else {
    return false;
  }  
}
function fer_submit() {
  document.submit();
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function limitText2(limitField, limitCount, limitNum) {
		limitCount.value = limitNum - limitField.value.length;
}

function LimitAttach(tField,iType,inomvar) 
{  
    nomfile=tField.value;
	iName=tField.name;
    if (iType==1) 
    {  
        extArray = new Array(".gif",".jpg");  
    }  
    if (iType==2) 
    {  
        extArray = new Array(".swf");  
    }  
    if (iType==3) 
    {  
        extArray = new Array(".exe",".sit",".zip",".tar",".swf",".mov",".hqx",".ra",".wmf",".mp3",".qt",".med",".et");  
    }  
    if (iType==4) 
    {  
        extArray = new Array(".mov",".ra",".wmf",".mp3",".qt",".med",".et",".wav");  
    }  
    if (iType==5) 
    {  
        extArray = new Array(".html",".htm",".shtml");  
    }  
    if (iType==6) 
    {  
        extArray = new Array(".doc",".xls",".ppt");  
    } 
    if (iType==7) 
    {  
        extArray = new Array(".pdf");  
    }
    if (iType==8) 
    {  
        extArray = new Array(".mp4",".MP4");  
    }
    if (iType==9) 
    {  
        extArray = new Array(".mp3");  
    }
    if (iType==10) 
    {  
        extArray = new Array(".jpg");  
    }
    if (iType==11) 
    {  
        extArray = new Array(".gif",".jpg",".swf");  
    } 
    allowSubmit = false;  
    if (!nomfile) return;  
    while (nomfile.indexOf("\\") != -1) nomfile = nomfile.slice(nomfile.indexOf("\\") + 1);  
    ext = nomfile.slice(nomfile.indexOf(".")).toLowerCase();  
    for (var i2 = 0; i2 < extArray.length; i2++) 
    {  
        if (extArray[i2] == ext) 
        {  
            allowSubmit = true;  
            break;  
        }  
    }  
    if (allowSubmit) 
    { 
		inomvar.value="[ " + nomfile + " ] pendent d�enviar al servidor";
    } 
    else 
    {  
		inomvar.value="";
        alert("Nom�s s`admeten extensions " + (extArray.join(" ")) + "\nTria un altre arxiu");  
    }  
}  

//-->
</script>
</head>
<?php
echo "<body";
if ($calendari_sn!="S" && ($firefox_tf===false || $chrome_tf===false)) echo " onLoad=\"".$body_txt."\"";
echo ">";
?>
