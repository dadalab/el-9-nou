<?php
  ####### CAMINS ########
  $ip_local="localhost";
  $ip_remota="localhost";
  $ip_pub_vic="109.69.9.37";
  $ip_pub_gra="185.107.106.47";
  $dir_ftp="www.el9nou.cat/";
  $cami_adjunts="../"; ## ja es el definitiu
  $cami_adjunts2="";
  $cami_adjunts3="";   ### quan estigui al lloc definitiu s'haur� d'esborrar
  $camiweb_sencer="/";
  $camiweb_mobil="/mobil/";
  ####### MIDES ########
  $mida_marc="0";
  $mida_spacing="1";
  $mida_padding="1";
  $mida_ample_web="996";
  $mida_blanc_web="2";
  ###### VARIABLES PARTICULARS ######
  $priv_usu_nom=array("","Redactor","Directiu","Administrador");
  $email_sec=array("","Informaci&oacute;","Redacci&oacute; Osona","Redacci&oacute; Vall&eacute;s","Promocions","Subscriptors","Classificats","Servei T&egrave;cnic","Publicitat","Copiats v�deo","Copiats foto Osona","Copiats foto Vall�s","Espai obert Osona","Espai obert Vall�s");
  $prod_nom=array("","EL 9 NOU Osona","EL 9 NOU Vall&egrave;s","EL 9 TV","CTL");
  $prod2_nom=array("","EL 9 NOU","EL 9 NOU","EL 9 TV","CTL");
  $inf_noms=array("","PROSA Osona","PROSA Vall�s","Av�s Legal","Contacteu-nos Osona","Contacteu-nos Vall�s","Edici� Paper","FAQ","Cookies");
  $nom_visible=array("Entre dates","Sempre","Mai");
  $ctl_apar=array("","actualitat","cat�leg","l�empresa");
  $tit_infoavis=array("","Informaci�","Av�s legal","Premsa d'Osona, SA","Subscriptors","Tarifes");
  $nom_mes=array("","gener","febrer","mar�","abril","maig","juny","juliol","agost","setembre","octubre","novembre","desembre");
  $tip_cont=array("","Exclusiu secci�","Redacci�","Publicitat");
  $tip_secc=array("","Indefinit","Men� sup.","Recomanacions","Enlla�os");
  $reg_per_pag=25;
  $amp_camp_max=40;  ### TALLA CAMP EN EL LLISTAT SI SUPERA AQUESTS CARACTERS
  $amp_camp_txt=250; ### AMPLADA FINESTRA TEXTE AMPLIAT
  $amp_camp_fot=100; ### AMPLADA FINESTRA FOTO AMPLIADA
  $ample_formulari=700; ### AMPLADA FORMULARI ALTES, BAIXES I MODIFICACIONS
  #$textes_sec=array("","Informaci�","Av�s Legal","Premsa d�Osona","Subscripcions","Publicitat");
  $comp_nom=array("","v�deo","audio","foto","pdf","fotogaleria","enlla�","not�cia","n.d�arxiu");
  $comp_ini=array("","V","A","F","P","G","L","N","X");
  $comp_ext=array("","mp4","mp3","jpg","pdf","","","","");
  $seccio_n=array("","Fotogaleries","Classificats","Agenda","Espai obert","El9Tv","Corporatiu","Contacte","RSS","PDFs","Club","Cerques","CTL","Documents","Enlla�os","Tv a la carta","UVIC","Podcast","Citoyen","Ofertes","Regals","Irresistible");
  $seccio_c=array("","99998","99997","99996","99995","99994","99993","99992","99991","99990","99989","99988","99987","99985","99984","99983","99982","99981","99980","99979","99978","99977");
  $caduca_ses="30"; ## MINUTS PER CADUCAR UN BANNER
  $cap_per_pag=21;  ## CAPITOLS TV CARTA PER PAGINA
  $ele_any_act=date("Y");
  $ele_any_ant=(date("Y")-2);
  $centre_treball_nom=array("Totes","Osona","Vall�s");
?>
