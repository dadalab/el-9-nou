<?php

/** 
 * Les configuracions bàsiques del WordPress.
 *
 * Aquest fitxer té les següents configuracions: configuració de MySQL, prefix de taules,
 * claus secretes, idioma del WordPress i ABSPATH. Trobaràs més informació 
 * al Còdex (en anglès): {@link http://codex.wordpress.org/Editing_wp-config.php Editant
 * el wp-config.php}. Les dades per a configurar MySQL les pots obtenir del
 * teu proveïdor d'hostatjament de web.
 *
 * Aquest fitxer és usat per l'script de creació de wp-config.php durant la
 * instal·lació. No cal que usis el web, pots simplement copiar aquest fitxer
 * sota el nom "wp-config.php" i omplir-lo amb els teus valors.
 *
 * @package WordPress
 */

// ** Configuració de MySQL - Pots obtenir aquestes informacions del teu proveïdor de web ** //
/** El nom de la base de dades per al WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/el9nou/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'el9nou');

/** El teu nom d'usuari a MySQL */
define('DB_USER', 'root');

/** La teva contrasenya a MySQL */
define('DB_PASSWORD', 'yzeHPMCQza2v');

/** Nom del host de MySQL */
define('DB_HOST', 'localhost');

/** Joc de caràcters usat en crear taules a la base de dades. */
define('DB_CHARSET', 'utf8mb4');

/** Tipus d'ordenació en la base de dades. No ho canvïis si tens cap dubte. */
define('DB_COLLATE', '');

/**#@+
 * Claus úniques d'autentificació.
 *
 * Canvia-les per frases úniques diferents!
 * Les pots generar usant el {@link http://api.wordpress.org/secret-key/1.1/ servei de claus secretes de WordPress.org}
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'j3p,7*-lnKr5Ly*u_cRe*&_]T&qId/(ts+([0hwz,w9_N+9F`A6xA,F~KT!J*Qn!');
define('SECURE_AUTH_KEY', 'MZI?aXn8(fA+jAi^`n=w+6/|{1B+);9dD-)W(8qZc2VP7dtRo z}7tzod{v%p}ST');
define('LOGGED_IN_KEY', 'G=sH19uS`9Y?D7te,{inNTv3S2n%$#m:GhV$jD Yb=Ff@QP2v-XOu, YTHa:2D+f');
define('NONCE_KEY', 'a06RvNxp>I$U&fpkaL4|Aq=,8pJ?Maog:I%*N{IN&jt:P,k]kLTA4ATf>MMaK9D ');
define('AUTH_SALT',        'XECGb#^%0my<z*i)c12-N&d|>36-4] 4l.U,uLo?U.9bRx5^#cX3ZOR+@snUG/},');
define('SECURE_AUTH_SALT', 'MtZ7^$6S]#D<o}V.%*f3>SA9:_8@I&w* Sj{2dyNr&ig:PoK0?#|^`?XEXg>kSP*');
define('LOGGED_IN_SALT',   '/aP%iLOr7Wn@3G/zP$>JFb-[U}Kb4b8E@wg:iGz*+VL{|3@?3vwQOQ3F,pmkQDV6');
define('NONCE_SALT',       'D)vg6}^]IgaIixlV93#5v>K;`HP}H4lK+w5fVqn!tlvi;u>SNpW;QPiduL[~;   ');
/**#@-*/

/**
 * Prefix de taules per a la base de dades del WordPress.
 *
 * Pots tenir múltiples instal·lacions en una única base de dades usant prefixos
 * diferents. Només xifres, lletres i subratllats!
 */
$table_prefix  = 'wp_';


/**
 * Per a desenvolupadors: WordPress en mode depuració.
 *
 * Canvieu això si voleu que es mostren els avisos durant el desenvolupament.
 * És molt recomanable que les extensions i el desenvolupadors de temes facien servir WP_DEBUG
 * al seus entorns de desenvolupament.
 *
 * Per informació sobre altres constants que es poden utilitzar per depurar,
 * visiteu el còdex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// No guardem versions antigues de notícies. No sobrecarregar BD.
define( 'WP_POST_REVISIONS', false );
define('SAVEQUERIES', true);

// Això és tot, prou d'editar - que bloguis de gust!

/** Ruta absoluta del directori del Wordpress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Assigna les variables del WordPress vars i fitxers inclosos. */
require_once(ABSPATH . 'wp-settings.php');
