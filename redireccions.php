<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("wp-load.php"); 

$old = $_GET['old'];
if (! $old) return;

global $wpdb;
$results = $wpdb->get_row( "SELECT * FROM redireccio WHERE not_cod = $old" );
$postId = $results->wp_id;
header("HTTP/1.1 301 Moved Permanently"); 
header("Location: " . get_permalink($postId)); 

