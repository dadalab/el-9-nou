<?php
$link = get_field('portada_vella') ? 'http://s3-eu-west-1.amazonaws.com/el9nou/' . get_field('portada_vella') : get_field('enllaç_portada');
?>
<div class="col-md-4 col-sm-4 col-xs-12 item">
	<div class="hemeroteca-item">
		<a target="_blank" href="<?php echo $link ?>"><?php the_post_thumbnail() ?></a>


		<?php
			if (get_field('data_diari') != "")
				$date = DateTime::createFromFormat('Ymd', get_field('data_diari'))->format('d/m/Y');
			else
				$date = get_the_date(); ?>

		<span class="data"><?php echo $date ?></span>
        <a target="_blank" title="Veure portada" href="<?php echo $link ?>" style="float:right;margin-top: 12px;font-size: 18px;"><i class="icon9fm-eye"></i></a>

		<div class="options" style="clear: both;float: none">
            <hemeroteca-diari inline-template v-cloak>
                <div style="display: flex;font-size: 12px;justify-content: space-between;"><?php
                    $diari = get_field('diari');
                    if ($diari != "") : ?>
                        <select v-model="diari" style="width: 90%;display: block;">
                            <option value="<?php echo $diari ?>" selected="selected">EL 9 NOU</option>
                            <?php if ($magazin = get_field('magazin')) : ?>
                            	<option value="<?php echo $magazin ?>">EL 9 MAGAZIN</option>
	                        <?php endif ?>
	                        <?php if ($esportiu = get_field('esportiu')) : ?>
                            	<option value="<?php echo $esportiu ?>">L'ESPORTIU</option>
	                        <?php endif ?>
	                        <?php if ($suplement = get_field('suplement')) : ?>
                            	<option value="<?php echo $suplement ?>">EL SUPLEMENT</option>
	                        <?php endif ?>
                        </select>

                        <button v-on:click="descarrega" style="-webkit-appearance: none;border: none;background: none">Descarrega</button><?php
                    endif; ?>
                </div>


                    <button class="btn-link" title="Descarrega edició PDF" data-toggle="tooltip" data-placement="top" style="display: none">
                        <i v-if="! loading" class="fa fa-newspaper-o" style="color:#333" v-on:click="descarrega('<?php echo $diari ?>', '<?php echo get_the_date() ?>')"></i>
                        <i v-if="loading" class="fa fa-spinner fa-spin fa-fw" style="color:#333"></i>
                    </button>

            </hemeroteca-diari>
		</div>
	</div>
</div>