<h4 class="titol-hemeroteca">Portada de l'última edició <i class="fa fa-newspaper-o" aria-hidden="true"></i></h4>
<h5 class="data"><?php echo get_the_date() ?></h5>
<a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/hemeroteca" class="p-hemeroteca"><?php the_post_thumbnail() ?></a>
<a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/hemeroteca" class="veure-tota-hemeroteca">Veure totes les portades</a>
