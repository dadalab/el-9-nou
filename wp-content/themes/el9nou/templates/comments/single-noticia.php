<section class="comments hidden-print" id="comments">
    <comments id="<?php the_ID() ?>" site="el9nou" edicio="<?php echo edicio_actual() ?>" inline-template v-cloak>
        <div class="formulari" style="margin-bottom: 0">
            <header>
                <div class="col-md-12 comment-form text-center">
                    <h1>Comentaris</h1>
                    <button v-if="acting_as" @click="anarAComentari" class="btn send" style="width: 200px;float:none;margin:20px 0">Fer un comentari</button>
                    <div class="text-center" v-if="!loading && !comments.length">Encara no hi ha comentaris en aquesta entrada.</div>
                </div>
            </header>
        </div>
        <div v-if="loading" class="text-center" style="margin-bottom: 40px"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>

        <ol class="commentlist" v-if=" ! loading ">
            <div class="row">
                <div class="comment clearfix" v-for="comment in comments">
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <i class="icon-nickname"></i>
                        <div class="nickname">
                            <span>{{ comment.usuari }}</span><br>{{ comment.data }}
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <small v-if="! comment.approved"><span class="label label-info">Comentari pendent d'aprovació</span></small>
                        <p>{{ comment.text }}</p>
                    </div>
                </div>
            </div>
        </ol>
        <hr>
        <div class="row" style="margin-bottom: 80px">
            <div class="col-md-4">
                <header>
                    <h1>Fer un comentari</h1>
                </header>
            </div>
            <div class="col-md-8 col-sm-8">
                <div id="commentform" class="comment-form" style="padding-top: 0!important" v-if="acting_as">
                    <div id="respond" class="comment-respond">
                        <p class="logged-in-as">Comentant com a {{ acting_as }}.</p>
                        <p class="comment-form-comment">
                            <label for="comment">Comentari</label>
                            <textarea placeholder="Comentari" id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required" v-model="text"></textarea>
                        </p>
                        <p class="form-submit">
                            <div v-if="success" class="alert alert-success"><p>{{ success }}</p></div>
                            <button @click="enviarComentari" name="submit" class="btn send" value="Enviar">Enviar <span v-if="loadingButton" class="right"><i class="fa fa-spinner fa-spin fa-fw"></i></span></button>
                        </p>
                    </div>
                </div>
                <p v-if="! acting_as">Per a fer un comentari has d'estar identificat com a usuari.<br>
                <a href="http://9club.el9nou.cat/<?php echo edicio_actual() ?>/entra?redirect=<?php the_permalink() ?>">Entra</a> o <a href="http://9club.el9nou.cat/<?php echo edicio_actual() ?>/registre">registra't</a></p>
            </div>
        </div>
    </comments>
</section>