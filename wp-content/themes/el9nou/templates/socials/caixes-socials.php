<?php if (edicio_actual() == 'osona-ripolles') : ?>    
    <a class="twitter-timeline" data-height="420" data-link-color="#c4003e" href="https://twitter.com/el9nou"></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    
    <div class="fb-page" data-href="https://www.facebook.com/el9nou/?fref=ts" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" style="margin-bottom: 20px;"><blockquote cite="https://www.facebook.com/el9nou/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/el9nou/?fref=ts">EL9NOU.CAT Osona</a></blockquote></div><?php 
else : ?>
    <a class="twitter-timeline" data-height="420" data-link-color="#c4003e" href="https://twitter.com/EL9NOU_VOr"></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

    <div class="fb-page" data-href="https://www.facebook.com/el9nouVOR/?fref=ts" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" style="margin-bottom: 20px;"><blockquote cite="https://www.facebook.com/el9nouVOR/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/el9nouVOR/?fref=ts">El 9 Nou Edició Vallès Oriental</a></blockquote></div><?php 
endif ?>