<div class="most-viewed-news hidden-xs hidden-sm">
    <h1>Les notícies més vistes <i class="fa fa-bookmark" aria-hidden="true"></i></h1>
    <div class="news-items"><?php
        $request = wp_remote_get('http://el9nou.cat/wp-json/api/el9nou/analytics?edicio=' . edicio_actual());
        $posts = json_decode( wp_remote_retrieve_body( $request ) );

       foreach($posts as $post) : ?>
            <a href="<?php echo $post->permalink ?>">
                <div class="news">
                    <div class="icon">
                        <i class="icon9icons-<?php echo $post->icon ?>"></i>
                    </div>
                    <h2><?php echo $post->post_title ?></h2>
                </div>
            </a><?php
        endforeach; ?>
    </div>
    <div class="gradient"></div>
</div>