<?php
$seccions = new WP_Query([
    'post_type' => 'seccio_radio',
    'posts_per_page' => 9,
    'orderby' => 'rand',
]); ?>

<div class="bloc-9fm">
    <a href="/el9fm/"><img class="logo" src="http://el9nou.cat/wp-content/themes/el9nou/img/el9fm.svg"></a>

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox"><?php
            while ( $seccions->have_posts() ) : $seccions->the_post(); ?>
                <div class="item <?php if ($seccions->current_post == 0) : echo active; endif ?>">
                    <a href="<?php the_permalink() ?>">
                        <img src="http://el-9-nou.dev/wp-content/uploads/2016/09/El-9-FM_Cinema.jpg" alt="...">
                        <?php //the_post_thumbnail() ?>
                        <div class="carousel-caption"><?php the_title() ?></div>
                    </a>
                </div><?php
            endwhile ?>
        </div>

        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>

    <div class="banner el9fm">
        <figcaption class="fm"><a class="9tv" href="/el9fm/"></a><a href="/el9fm/">EN DIRECTE</a> | <a href="/el9fm/programacio/">PROGRAMACIÓ</a></figcaption>
    </div>
</div>
