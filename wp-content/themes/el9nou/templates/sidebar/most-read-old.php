<div class="most-viewed-news hidden-xs hidden-sm">
    <h1>Les notícies més vistes <i class="fa fa-bookmark" aria-hidden="true"></i></h1>
    <div class="news-items"><?php
        $popular = wpp_get_mostpopular('post_type="post"&range="monthly"&limit=30');
        $popular = explode(',', $popular);
        $popularposts = new WP_Query(array('post__in' => $popular, 'posts_per_page' => 10, 'ignore_sticky_posts'   => 1, 'tax_query' => edicio_query_per_llistats()));
        if ( $popularposts->have_posts() ) :
            while ($popularposts->have_posts()) : $popularposts->the_post(); ?>
                <a href="<?php the_permalink() ?>">
                    <div class="news">
                        <div class="icon">
                            <i class="icon9icons-<?php echo post_category_for_icon() ?>"></i>
                        </div>
                        <h2><?php the_title() ?></h2>
                    </div>
                </a><?php
            endwhile;
        endif; ?>
    </div>
    <div class="gradient"></div>
</div>