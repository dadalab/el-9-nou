<div class="col-md-6 col-sm-6 col-xs-12 noticia no-imatge">
    <article>
    <figure><img style="max-width:1px !important; height:auto !important;" width="1" height="0" src="<?php echo get_template_directory_uri() ?>/img/white-space.gif" class="img-responsive wp-post-image" alt=""></figure>
        <header>
            <h2 class="avant-title"><?php the_post_category_child() ?></h2>
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>            
        </header>                                    
        <p class="sumari"><?php echo get_the_excerpt(); ?></p>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
            <li class="sharer"><?php the_news_sharing() ?></li>
        </ul>
    </article>
</div>        