<div class="col-md-12 noticia sencera">
    <article class="destacada">
        <header>
            <h2 class="avant-title"><?php the_post_category_child() ?></h2>
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
        </header>   


            <?php
            $value = get_field( "video" );

            if( $value ) {  ?>

                <figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank-large.png" class="img-responsive wp-post-image" alt=""><figcaption><?php the_thumbnail_author() ?></figcaption><div class="video-hover"><i class="icon-play" aria-hidden="true"></i></div></a></figure>

            <?php } else { ?>
                    
                    <figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank-large.png" class="img-responsive wp-post-image" alt=""></a><figcaption><?php the_thumbnail_author() ?></figcaption></figure>
            <?php     
            }
            ?>

        <?php /*
        <figure><a href="<?php the_permalink() ?>"><?php el9nou_post_thumbnail() ?></a><figcaption><?php the_thumbnail_author() ?></figcaption><div class="video-hover"><i class="icon-play" aria-hidden="true"></i></div></figure>      
        */ ?>             

        <p class="sumari" style="margin-top:20px;"><?php echo get_the_excerpt(); ?></p>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
            <li class="sharer"><?php the_news_sharing() ?></li>
        </ul>
    </article>
</div>                            