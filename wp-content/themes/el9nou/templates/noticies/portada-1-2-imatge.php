<div class="col-md-6 col-sm-6 col-xs-12 noticia noticia-imatge">
    <article>
        <?php if (get_field('es_premium')) : ?>
            <?php echo get_template_part('templates/noticies/snippet', 'premium'); ?>
        <?php endif ?>

    <?php
    $value = get_field( "video" );

    if( $value ) {

        //echo $value;
        if (has_post_thumbnail()) : ?><a href="<?php the_permalink() ?>"><figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""><figcaption><?php the_thumbnail_author() ?></figcaption><div class="video-hover"><i class="icon-play" aria-hidden="true"></i>
</div></figure></a><?php endif ?> <?php  ?>

    <?php } else {

        if (has_post_thumbnail()) : ?><a href="<?php the_permalink() ?>"><figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""><figcaption><?php the_thumbnail_author() ?></figcaption></figure></a><?php endif ?> <?php
    }
    ?>

        <header>
            <h2 class="avant-title"><?php the_post_category_child() ?></h2>
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
            <div class="img-responsive"></div>
        </header>
        <p class="sumari"><?php echo get_the_excerpt(); ?></p>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
            <li class="sharer"><?php the_news_sharing() ?></li>
        </ul>
    </article>
</div>