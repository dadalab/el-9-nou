<?php
if (get_field('video')) : ?>
	<iframe src="<?php the_field('video') ?>" class="video-news" frameborder="0" scrolling="no" allowfullscreen></iframe><?php
elseif (has_post_thumbnail()) : ?>
	<div class="owl-carousel-noticia">
        <figure>
            <div class="item"><?php the_post_thumbnail() ?></div>
            <figcaption><?php the_thumbnail_title() ?> | <span><?php the_thumbnail_author() ?></span></figcaption>
        </figure><?php
        $attachments = new Attachments( 'attachments' );
        if ($attachments->total()) :
			while ( $attachments->get() ) : ?>
			<figure>
		  		<div class="item"><?php echo $attachments->image( 'full' ); ?></div>
		        <figcaption><?php echo $attachments->field( 'title' ); ?> | <span><?php echo $attachments->field( 'caption' ); ?></span></figcaption>
		  	</figure><?php
			endwhile;
		endif; ?>
    </div>
<?php else : ?>
	<hr>
<?php endif ?>