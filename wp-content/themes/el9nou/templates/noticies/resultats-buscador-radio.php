<div class="col-md-12 col-sm-12 noticia">
    <article>
        <header>                         
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
            <a href="<?php the_permalink() ?>" class="pull-right">Veure tots</a>
        </header>

        <p>L'última emissió</p>
        <iframe width="100%" height="130" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/<?php the_field('llista_id') ?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
    </article>
    <hr>
</div> 