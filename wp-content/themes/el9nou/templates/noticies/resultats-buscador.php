<div class="col-md-12 col-sm-12 noticia">
    <article>
        <header>   
            <span class="date"><?php echo get_the_date('d/m/Y') ?></span>                                    
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
        </header>                                    
        <?php if (! has_post_thumbnail()) : ?><p class="sumari"><?php echo wp_trim_words(get_the_excerpt(), 15) ?></p><?php endif ?>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
        </ul>
    </article>
</div> 