<div class="col-md-6 col-sm-6 col-xs-12 noticia">
    <article><?php
        if (has_post_thumbnail()) : ?>
            <figure class="thumb"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a></figure>
            <div class="content"><?php
	    elseif( has_category( 'opinio', $post ) ) :
        	$authors = get_coauthors(get_the_ID()); ?>
            <figure class="thumb"><a href="<?php the_permalink() ?>"><img width="150" height="150" class="attachment-thumbnail size-thumbnail wp-post-image" src="<?php echo get_author_image_url($authors[0]->ID) ?>"></a></figure>
            <div class="content"><?php 
        endif; ?>

        <header>
            <span class="date"><?php echo get_the_date('d/m/Y') ?></span>
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
        </header>

        <?php if (! has_post_thumbnail() && ! has_category( 'opinio', $post )) : ?><p class="sumari"><?php echo wp_trim_words(get_the_excerpt(), 15) ?></p><?php endif ?>
        <?php
        if (has_post_thumbnail() || has_category( 'opinio', $post )) : ?>
            </div><?php
        endif; ?>
        <div class="clear visible-xs"></div>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>           
            <li class="sharer"><?php the_news_sharing() ?></li>            
        </ul>
    </article>
</div> 