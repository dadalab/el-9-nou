<article class="destacada bloc not-full" <?php if ( is_front_page() ) : ?>style="margin-bottom: 0;" <?php endif ?>>
    <?php if (get_field('es_premium')) : ?>
        <?php echo get_template_part('templates/noticies/snippet', 'premium'); ?>
    <?php endif ?>
    <div class="alsada">
        <div class="col-lg-4 col-md-6 col-sm-6 noticia">

            <?php
            $value = get_field( "video" );

            if( $value ) {  ?>

                <figure class="visible-xs" style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""><figcaption><?php the_thumbnail_author() ?></figcaption><div class="video-hover"><i class="icon-play" aria-hidden="true"></i></div></a></figure>

            <?php } else { ?>

                    <figure class="visible-xs" style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""></a><figcaption><?php the_thumbnail_author() ?></figcaption></figure>
            <?php
            }
            ?>

            <header>
                <h2 class="avant-title is-home"><?php the_post_category_child() ?></h2>
                <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
            </header>
            <p class="sumari"><?php echo get_the_excerpt(); ?></p>
            <ul class="info-article">
                <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
                <li class="sharer"><?php the_news_sharing() ?></li>
            </ul>
        </div>
        <div class="col-lg-8 col-md-6 col-sm-6 noticia hidden-xs">

            <?php
            $value = get_field( "video" );

            if( $value ) {  ?>

                <figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""><figcaption><?php the_thumbnail_author() ?></figcaption><div class="video-hover"><i class="icon-play" aria-hidden="true"></i></div></a></figure>

            <?php } else { ?>

                    <figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><a href="<?php the_permalink() ?>"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""></a><figcaption><?php the_thumbnail_author() ?></figcaption></figure>
            <?php
            }
            ?>

        </div>
    </div>
</article>