<div class="col-md-4 col-sm-4 col-xs-12 item article-imatge">                                                                                       
	<a href="<?php the_permalink() ?>">
        <figure class="row" style="background-image: url('<?php echo the_post_thumbnail_url("9nou-medium") ?>');-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;background-repeat: no-repeat; background-position: 50% 50%;">
        	<?php // the_post_thumbnail() ?>
        </figure>      
        <h1><?php the_title() ?></h1>
    </a>
</div>
