<div class="col-md-4 col-sm-4 col-xs-12 noticia">
    <article><?php
	    
        if ( has_post_thumbnail() ) : ?>
        <a href="<?php the_permalink() ?>"><figure style="background-image: url('<?php the_post_thumbnail_url('medium') ?>'); background-size:cover;"><img width="900" height="598" src="<?php echo get_template_directory_uri() ?>/img/blank.png" class="img-responsive wp-post-image" alt=""></figure></a><?php
        elseif( has_category( 'opinio', $post ) ) :
        	$authors = get_coauthors(get_the_ID()); ?>
            <figure style="height: 80px;width: 80px;overflow: hidden;background-size: cover;background-position: center center;background-image: url('<?php echo get_author_image_url($authors[0]->ID) ?>')"></figure><?php 
        endif; ?>

        <header>
            <span class="date"><?php echo get_the_date('d/m/Y') ?></span>
            <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
        </header>                                    
        <p class="sumari"><?php
            $limit = (has_post_thumbnail()) ? 15 : 100;
            echo wp_trim_words(get_the_excerpt(), $limit) ?>
        </p>
        <ul class="info-article">
            <li class="author"><span><?php get_the_authors() ?></span> <?php the_comments_link() ?></li>
            <li class="sharer"><?php the_news_sharing() ?></li>
        </ul>
    </article>
</div>