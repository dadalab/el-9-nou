<?php
    $posts = new WP_Query( [
        'category_name' => 'opinio',
        'posts_per_page' => 3,
        'tax_query' => edicio_query_per_llistats(),
    ] );

    if ( $posts->have_posts() ) : ?>

        <div id="opinio">
            <section class="articles-petits opinio" <?php if (edicio_actual() == 'valles-oriental') : ?> style="background-color: #008c6e" <?php endif ?>>
                <a href="/<?php echo edicio_actual() ?>/seccio/opinio"><h3>OPINIÓ <i class="fa fa-star" aria-hidden="true" <?php if (edicio_actual() == 'valles-oriental') : ?> style="color: #008c6e" <?php endif ?>><span>prèmium</span></i></h3></a>
                <div class="row flex-row"><?php
                    while ( $posts->have_posts() ) : $posts->the_post(); ?>
                        <div class="col-md-3 col-xs-12 flex-element" <?php if (edicio_actual() == 'valles-oriental') : ?> style="border-color: #fff" <?php endif ?>>
                            <?php $authors = get_coauthors(get_the_ID()); ?>
                            <figure style="background-image: url('<?php echo get_author_image_url($authors[0]->ID) ?>')"></figure>
                            <div class="text-opinio">
                                <h5><?php get_the_authors() ?></h5>
                                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                            </div>
                        </div><?php
                    endwhile; ?>

                </div>
            </section>
        </div><?php
        wp_reset_postdata();
    endif; ?>

