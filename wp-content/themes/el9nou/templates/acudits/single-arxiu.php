<?php
//$link = get_field('portada_vella') ? 'http://s3-eu-west-1.amazonaws.com/el9nou/' . get_field('portada_vella') : get_field('enllaç_portada');
?>
<figure class="acudit-item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
	<a href="<?php echo (the_post_thumbnail_url()) ?>" itemprop="contentUrl" data-size="1024x1024"><?php the_post_thumbnail() ?></a>
	<h5 class="data"><?php echo get_the_date() ?></h5>
	<div class="share"><?php the_news_sharing() ?></div>
</figure>
	
