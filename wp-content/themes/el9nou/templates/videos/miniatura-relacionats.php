<div class="col-md-3 col-sm-6">
	<div class="video-element">
		<article>
			<figure>
				<a href="<?php the_permalink() ?>">
					<div class="play"><i class="icon-play"></i></div>	
					<div class="bg-capt"></div>												
					<?php the_post_thumbnail() ?>
				</a>
			</figure>
			<header>
				<span class="data"><?php echo get_the_date() ?></span>
				<a class="titol" href="<?php the_permalink() ?>"><?php the_title() ?></a>
				<a href="<?php echo get_permalink(get_post_meta(get_the_ID(), 'programa', true)) ?>">
					<span class="programa"><?php echo get_the_title(get_post_meta(get_the_ID(), 'programa', true)) ?></span></a>
				<div class="autor"><?php // get_the_authors() ?></div>
			</header>
		</article>
	</div>
</div>