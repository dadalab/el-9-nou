<div id="navegation-site" class="overlay">
    <div class="container" style="position: relative;">

        <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
          </span>
      </button>

      <!-- logo -->
      <a class="navbar-brand" href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou.cat-white.svg"></a>

      <!-- search -->
      <div class="search-btn"><i class="icon-lupa"></i></div>

      <div class="clear"></div>

      <div class="row">
        <div class="col-md-8">
            <hr class="linia">
            <div class="row list-options"><?php
            if (strpos($_SERVER["REQUEST_URI"], '/el9fm/') !== false) :  ?>
            <div class="col-md-3 col-sm-3 this-options hidden-xs">
                <a class="brand fm" href="<?php url_site('el9fm') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9fm-white.svg"></a>
            </div>
            <div class="col-md-9 col-sm-9">
                <ul class="menu-large">
                    <li>EL 9 FM</li>
                    <li><a href="<?php url_site('el9fm') ?>">En directe</a></li>
                    <li><a href="<?php url_site('el9fm') ?>programacio">Programació</a></li>
                    <li><a href="<?php url_site('el9fm') ?>hora-de-la-veritat">L'Hora de la veritat</a></li>
                    <li><a href="<?php url_site('el9fm') ?>seccions">Seccions</a></li>
                </ul>
            </div>
            <div class="col-md-12 col-sm-12">
                <hr class="linia hidden-xs">
                </div><?php
                elseif (strpos($_SERVER["REQUEST_URI"], '/el9tv/') !== false) : ?>
                <div class="col-md-3 col-sm-3 this-options hidden-xs">
                    <a class="brand" href="<?php url_site('el9tv') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9tv.svg"></a>
                </div>
                <div class="col-md-9 col-sm-9">
                    <ul class="menu-large">
                        <li>EL 9 TV</li>
                        <li><a href="<?php url_site('el9tv') ?>">En directe</a></li>
                        <li><a href="<?php url_site('el9tv') ?>programacio">Programació</a></li>
                        <li><a href="<?php url_site('el9tv') ?>programes">Programes</a></li>
                        <li><a href="<?php url_site('el9tv') ?>a-la-carta">A la carta</a></li>
                    </ul>
                </div>
                <div class="col-md-12 col-sm-12">
                    <hr class="linia hidden-xs">
                    </div><?php
                    endif ?>
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options"><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat">ACTUALITAT</a></h2>
                        <ul class="menu selected">
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/politica">Política</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/societat">Societat</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/economia">Economia</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat">Veure totes</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options"><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports">ESPORTS</a></h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/futbol">Futbol</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/hoquei-patins">Hoquei patins</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/motor">Motor</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports">Veure totes</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options"><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent">CULTURA</a></h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/arts-esceniques">Arts escèniques</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/cultura-popular">Cultura popular</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/llibres">Llibres</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/calaix">Calaix</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent">Veure totes</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <h2 class="options">EL 9 NOU</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('fotogaleries') ?>/<?php echo edicio_actual() ?>">Fotogaleries</a></li>
                            <li><a href="<?php url_site('9magazin') ?>">9magazín</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/osona-ripolles/hemeroteca/">Portada paper Osona</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/valles-oriental/hemeroteca/">Portada paper Vallès</a></li>
                            <li class="visible-xs"><a href="http://club.el9nou.cat/pdf_club.php" target="_blank">Edició paper SUBSCRIPCIONS</a></li>
                            <li class="digues-la-teva"><a href="#">Digues la teva</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 visible-xs">
                        <h2 class="no-plus"><a href="<?php url_site('fotogaleries') ?>/<?php echo edicio_actual() ?>">Fotogaleries</a></h2>
                        <h2 class="no-plus"><a href="<?php url_site('9magazin') ?>">9magazín</a></h2>
                        <h2 class="no-plus"><a href="<?php url_site('agenda') ?>">Agenda</a></h2>
                        <h2 class="no-plus"><a href="<?php url_site('blogosfera') ?>">Blogosfera</a></h2>
                    </div>
                </div><!-- /row -->

                <hr class="hidden-xs">

                <div class="row list-options">
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <h2 class="options">AGENDA</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('agenda') ?>">Portada</a></li>
                            <li><a href="<?php url_site('agenda') ?>">Què és</a></li>
                            <li><a href="<?php url_site('agenda') ?>">Alta promotor</a></li>
                            <li><a href="<?php url_site('agenda') ?>">Accés promotor</a></li>
                            <li><a href="<?php url_site('agenda') ?>">Contacte</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <h2 class="options">BLOGOSFERA</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('blogosfera') ?>">Portada</a></li>
                            <li><a href="<?php url_site('blogosfera') ?>/que-es/">Què és</a></li>
                            <li><a href="<?php url_site('blogosfera') ?>/llistat-de-blogs/">Llistat blocs</a></li>
                            <li><a href="<?php url_site('blogosfera') ?>/insereix-un-nou-bloc/">Inscriu el teu bloc</a></li>
                            <li><a href="<?php url_site('blogosfera') ?>/contacte/">Contacte</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 visible-xs">
                        <h2 class="options">SERVEIS</h2>
                        <ul class="menu temps">
                            <li><a href="<?php url_site('el9nou') ?>/serveis/el-temps">El temps</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/defuncions">Defuncions</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <h2 class="options">EL 9 FM</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9fm') ?>">En directe</a></li>
                            <li><a href="<?php url_site('el9fm') ?>hora-de-la-veritat/">L'Hora de la v.</a></li>
                            <li><a href="<?php url_site('el9fm') ?>programacio/">Programació</a></li>
                            <li><a href="<?php url_site('el9fm') ?>seccions/">Seccions</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <h2 class="options">EL 9 TV</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9tv') ?>">En directe</a></li>
                            <li><a href="<?php url_site('el9tv') ?>programacio/">Programació</a></li>
                            <li><a href="<?php url_site('el9tv') ?>programes/">Programes</a></li>
                            <li><a href="<?php url_site('el9tv') ?>a-la-carta/">A la carta</a></li>
                        </ul>
                    </div>

                </div><!-- / row -->

                <!-- GRUP CORPORATIU -->
                <div class="grup-corporatiu visible-xs">

                    <h2 class="options minus2">GRUP EL 9 NOU</h2>
                    <hr class="mobile-bottom">
                    <div class="row logos-corporative ">
                        <div class="col-md-6 col-sm-6 col-xs-12 nou-cat">
                            <a href="<?php url_site('el9nou') ?>/osona-ripolles"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou-overlay.svg"></a><span><a href="<?php url_site('el9nou') ?>/osona-ripolles">OSONA I RIPOLLÈS</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 nou-cat">
                            <a href="<?php url_site('el9nou') ?>/valles-oriental"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou-overlay-valles.svg"></a>
                            <span><a href="<?php url_site('el9nou') ?>/valles-oriental">VALLÈS ORIENTAL</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9tv">
                            <a href="<?php url_site('el9tv') ?>"><img src="<?php echo url_site("el9nou") ?>/wp-content/themes/el9nou/img/9TV-overlay.png"></a>
                            <span><a href="<?php url_site('el9tv') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9tv') ?>/a-la-carta">A LA CARTA</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9fm">
                            <a href="<?php url_site('el9fm') ?>"><img src="<?php echo url_site("el9nou") ?>/wp-content/themes/el9nou/img/9FM-overlay.png"></a>
                            <span><a href="<?php url_site('el9fm') ?>">EN DIRECTE</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9clics logo">
                            <a href="<?php url_site('9clics') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/9clics-overlay.png"></a>
                            <span><a href="<?php url_site('9clics') ?>">DIRECTORI | OFERTES</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-club">
                            <a href="<?php enllac_club() ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/9-club-subscriptor.jpg"></a>
                        </div>
                    </div>
                </div><!-- / grup-corporatiu -->

                <div class="row list-options visible-xs">
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options" style="background-color: #111111;">EDICIÓ IMPRESA</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9nou') ?>/osona-ripolles/hemeroteca/">Portada edició impresa Osona</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/valles-oriental/hemeroteca/">Portada edició impresa Vallès</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/hemeroteca"> Descàrrega pdf edició impresa </a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options" style="background-color: #111111;">PREMSA D’OSONA</h2>
                        <ul class="menu">
                            <li><a href="<?php url_site('el9nou') ?>/grup/qui-som">Qui som</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/grup/on-som">On som</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/grup/codi-deontologic">Codi deontològic</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/grup/premis">Premis</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <h2 class="options" style="background-color: #111111;">PUBLICITAT</h2>
                        <ul class="menu">
                            <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9noucat.pdf">Tarifes EL9NOU.CAT</a></li>
                            <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9nou-conjunta-2016-17.pdf">Tarifes EL 9 NOU</a></li>
                            <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9tv-2016-17.pdf">Tarifes EL 9 TV</a></li>
                            <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9fm-2016-17.pdf">Tarifes EL 9 FM</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/contacte/publicitat">Contacte</a></li>
                        </ul>
                    </div>
                </div>

                <hr class="hidden-xs">

            </div><!-- / col-md-8 -->

            <!-- columna dreta -->
            <div class="col-md-4">

                <!-- columna 2 -->
                <div class="el-temps text-center" id="temps-vue">

                    <h2><a href="<?php url_site('el9nou') ?>/serveis/el-temps">EL TEMPS</a> |
                        <input type="text" v-model="locationTemps" class="location-field" name="location" v-show="canviCiutat">
                        <span v-show="! canviCiutat" class="location">{{ locationTemps }}</span>
                        <i v-show="! canviCiutat" v-on:click="canviCiutat = 1" class="fa fa-pencil" aria-hidden="true"></i>
                        <i v-show="canviCiutat" v-on:click="buscaCiutat" class="fa fa-check" aria-hidden="true"></i>
                    </h2>

                    <hr style="border-color: #000;">

                    <div class="row">

                        <!-- el temps -->
                        <a href="<?php url_site('el9nou') ?>/serveis/el-temps">
                            <div class="col-md-4 col-sm-4 temps" id="temps-avui">
                                <div class="imatge"></div>
                                <span class="dia">Avui</span>
                                <span class="temperatura-max"></span>
                                <span class="temperatura-min"></span>
                            </div>
                            <div class="col-md-4 col-sm-4 temps" id="temps-dema">
                                <div class="imatge"></div>
                                <span class="dia">Demà</span>
                                <span class="temperatura-max"></span>
                                <span class="temperatura-min"></span>
                            </div>
                            <div class="col-md-4 col-sm-4 temps" id="temps-dema-passat">
                                <div class="imatge"></div>
                                <span class="dia">Demà passat</span>
                                <span class="temperatura-max"></span>
                                <span class="temperatura-min"></span>
                            </div>
                        </a>
                    </div>
                    <div class="clear"></div>
                </div><!-- / el temps -->

                <!-- GRUP CORPORATIU -->
                <div class="grup-corporatiu hidden-xs">

                    <h2 class="options">GRUP EL 9 NOU</h2>
                    <hr class="mobile-bottom">
                    <div class="row logos-corporative">
                        <div class="col-md-6 col-sm-6 col-xs-12 nou-cat">
                            <a href="<?php url_site('el9nou') ?>/osona-ripolles"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou-overlay.svg"></a><span><a href="<?php url_site('el9nou') ?>/osona-ripolles">OSONA I RIPOLLÈS</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 nou-cat">
                            <a href="<?php url_site('el9nou') ?>/valles-oriental"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou-overlay-valles.svg"></a>
                            <span><a href="<?php url_site('el9nou') ?>/valles-oriental">VALLÈS ORIENTAL</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9tv">
                            <a href="<?php url_site('el9tv') ?>"><img src="<?php echo url_site("el9nou") ?>/wp-content/themes/el9nou/img/9TV-overlay.png"></a>
                            <span><a href="<?php url_site('el9tv') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9tv') ?>/a-la-carta">A LA CARTA</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9fm">
                            <a href="<?php url_site('el9fm') ?>"><img src="<?php echo url_site("el9nou") ?>/wp-content/themes/el9nou/img/9FM-overlay.png"></a>
                            <span><a href="<?php url_site('el9fm') ?>">EN DIRECTE</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-9clics logo">
                            <a href="<?php url_site('9clics') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/9clics-overlay.png"></a>
                            <span><a href="<?php url_site('9clics') ?>">DIRECTORI | OFERTES</a></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 logo-club">
                            <a href="<?php enllac_club() ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/9-club-subscriptor.jpg"></a>
                        </div>
                    </div>
                </div><!-- / grup-corporatiu -->

                <div class="social">
                    <h2 class="xarxes  mobile-bottom">XARXES SOCIALS</h2>
                    <hr class="mobile-bottom">
                    <ul class="compartir"><?php
                    if (edicio_actual() == 'valles-oriental') : ?>
                    <li><a href="https://www.facebook.com/el9nouVOR"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/EL9NOU_VOr"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <?php else : ?>
                    <li><a href="https://www.facebook.com/el9nou"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/el9nou"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/el9nou/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCObpBdpXZ2kEqtZh4EQXQrQ" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <?php endif ?>
            </ul>
        </div><!-- / social -->

    </div><!-- / col-md-4 -->
</div><!-- / row -->
</div><!-- / row -->
<div class="clear"></div>
</div>