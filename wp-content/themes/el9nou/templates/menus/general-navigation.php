<nav class="hidden-print">
    <div class="container">
        <ul class="sf-menu">
            <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/actualitat" class="st-with-ul">ACTUALITAT</a>
                <ul class="sub-menu">
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/actualitat/politica">Política</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/actualitat/societat">Societat</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/actualitat/economia">Economia</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/opinio">Opinió</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/actualitat">Veure totes</a></li>
                </ul>
            </li>
            <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports" class="st-with-ul">ESPORTS</a>
                <ul class="sub-menu">
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports/futbol">Futbol</a></li><?php 
                    if (edicio_actual() == 'valles-oriental') : ?>
                        <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports/handbol">Handbol</a></li><?php 
                    endif;
                    if (edicio_actual() == 'osona-ripolles') : ?>
                        <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports/hoquei-patins">Hoquei patins</a></li><?php 
                    endif ?>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports/motor">Motor</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/esports">Veure totes</a></li>
                </ul>
            </li>
            <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent" class="sf-with-ul">CULTURA</a>
                <ul class="sub-menu">
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/arts-esceniques">Arts escèniques</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/cultura-popular">Cultura popular</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/llibres">Llibres</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/calaix">Calaix</a></li>
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent">Veure totes</a></li>
                </ul>
            </li>
            <li><a href="<?php url_site('9magazin') ?>" target="_blank">9MAGAZÍN</a></li>
            <li><a href="<?php url_site('fotogaleries') ?>/<?php echo edicio_actual() ?>" target="_blank">FOTOGALERIES</a></li>
            <li><a href="<?php url_site('agenda') ?>" target="_blank">AGENDA</a></li>
            <li><a href="<?php url_site('blogosfera') ?>" target="_blank">BLOGOSFERA</a></li>
            <li><a href="#">SERVEIS</a>
                <ul class="sub-menu">                    
                    <li><a href="<?php echo site_url() ?>/serveis/el-temps">El temps</a></li> 
                    <li><a href="<?php echo site_url() ?>/<?php echo edicio_actual() ?>/defuncions">Defuncions</a></li> 
                    <?php /*
                    <li><a href="#">El trànsit</a></li>
                    <li><a href="#">Cartellera</a></li>                       
                    <li><a href="#">Horaris RENFE</a></li>            
                    <li><a href="#">Horari BUS</a></li>                                          
                    <li><a href="#">Adreces d’interès</a></li>            
                    <li><a href="#">Emergències</a></li> 
                    */
                    ?>
                </ul>
            </li>
        </ul>
    </div>
</nav>