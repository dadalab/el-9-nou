<div class="topbar-wrapp hidden-print ">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 hidden-xs text-left">
                <ul class="corporative">
                    <li class=""><?php echo edicio_obte_link_de_canvi() ?></li>
                    <li><a href="<?php url_site('9club') ?>/<?php echo edicio_actual() ?>">el9club</a></li>
                    <li class="hidden-sm hidden-xs"><a href="<?php url_site('el9tv') ?>">EL 9 TV</a></li>
                    <li class="hidden-sm hidden-xs"><a href="<?php url_site('el9fm') ?>">EL 9 FM</a></li>
                    <li class="hidden-sm hidden-xs"><a href="<?php url_site('9clics') ?>">9clics</a></li>
                </ul>
            </div>


			<div class="col-md-4 col-sm-4 col-xs-5 hidden-xs text-center">
				<div class="data" style="text-align: center;">
		             <esi:remove>
                            <?php echo date_i18n('j F Y', time()); ?>
                    </esi:remove>
                    <esi:include src="/wp-content/plugins/varnish-http-purge/regenerate-partials/date.php"/>
				</div>
			</div>


            <div class="col-md-4 col-sm-4 text-right user-menu">
                    <div id="user-logged-in">
                        <user-logged-in inline-template v-cloak>
                            <ul class="user" v-if=" ! loading ">
                                <li v-if="user"><a href="http://9club.el9nou.cat/usuari">Hola {{ user.nom }}!</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/<?php echo edicio_get_slug() ?>/registre">Registra't</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/<?php echo edicio_get_slug() ?>/entra?redirect=<?php current_url() ?>">Inicia sessió</a></li>
                                <li><a href="/<?php echo edicio_actual() ?>/hemeroteca">Edició impresa</a></li>
                            </ul>
                        </user-logged-in>
                    </div>

                 <?php /**   <esi:remove><?php
                        if (is_user_logged_in()) :
                            $current_user = wp_get_current_user(); ?>
                            <li><a href="<?php echo site_url() ?>/perfil">Hola <?php echo $current_user->user_firstname ?>!</a></li>
                            <li><a href="<?php echo wp_logout_url( site_url() ); ?>">Sortir</a></li><?php
                        else : ?>
                            <li><a href="<?php echo site_url() ?>/registre">Registra't</a></li>
                            <li><a href="<?php echo site_url() ?>/entra">Inicia sessió</a></li><?php
                        endif ?>
                    </esi:remove>

                    <esi:include src="/wp-content/plugins/varnish-http-purge/regenerate-partials/user-logged-in.php"/>
                **/ ?>

            </div>
            <a class="log-in hidden-xs" href="<?php echo site_url() ?>/entra"><i class="fa fa-user" aria-hidden="true"></i></a>
        </div>
    </div>
</div>