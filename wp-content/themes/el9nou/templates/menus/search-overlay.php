<div class="overlay-search hidden-print">    
    <div class="container">     
        <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url() ?>">
            <div class="col-md-11 col-sm-10 col-xs-10"><?php
                    $postType = isset($_GET['post_type']) ? $_GET['post_type'] : false;
                    if (strpos($_SERVER['REQUEST_URI'], "/el9tv/") !== false || strpos($_SERVER['REQUEST_URI'], "/video/") !== false || $postType == 'video') : ?>
                        <input type="text" name="s" placeholder="Cercar vídeos..." id="search" >
                        <input type="hidden" value="video" name="post_type" /><?php
                    elseif (strpos($_SERVER['REQUEST_URI'], "/el9fm/") !== false || $postType == 'seccio_radio') : ?>
                        <input type="text" name="s" placeholder="Cercar seccions..." id="search" >
                        <input type="hidden" value="seccio_radio" name="post_type" />           
                    <?php else : ?>
                        <input type="text" name="s" placeholder="Cercar notícies..." id="search" >
                        <input type="hidden" value="post" name="post_type" />           
                    <?php endif; ?>
            </div>
            <div class="col-md-1 col-sm-2 col-xs-2">
                <a href="#" onclick="document.forms['searchform'].submit(); return false;"><i class="icon-lupa"></i></a>
            </div>        
         </form>
       
        <div class="clear"></div>
        <div class="col-md-12"><hr></div>

		<?php /**
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-6 col-sm-6 search-left">
                    <h1>Recents</h1><?php
                    $recents = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'tax_query' => edicio_query_per_llistats()
                        )
                    );
                    while($recents->have_posts()) : $recents->the_post(); ?>
                        <div class="recent-news">
                            <figure class="thumb"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'thumbnail' ) ?></a></figure>
                            <div class="content">
                                <header>                                       
                                    <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
                                </header>                                             
                            </div>
                        </div><?php 
                    endwhile; ?>
                </div>

                <div class="col-md-6 col-sm-6">
                    <h1>Més vistes</h1><?php
                    $popular = explode(',', wpp_get_mostpopular('post_type="post"&range="weekly"'));
                    $populars = new WP_Query(array('post__in' => $popular, 'ignore_sticky_posts' => 1, 'posts_per_page' => 3, 'tax_query' => edicio_query_per_llistats()));
                    
                    
                    
                    
                    while($populars->have_posts()) : $populars->the_post(); ?>
                        <div class="recent-news">
                            <figure class="thumb"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'thumbnail' ) ?></a></figure>
                            <div class="content">
                                <header>                                       
                                    <a href="<?php the_permalink() ?>"><h1 itemprop="headline"><?php the_title() ?></h1></a>
                                </header>                                             
                            </div>
                        </div><?php 
                    endwhile; ?>
                </div> ?>
            </div>
            <div class="col-md-3">
               <!--  <figure class="banner"><a href="http://www.9clics.cat/" target="_blank"><img src="< ?php echo get_template_directory_uri() ?>/img/9clics.jpg"></a></figure> -->
            </div>       
        </div> **/ ?>
    </div>
</div>