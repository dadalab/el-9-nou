<div class="overlay-edition">              
    <div class="left parallax" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/osona-ripolles.jpg')">
        <a href="#" class="select-edition">
            <div class="bg-white"></div>
            <div class="cc">
                <img src="<?php echo get_template_directory_uri() ?>/img/el9nou-osona-ripolles.svg">
                <span>Edició Osona i Ripollès</span>
            </div>
        </a>
    </div>
    <div class="center"><h1>Tria la teva edició predeterminada</h1></div>

    <div class="right" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/valles-oriental.jpg')">
        <a href="#" class="select-edition">
            <div class="bg-white"></div>
            <div class="cc">
                <img src="<?php echo get_template_directory_uri() ?>/img/el9nou-valles-oriental.svg">
                <span>Edició Vallès Oriental</span>
            </div>
        </a>
    </div>

</div>