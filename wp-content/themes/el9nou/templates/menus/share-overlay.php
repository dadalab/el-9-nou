<!-- Share Overlay -->
<div class="overlay-share hidden-print">              
     <div class="overlayInner">
         <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
              <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
              </span>
         </button>
         <ul>
             <li>Compartir</li>
             <input type="hidden" id="url_to_share">
             <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" class="share-now-button" data-share-url="https://www.facebook.com/sharer/sharer.php?u="><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
             <li><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink() ?>" class="share-now-button" data-share-url="https://twitter.com/intent/tweet?url="><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
             <?php if (wp_is_mobile()) : ?>
             <li><a href="#" class="share-now-button" data-share-url="whatsapp://send?text="><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a></li> 
             <?php endif ?>                                         
         </ul>
     </div>
</div>
<!-- / Share Overlay -->