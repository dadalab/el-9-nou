<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo wp_title() ?> | EL9NOU.CAT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script src="https://use.fontawesome.com/f0cbaa2dc7.js"></script>

        <meta name="robots" content="INDEX,FOLLOW,NOODP">
		<meta name="google-site-verification" content="5IDL0iqBCq0ihG6_8prRFwKTX06f8DgDSHucOETPmGg" />

        <?php if (edicio_actual() == 'osona-ripolles') : ?>
            <link rel="shortcut icon" href="<?php echo site_url() ?>/favicon-el9nou-osona-ripolles.cat.ico" />
            <link type="image/ico" href="<?php echo site_url() ?>/favicon-el9nou-osona-ripolles.cat.ico" rel="icon" />
        <?php else : ?>
            <link rel="shortcut icon" href="<?php echo site_url() ?>/favicon-el9nou-valles-oriental.cat.ico" />
            <link type="image/ico" href="<?php echo site_url() ?>/favicon-el9nou-valles-oriental.cat.ico" rel="icon" />
        <?php endif ?>

		<?php wp_head() ?>

<!--<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101096417-1', 'auto');
  ga('send', 'pageview');

</script>-->

        <?php analytics_code() ?>

        <?php ads_header_tags() ?>

    </head>

<body <?php echo body_class(); ?> <?php if (edicio_actual() == 'osona-ripolles') : ?> id="skinBanner" <?php endif ?>>
	
    <?php google_tag_manager() ?>
    
    <?php 
    /*
	    if ($_SERVER['HTTP_X_FORWARDED_FOR'] == "85.57.173.84"){
    ?>
       <div class="add_web_home">
       	<div class="add_home_content" onclick="closeAddHome();">
       		<div class="close">X</div>
       		<img class="logo_add_home" src="<?php echo get_template_directory_uri().'/img/el9nou.png' ?>">
       		<div class="missatge_android">
	       		<span>Per afegir aquesta web a la pantalla d'inici, obre les opcions i prem <strong>Afegir a la pantalla d'inici</strong>.</span>
	       		<br/>
	       		<span class="small">El menú és accessible prement el botó tàctil en cas de tenir-lo o bé, l'icona de la part superior dreta de la pantalla
	       		<img src="<?php echo get_template_directory_uri().'/img/punts_menu.png' ?>">.</span>
       		</div>
			<div class="missatge_android hide">
	       		<span>Per afegir aquesta web a la pantalla d'inici: prem i selecciona <strong>Afegir a la pantalla d'inici</strong>.</span>
       		</div> 
       	</div>      		
       </div>
       <script>
       	function closeAddHome(){
       		$('.add_web_home').hide();
       	}
       </script>
    <?php
	    }
	    */
//    	echo $_SERVER['HTTP_X_FORWARDED_FOR'];
    	
    	/*
    	public function getRemoteIPAddress() {
		    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		        return $_SERVER['HTTP_CLIENT_IP'];
		
		    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
		        return $_SERVER['HTTP_X_FORWARDED_FOR'];
		    }
		    return $_SERVER['REMOTE_ADDR'];
		}
		echo "abc";		
		echo getRemoteIPAddress();
		*/
    
    ?>
    
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ca_ES/sdk.js#xfbml=1&version=v2.7&appId=155748861168651";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- /119069366/skinbàner -->
        <!--<img src="https://tpc.googlesyndication.com/pagead/imgad?id=CICAgKCz1_-rowEQmBEYuAgoATIIkEPsdhk7ASI">-->
    <div id='div-gpt-ad-1508740757417-0' class="skin-banner" style='height:1080px; width:2200px;position: absolute;left: 0;right: 0;margin:auto;z-index: -1'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1508740757417-0'); });
        </script>
    </div>

<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<?php /** Selector d'edició **/ ?>
<?php get_template_part('templates/menus/general', 'edicio'); ?>


<?php /** Overlay general **/ ?>
<?php get_template_part('templates/menus/general', 'overlay'); ?>
<div style="width: 1170x;margin:auto">


<?php /*
<!-- /119069366/skinbàner -->
    <div id='div-gpt-ad-1508740757417-0' class="skin-banner">
        <script>
         //   googletag.cmd.push(function() { googletag.display('div-gpt-ad-1508740757417-0'); });
        </script>
    </div>
*/ ?>

<?php if (edicio_actual() == 'osona-ripolles') : ?>
<div class="box-content">
<?php endif; ?>


<?php /*
<!-- /119069366/skinbàner -->
    <div id='div-gpt-ad-1508740757417-0' class="skin-banner">
        <script>
         //   googletag.cmd.push(function() { googletag.display('div-gpt-ad-1508740757417-0'); });
        </script>
    </div>
*/ ?>

<?php if (edicio_actual() == 'osona-ripolles') : ?>
<div class="box-content">
<?php endif; ?>

<?php /** Top banner ad **/ ?>
<?php // ads_the_top_banner(); ?>

<div <?php if (0 && edicio_actual() == 'osona-ripolles') : ?>class="top-banner-sky" <?php endif ?>style="max-width:100%;">
	<?php ads_the_top_banner(); ?>
<!--	<img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/10/920x220_supeco_1.gif">-->
</div>

<?php /** Top menu **/ ?>
<?php get_template_part('templates/menus/general', 'top'); ?>


<div class="smaller hidden-print">
    <div class="container">
        <!-- menu-toggle -->
        <button class="hamburger hamburger--squeeze js-hamburger" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <!-- navbar-brand -->
        <a class="navbar-brand" href="<?php echo site_url() ?>"><?php edicio_the_logo() ?></a>
        <!-- search -->
        <div class="search-btn"><i class="icon-lupa"></i></div>
    </div>
</div>

<a class="navbar-brand visible-print" style="margin:auto; display:block; width:250px;" href="<?php echo site_url() ?>"><?php edicio_the_logo() ?></a>
<div class="clear visible-print"></div>

<?php /*
<div class="container">
*/ ?>
<header class="header hidden-print">
    <div class="container">
        <div class="header-wrapp">
            <!-- menu toggle -->
            <button class="hamburger hamburger--squeeze js-hamburger" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
            <!-- navbar-brand -->
            <a class="navbar-brand" href="<?php echo site_url() ?>"><?php edicio_the_logo() ?></a>
            <!-- search -->
            <div class="search-btn"><i class="icon-lupa"></i></div>
        </div><!-- / header-wrapp -->
    </div><!-- / container -->

    <?php /** General navigation menú **/ ?>
    <?php get_template_part('templates/menus/general', 'navigation'); ?>
</header>

<?php /*
</div>
*/ ?>


