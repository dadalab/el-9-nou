<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo wp_title() ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php wp_head() ?>

    <link rel="stylesheet" href="<?php echo url_site('el9nou'); ?>/wp-content/themes/el9nou/css/overlay-general.css">
    <script src="https://use.fontawesome.com/f0cbaa2dc7.js"></script>

    <?php analytics_code() ?>
</head>
<body class="el9tv">
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

<?php /** Overlay general **/ ?>
<?php get_template_part('templates/menus/general', 'overlay'); ?>


    <div style="background-color: #262626;">
        <div class="container">
            <div class="topbar-wrapp">            
                <div class="col-md-4 col-sm-4 hidden-xs text-left">
                    <ul class="corporative">
                        <li><a href="<?php url_site('el9nou') ?>">EL 9 NOU</a></li>
						<li><a href="<?php url_site('9club') ?>/<?php echo edicio_actual() ?>">el9club</a></li>
                        <li class="hidden-sm hidden-xs"><a href="<?php url_site('el9fm') ?>">EL 9 FM</a></li>
                        <li class="hidden-sm hidden-xs"><a href="<?php url_site('9clics') ?>">9clics</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center"><div class="data" style="text-align: center;"><?php echo date_i18n('j F Y', time()); ?></div></div>
                <div class="col-md-4 col-sm-4 text-right">
                	<div id="user-logged-in">
                        <user-logged-in inline-template v-cloak>
                            <ul class="user" v-if=" ! loading ">
                                <li v-if="user"><a href="http://9club.el9nou.cat/usuari">Hola {{ user.nom }}!</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/<?php echo edicio_get_slug() ?>/registre">Registra't</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/<?php echo edicio_get_slug() ?>/entra?redirect=<?php current_url() ?>">Inicia sessió</a></li>
                            </ul>
                        </user-logged-in>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="smaller">
        <div class="container">
                <!-- menu-toggle -->
                <button class="hamburger hamburger--squeeze js-hamburger" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
                <!-- navbar-brand -->
                <a class="navbar-brand" href="<?php url_site('el9tv') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9tv.svg"></a>
                <!-- search -->
                <div class="search-btn"><i class="icon-lupa"></i></div>
        </div>
    </div>

    <header class="header el9tv">
        <div class="container">
            <div class="header-wrapp">
                <!-- menu toggle -->
                <button class="hamburger hamburger--squeeze js-hamburger" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>

            <!-- navbar-brand -->
            <a class="navbar-brand" href="<?php url_site('el9tv') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9tv.svg"></a>

            <!-- search -->
            <div class="search-btn">
                <i class="icon-lupa"></i>
            </div>

        </div><!-- / header-wrapp -->
    </div><!-- / container -->

    <nav>
        <div class="container">
            <ul>
                <li class="col-md-4 col-sm-4 col-xs-12"><a href="<?php url_site('el9tv') ?>programacio">PROGRAMACIÓ</a></li>
                <li class="col-md-4 col-sm-4 col-xs-12"><a href="<?php url_site('el9tv') ?>programes">ELS PROGRAMES</a></li>
                <li class="col-md-4 col-sm-4 col-xs-12"><a href="<?php url_site('el9tv') ?>a-la-carta">EL 9 TV A LA CARTA</a></li>
            </ul>
        </div>
    </nav>
    <!-- / nav -->
</header>
<!-- header -->