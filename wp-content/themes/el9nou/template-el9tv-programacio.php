<?php
/**
 *
 *	Template name: PROGRAMACIÓ EL 9TV
 */
get_header('el9tv') ?>

<main id="main" role="main" class="main">
	<section class="video-stream">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1>La programació</h1>
				</div>
			</div>				
		</div>
	</section>

	<section class="programacio">		
		<div class="container" style="position: relative;">
			<div class="left"><div class="capa"></div></div>
			<div class="right"><div class="capa"></div></div>

			<div class="row"><?php
				$dies = array('diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte');
				$diesOrdenats = array();
				$dia = date('w');
				for ($i = 0; $i < 7; $i++) :
					$diesOrdenats[] = $dies[$dia];
					$dia++;
					if ($dia > 6) $dia = 0;
				endfor; ?>				
				<div class="col-md-12">
					<div class="titol">
						<h2>La programació d'EL 9 TV</h2>
						<div class="navigation-owl"> 
							<a href="#" class="left-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
							<a href="#" class="right-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>

					<div id="owl-demo"><?php
						$count = 0;
						foreach($diesOrdenats as $dia) :
							$avui = new WP_Query( array(
								'post_type' => 'programacio_tv',
								'name' => $dia,
							)); ?>
						  	<div class="item">
								<div class="border col-md-12">
									<div class="row">
										<h1><?php echo $dia . ' ' . date('d-m-Y', strtotime("+" . $count . " days")) ?></h1>
										<?php while ( $avui->have_posts() ) : $avui->the_post(); ?>
											<?php the_content() ?>
										<?php endwhile; ?>
									</div>
								</div>
					  		</div><?php
					  		$count++;
					  	endforeach; ?>
					</div>

					<div class="titol">
						<hr>
						<h3>Desplaçar-se cap als costats per a veure la resta de la programació</h3>			
						<div class="navigation-owl bottom"> 
							<a href="#" class="left-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
							<a href="#" class="right-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>				
</main>
<?php get_footer(); ?>