<?php
/**
 *
 *	Template name: Perfil Usuaris
 */
global $ads;
$ads = false;

get_header();
$currentUser = $current_user = wp_get_current_user(); ?>
<main id="main" role="main" class="main registre">
    <div class="container">
        <div class="row">
        	<div class="col-md-3">
				<h3>El teu perfil</h3><br>
                <b>Nom: </b><?php echo $currentUser->first_name ?><br>
                <b>Email: </b><?php echo $currentUser->user_email ?><br>
                <b>Gènere: </b><?php echo get_user_meta($currentUser->ID, 'genere', true);  ?><br>
                <b>Data Naixement: </b><?php echo get_user_meta($currentUser->ID, 'data', true);  ?><br>
                <b>Codi Postal: </b><?php echo get_user_meta($currentUser->ID, 'cp', true);  ?><br>
        	</div>
            <div class="col-md-9">
                <h3>Els teus comentaris</h3><br><?php
                $args = array(
                    'user_id' => $currentUser->ID,
                    'number' => '10'
                );
                $comments = get_comments($args);
                if (count($comments)) :
                    foreach ($comments as $comment) : ?>
                        <div class="row">
                            <div class="comment clearfix">
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="comment-news-thumbnail">
                                        <a href="<?php echo get_permalink($comment->comment_post_ID) ?>">
                                            <?php echo get_the_post_thumbnail($comment->comment_post_ID) ?>
                                        </a>
                                    </div>
                                    <h5><a href="<?php echo get_permalink($comment->comment_post_ID) ?>"> <?php echo get_the_title($comment->comment_post_ID) ?> </a></h5>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <?php if ($comment->comment_approved == '0') : ?>
                                        <span class="error">El teu comentari està pendent de moderació.</span>
                                    <?php endif; ?>
                                    <div class="nickname">
                                        <?php echo get_comment_date() ?> | <?php echo get_comment_time() ?>
                                    </div>
                                    <?php comment_text() ?>
                                </div>
                            </div>
                        </div><?php
                    endforeach;
                else : ?>
                <div class="well">Encara no has fet comentaris a cap notícia.</div><?php
                endif; ?>
            </div>
        </div>
	</div>
</main>

<?php get_footer(); ?>