<?php
/**
 *
 *	Template name: Digues la teva
 */
get_header() ?>


<main id="main" role="main" class="main">
	
		<div class="container" style="margin-top: 40px;">
			<div class="col-md-9">

   			<div class="barra-superior clearfix">
	            <div class="breadcrumb">Digues la teva</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
					Fes-nos arribar suggeriments, notícies, esmenes...
					
					<?php echo do_shortcode('[ninja_form id=8]') ?>
				</div><!-- / col-md-12 -->
	        </div><!-- / row -->

	</div>
    <?php get_sidebar() ?>
   </div>
</main>
<!-- / section -->


<?php get_footer(); ?>