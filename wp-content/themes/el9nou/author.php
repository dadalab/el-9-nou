<?php
global $ads;
$ads = false; ?>

<?php get_header(); ?>
<?php $author = get_userdata( get_query_var('author') );?>
<main id="main" role="main" class="main categoria author">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">Arxiu de: <?php echo $author->display_name ?></div>                    
                </div> 
                <div class="row">
                    <div class="author-content">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <img src="<?php echo get_author_image_url($author->ID) ?>">
                        </div>  
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <header><h1><?php echo $author->display_name ?></h1></header>                     
                            <p><?php echo $author->description ?></p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 socials">       
                            <h2>Les xarxes socials</h2>
                            <?php if ($twitter = get_user_meta( $author->ID, 'twitter', true )) : ?>
                                <a href="<?php echo $twitter ?>" target="_blank"><i class="icon-twitter-circled"></i></a>
                            <?php endif ?>
                            <?php if ($facebook = get_user_meta( $author->ID, 'facebook', true )) : ?>
                                <a href="<?php echo $facebook ?>" target="_blank"><i class="icon-facebook-circled"></i></a>
                            <?php endif ?>
                            <?php if ($instagram = get_user_meta( $author->ID, 'instagram', true )) : ?>
                                <a href="<?php echo $instagram ?>" target="_blank"><i class="icon-instagram-circled"></i></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
               <!-- <hr> -->
                <h3>Notícies publicades per <?php echo $author->display_name ?></h3>
                <section id="noticies-arxiu">
                    <div class="row">
                        <div class="bloc"><?php
                            if (have_posts()) :
                                while (have_posts()) : the_post();
                                    get_template_part('templates/noticies/categoria', 'arxiu');
                                endwhile; ?>
                                
                                <nav class="prev-next-posts">
                                    <div class="prev-posts-link">
                                        <?php echo get_next_posts_link('Notícies més antigues'); ?>
                                    </div>
                                    <div class="next-posts-link">
                                        <?php echo get_previous_posts_link('Notícies més recents'); ?>
                                    </div>
                                </nav><?php 
                            endif; ?>
                        </div>
                    </div>
                </section>
            </div>

            <?php get_sidebar() ?>
        </div>
    </div>
</main>

<?php get_footer() ?>