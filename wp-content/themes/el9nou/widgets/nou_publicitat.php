<?php 

// Creating the widget 
class nou_publicitat extends WP_Widget {

	function __construct() {
		parent::__construct('nou_publicitat', 'Publicitat', array( 'description' => 'Publicitat a la barra lateral' ) );
	}

	public function widget( $args, $instance ) { ?>
		<div class="anunci"><?php
			ads_mostra_anunci($instance['codi_osona'], $instance['codi_valles']);
			//echo (edicio_actual() == 'valles-oriental') ? $instance['codi_valles'] :  ?>
		</div><?php
	}
		
	// Widget Backend 
	public function form( $instance ) {
		$titol = isset( $instance[ 'titol' ] ) ? $instance[ 'titol' ] : 'Nom del bànner';
		$codi_osona = isset( $instance[ 'codi_osona' ] ) ? $instance[ 'codi_osona' ] : '';
		$codi_valles = isset( $instance[ 'codi_valles' ] ) ? $instance[ 'codi_valles' ] : ''; ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'titol' ); ?>">Títol</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'titol' ); ?>" name="<?php echo $this->get_field_name( 'titol' ); ?>" type="text" value="<?php echo esc_attr( $titol ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'codi_osona' ); ?>">Codi Osona</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'codi_osona' ); ?>" name="<?php echo $this->get_field_name( 'codi_osona' ); ?>" type="text" value="<?php echo esc_attr( $codi_osona ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'codi_valles' ); ?>">Codi Vallès</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'codi_valles' ); ?>" name="<?php echo $this->get_field_name( 'codi_valles' ); ?>" type="text" value="<?php echo esc_attr( $codi_valles ); ?>" />
		</p>
		
		<?php
	}
	
} // Class wpb_widget ends here