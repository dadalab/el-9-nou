<?php 

// Creating the widget 
class nou_hemeroteca_portada extends WP_Widget {

	function __construct() {
		parent::__construct('nou_hemeroteca_portada', 'Hemeroteca per la portada', array( 'description' => 'Widget per mostrar la última portada' ) );
	}

	public function widget( $args, $instance ) {
		if (nou_home()) : 
			$portada = new WP_Query(
				array(
					'post_type' => 'hemeroteca',
					'posts_per_page' => 1,
					'tax_query' => edicio_query_per_llistats()
				)
			);
			while ($portada->have_posts()) : $portada->the_post();
	        	get_template_part('templates/hemeroteca/latest', 'widget');
			endwhile;
		endif;
	}
		
	// Widget Backend 
	public function form( $instance ) {
		
	}
	
}