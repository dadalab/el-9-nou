<?php 

// Creating the widget 
class nou_acudits extends WP_Widget {

    function __construct() {
        parent::__construct('nou_acudits', 'Acudits a la portada', array( 'description' => 'Widget per mostrar acudits a la portada' ) );
    }

    public function widget( $args, $instance ) {
        if (nou_home()) : 
            $portada = new WP_Query(
                array(
                    'post_type' => 'acudits',
                    'posts_per_page' => 1,
                )
            ); ?>
            <div class="acudit"><?php
                while ($portada->have_posts()) : $portada->the_post();
                    get_template_part('templates/acudits/latest', 'widget');
                endwhile; ?>                        
            </div><?php
        endif;
    }
        
    // Widget Backend 
    public function form( $instance ) {

    }
    
}