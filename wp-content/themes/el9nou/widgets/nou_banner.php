<?php 

// Creating the widget 
class nou_banner extends WP_Widget {

	function __construct() {
		parent::__construct('nou_banner', 'Bànner general', array( 'description' => 'Widget per mostrar bànners generals' ) );
	}

	public function widget( $args, $instance ) {
		if ($instance['titol'] == 'valles' && edicio_actual() != 'valles-oriental') return;
		if ($instance['titol'] == 'osona' && edicio_actual() != 'osona-ripolles') return ?>
		<figure class="banner"><?php
			echo str_replace('{{ edicio_actual }}', edicio_actual(), $instance['codi']); 
			
			?>
		</figure><?php
	}
		
	// Widget Backend 
	public function form( $instance ) {
		$titol = isset( $instance[ 'titol' ] ) ? $instance[ 'titol' ] : 'Nom del bànner';
		$codi = isset( $instance[ 'codi' ] ) ? $instance[ 'codi' ] : ''; ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'titol' ); ?>">Títol</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'titol' ); ?>" name="<?php echo $this->get_field_name( 'titol' ); ?>" type="text" value="<?php echo esc_attr( $titol ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'codi' ); ?>">Codi</label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'codi' ); ?>" name="<?php echo $this->get_field_name( 'codi' ); ?>"><?php echo esc_attr( $codi ); ?></textarea>
		</p>
		<?php
	}
	
} // Class wpb_widget ends here