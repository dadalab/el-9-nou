<?php

// Creating the widget
class nou_blogosfera extends WP_Widget {

    function __construct() {
        parent::__construct('nou_blogosfera', 'Bànner Blogosfera', array( 'description' => 'Widget de promoció de la Blogosfera' ) );
    }

    public function widget( $args, $instance ) {?>
        <div id="sidebarblogosfera">
	        <sidebar-blogosfera></sidebar-blogosfera>
        </div><?php
    }

    public function form( $instance ) { }

}