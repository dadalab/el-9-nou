<?php 

// Creating the widget 
class nou_mes_vistes extends WP_Widget {

	function __construct() {
		parent::__construct('nou_mes_vistes', 'Llistat de més vistes', array( 'description' => 'Widget per mostrar les notícies més vistes' ) );
	}

	public function widget( $args, $instance ) {
		include( get_stylesheet_directory() . '/templates/sidebar/most-read.php' );
	}
		
	// Widget Backend 
	public function form( $instance ) {
		/**if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p><?php **/ 
	}
	
} // Class wpb_widget ends here