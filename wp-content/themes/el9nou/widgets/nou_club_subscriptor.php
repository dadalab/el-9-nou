<?php 

// Creating the widget 
class nou_club_subscriptor extends WP_Widget {

	function __construct() {
		parent::__construct('nou_club_subscriptor', 'Bànner Club del Subscriptor', array( 'description' => '' ) );
	}

	public function widget( $args, $instance ) { ?>
		<figure class="banner">
           <a class="club" href="<?php enllac_club() ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-club.jpg">
                <figcaption style="bottom: 20px;">Ofertes per a subscriptors</figcaption>
             </a>
        </figure>
        <?php
	}
		
	// Widget Backend 
	public function form( $instance ) {
		/**if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p><?php **/ 
	}
	
} // Class wpb_widget ends here