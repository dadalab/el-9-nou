<?php 

// Creating the widget 
class nou_xarxes_socials extends WP_Widget {

	function __construct() {
		parent::__construct('nou_xarxes_socials', 'Xarxes socials', array( 'description' => 'Mostra els feeds de les xarxes socials' ) );
	}

	public function widget( $args, $instance ) {
		if (nou_home()) : 
			include( get_stylesheet_directory() . '/templates/sidebar/xarxes-socials.php' );
		endif;
	}
		
	public function form( $instance ) {}
	
}