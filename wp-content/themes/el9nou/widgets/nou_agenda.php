<?php

// Creating the widget
class nou_agenda extends WP_Widget {

    function __construct() {
        parent::__construct('nou_agenda', 'Bànner dinàmic agenda', array( 'description' => 'Widget de promoció dels esdeveniments de l\'agenda' ) );
    }

    public function widget( $args, $instance ) { ?>
    	<div id="sidebaragenda">
        <sidebar-agenda inline-template edicio="<?php echo (edicio_actual() == 'valles-oriental') ? 'valles' : 'osona' ?>">
            <div class="most-viewed-news hidden-xs hidden-sm" style="margin-bottom: 0;border-bottom: 0">
                <a href="http://agenda.el9nou.cat"><h1><img src="http://agenda.el9nou.cat/imgs/logo.png"></h1></a>
                <div id="carousel-agenda" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item agenda" v-bind:class="{ 'active': ! count}" v-for="(count, item) in content">
                            <a href="{{ item.link }}" target="_blank">
                                <div class="carousel-caption">
                                    <h2>{{ item.nom_event }}</h2>
                                    <h4>{{ item.lloc }} - {{ item.municipi }}</h4>
                                    <span class="data">{{ new Date(item.data_inici).toLocaleDateString() }}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-agenda" role="button" data-slide="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <a class="right carousel-control" href="#carousel-agenda" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <a href="http://agenda.el9nou.cat" class="veure-tota-hemeroteca">Anar a l'agenda</a>
        </sidebar-agenda>
    	</div>
    	
    	<?php
    }

public function form( $instance ) { }

}