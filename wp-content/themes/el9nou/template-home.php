<?php
/*
 *	Template name: Home
 */
get_header(); ?>

<main id="main" role="main" class="main home">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                <?php the_content() ?>
            </div>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>