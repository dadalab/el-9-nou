<div class="col-md-3">
    <aside class="hidden-print">

        <div class="most-viewed-news hidden-xs hidden-sm">
            <h1>Les notícies més vistes</h1>
            <div class="news-items"><?php 
                $popular = wpp_get_mostpopular('post_type="post"&range="weekly"&limit=30');
                $popular = explode(',', $popular);
                $popularposts = new WP_Query(array('post__in' => $popular, 'posts_per_page' => 10, 'ignore_sticky_posts'   => 1, 'tax_query' => edicio_query_per_llistats()));
                if ( $popularposts->have_posts() ) :
                    while ($popularposts->have_posts()) : $popularposts->the_post(); ?>
                        <a href="<?php the_permalink() ?>">
                            <div class="news">
                                <div class="icon">
                                    <i class="icon9icons-<?php echo post_category_for_icon() ?>"></i>
                                </div>
                                <h2><?php the_title() ?></h2>
                            </div>
                        </a><?php
                    endwhile;
                endif; ?>               
            </div>
            <div class="gradient"></div>
        </div>

        <figure class="banner">
           <a class="club" href="<?php enllac_club() ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-club.jpg">   
                <figcaption style="bottom: 20px;">Ofertes per a subscriptors</figcaption>               
             </a>
        </figure>
    
        <?php // 9TV  ?>
        <figure class="banner">
            <a class="9tv" href="<?php url_site('el9tv') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-tv.jpg">  
                <figcaption><a href="<?php url_site('el9tv') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9tv') ?>/a-la-carta/">A LA CARTA</a></figcaption>
            </a>
        </figure>

        <?php // 9FM  ?>
        <figure class="banner">
            <a class="9tv" href="<?php url_site('el9fm') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-fm.jpg">  
                <figcaption class="fm"><a href="<?php url_site('el9fm') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9fm') ?>programacio/">PROGRAMACIÓ</a></figcaption>
            </a>
        </figure>
        
        <?php ads_sidebar_home() ?>

        <?php 
            ////////////////////
            // si és la HOME
            if ( 1 || nou_home() ) :

                // PORTADA PAPER
                hemeroteca_get_last_portada(); ?>
                <div class="anunci"><?php 
                if (edicio_actual() == 'valles-oriental') : ?>
                    <!-- /119069366/lateral-1-valles-oriental -->
                    <div id='div-gpt-ad-1479195683101-1' style='height:250px; width:300px;'>
                        <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479195683101-1'); });
                        </script>
                    </div>
                    <?php /**<figure class="banner">
                        <a href="https://www.estabanell.cat/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/banner-estabanell.gif"></a>
                    </figure> **/
                else : ?>
                    <!-- /119069366/lateral-1-osona-ripolles -->
                    <div id='div-gpt-ad-1479195683101-0' style='height:250px; width:300px;'>
                        <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479195683101-0'); });
                        </script>
                    </div>  
               <?php endif ?>
                </div>
                
               <?php // TWITTER + FACEBOOK  ?>
               <?php include('templates/socials/caixes-socials.php'); ?>

        <?php endif;

        // DIGUES LA TEVA  ?>
        <?php p_periodisme_ciutada_button() ?>    

        <?php
            // SI ÉS LA HOME
            if ( nou_home() ) {  

                // ANUNCI 2 EL VALLÈS
                /*
                if (edicio_actual() == 'valles-oriental') : ?>

                    <figure class="banner">
                        <a href="http://www.jda.es/es/2016/10/05/lempresa-davant-duna-inspeccio-tributaria/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/JDA-octubre16.gif"></a>
                    </figure>
               <?php endif 
               */ ?>

                <?php 
                // ANUNCI 2 ANUNCI DE TRÀNSIT  - Només dij, dv //
                if (date('N') > 3 && date('N') < 6)
                    include('ads/robapagines2.php');
            } 
        ?>

        <?php // 9MAGAZIN  ?>
        <figure class="banner">
            <a class="9magazin" href="<?php url_site('9magazin') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-magazin.jpg">  
                <figcaption class="magazin">SUPLEMENT</figcaption>
            </a>
        </figure>

        <?php // AGENDA  ?>
        <figure class="banner">
            <a class="agenda" href="<?php url_site('agenda') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/agenda-9nou.jpg">  
                <figcaption>CONCERTS, TEATRE...</figcaption>
            </a>
        </figure>
        
         <?php
        // SI ÉS LA HOME
        if ( nou_home() ) {   
                // ANUNCI 3
                include('ads/robapagines3.php');
        } ?>
        
        <?php // BLOGOSFERA  ?>
        <figure class="banner">
            <a class="blogosfera" href="<?php url_site('blogosfera') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/blogosfera.jpg">                  
            </a>
        </figure>

        <?php // fotogaleries  ?>
        <figure class="banner">
            <a class="fotogaleria" href="<?php url_site('fotogaleries') ?>/<?php echo edicio_actual() ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/fotogaleria.jpg">  
            </a>
        </figure>
        
        <?php
        // SI ÉS LA HOME
        if ( 1 || nou_home() ) : ?>
            <!--<script src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=19579756&PluID=0&w=300&h=600&ord=[timestamp]&ifl=$$/banners/eyeblaster/addineyeV2.html$$&ncu=$$#redir_url2#%2b#remote_addr#%2b$$&z=999999"></script>
            <noscript>
                <a href="#redir_url2#%2b#remote_addr#%2bhttp%3A//bs.serving-sys.com/BurstingPipe/adServer.bs%3Fcn%3Dbrd%26FlightID%3D19579756%26Page%3D%26PluID%3D0%26Pos%3D696356372" target="_blank"><img src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=19579756&Page=&PluID=0&Pos=696356372" border=0 width=300 height=600></a>
            </noscript>-->
            <div class="anunci">
            <?php if (edicio_actual() == 'osona-ripolles') : ?>
                <!-- /119069366/lateral-3-osona-ripolles -->
                <div id='div-gpt-ad-1479197835853-0'>
                    <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479197835853-0'); });</script>
                </div>
            <?php else : ?>
                <!-- /119069366/lateral-3-valles-oriental -->
                <div id='div-gpt-ad-1479197835853-1'>
                    <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479197835853-1'); });</script>
                </div>
            <?php endif ?>
            </div>

            <figure class="banner" style="margin-top: 20px;"><a href="http://www.badabum.cat/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/badabum.png"></a></figure><?php
        endif;
        
        if ( nou_home() ) :
            include('ads/robapagines4.php');
        endif; ?>

        <figure class="banner">
            <a class="club" href="http://www.9clics.cat/" target="_blank">
                <img src="<?php echo get_template_directory_uri() ?>/img/9-clics.jpg">                             
            </a>
        </figure><?php
        
        if ( nou_home() && time() < strtotime('2016/11/03') ) : ?>
            <figure class="banner">
                <a class="club" href="http://www.fundaciocarulla.cat/ca/premis-baldiri-reixac/bases" target="_blank">
                    <img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2016/10/Banner300x250GIF.gif">                             
                </a>
            </figure><?php 
        endif ?>
    </aside>
    
    <!-- ONLY PHONE -->
    <div class="sidebar-mobile visible-xs hidden-print">
        <div class="row">

            <figure class="banner col-xs-6">
                <a class="9tv" href="<?php url_site('el9tv') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/9-tv.jpg">  
                    <figcaption class="hidden-xs"><a href="<?php url_site('el9tv') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9tv') ?>/a-la-carta/">A LA CARTA</a></figcaption>
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="9tv" href="<?php url_site('el9fm') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/9-fm.jpg">  
                    <figcaption class="fm hidden-xs"><a href="<?php url_site('el9tv') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9fm') ?>/programacio/">PROGRAMACIÓ</a></figcaption>
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="blogosfera" href="<?php url_site('blogosfera') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/blogosfera.jpg">                  
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="9magazin" href="<?php url_site('9magazin') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/9-magazin.jpg">  
                    <figcaption class="magazin hidden-xs">SUPLEMENT</figcaption>
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="agenda" href="<?php url_site('agenda') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/agenda.jpg">  
                    <figcaption class="hidden-xs">CONCERTS, TEATRE...</figcaption>
                </a>
            </figure>
            
            <figure class="banner col-xs-6">
                <a class="fotogaleries" href="<?php url_site('fotogaleries') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/fotogaleries.jpg">  
                    <figcaption class="hidden-xs">CONCERTS, TEATRE...</figcaption>
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="club" href="http://club.el9nou.cat/ofertes_club.php" target="_blank">
                    <img src="<?php echo get_template_directory_uri() ?>/img/club-2.jpg">   
                    <figcaption class="hidden-xs" style="bottom: 20px;">Ofertes per a subscriptors</figcaption>               
                </a>
            </figure>

            <figure class="banner col-xs-6">
                <a class="club" href="#" target="_blank">
                    <img src="<?php echo get_template_directory_uri() ?>/img/digues-la-teva.jpg">   
                </a>
            </figure>
            
            <div class="col-xs-12"><hr style="margin-top: 0px;"></div>
        </div>    
    </div>
</div>