<?php
/**
 *
 *	Template name: EL 9FM
 */

get_header('el9fm') ?>

<main id="main" role="main" class="main">
   <section class="audio-stream">
	   <div class="container">	              
	           <div class="col-md-8 col-sm-12 col-xs-12 fit-vertical">
		           	<div class="row">
		           		<figure>
		           			<figcaption>
			           			La ràdio<br>
								en directe<br>
								<span>92.8 FM</span>
							</figcaption>
		           		</figure>
						<audio controls="controls" autoplay>
						    <source src="http://el9nou.cat/9fm/source" type="audio/mp3" />
						    <source src="http://el9nou.cat/9fm/source" type="audio/ogg" />
						    <?php //canviar a l'.htaccess ?>
						    Your browser does not support the audio tag.
						</audio>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12 fit-vertical">
					<div class="row">
						<div class="clearfix"></div>
						<div class="info-audio">
							<iframe width="100%" height="305" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/264555053&amp;color=c4003e&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
							<!--<div class="programa">
								<span class="data">09-06-2016</span>
								<h2><a href="#">L’hora de la veritat</a></h2>
								<p>Música, Art, Cinema, Oci nocturn, Cinema, Oci nocturn</p>
								<div class="sharer">
									<a href="#"><i class="icon9fm-download"></i></a>
									<a href="#"><i class="icon9fm-rss"></i></a>
								</div>
							</div>
							<div class="programa">
								<span class="data">09-06-2016</span>
								<h2><a href="#">L’hora de la veritat</a></h2>
								<p>Música, Art, Cinema, Oci nocturn, Cinema, Oci nocturn</p>
								<div class="sharer">
									<a href="#"><i class="icon9fm-download"></i></a>
									<a href="#"><i class="icon9fm-rss"></i></a>
								</div>						
							</div>
							<div class="programa">
								<span class="data">09-06-2016</span>
								<h2><a href="#">L’hora de la veritat</a></h2>
								<p>Música, Art, Cinema, Oci nocturn, Cinema, Oci nocturn</p>
								<div class="sharer">
									<a href="#"><i class="icon9fm-download"></i></a>
									<a href="#"><i class="icon9fm-rss"></i></a>
								</div>
							</div>							-->
						</div>
					</div>
				</div>
		</div>
	</section>
	<div class="container">
		<div class="row">

			<section class="audio-destacats">
				<div class="col-md-8">
					<h2>Seccions</h2>
					<div class="clear"></div><?php 
					$seccions = new WP_Query([
						'post_type' => 'seccio_radio',
						'posts_per_page' => 9
					]); ?>

					<section class="programacio audio-destacats">
							<div class="row"><?php
								while ($seccions->have_posts()) : $seccions->the_post(); ?>
									<div class="col-md-4 col-sm-4 programa"> 
										<a href="<?php the_permalink() ?>">
											<?php the_post_thumbnail() ?>
											<h1><?php the_title() ?></h1>
										</a>
									</div><?php 
								endwhile; ?>
							</div>
					</section>	
				</div>
			</section>

			<section class="programacio"><?php
				$dies = array('diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte');
				$avui = new WP_Query( array(
					'post_type' => 'programacio_radio',
					'name' => $dies[date('w')]
				)); ?>
				<div class="col-md-4">
					<h2>Programació <span><a class="all" href="<?php echo site_url() ?>/9fm/programacio">VEURE-LA TOTA</a></span></h2>
					
					<div class="border">
						<div class="col-md-12">
							<div class="row">
								<h1>AVUI <?php echo date('d-m-Y') ?> <span class="icon9fm-waves"></span> </h1>		
								<h3>
									<span>Cada hora a partir 7.00 h. </span>
									Butlletí informatiu + Selecció musical
								</h3>
								<?php while ( $avui->have_posts() ) : $avui->the_post(); ?>
									<?php the_content() ?>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
   </div>
</main>
<?php get_footer(); ?>