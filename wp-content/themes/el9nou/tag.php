<?php get_header() ?>
<main id="main" role="main" class="main categoria">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
            
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">Etiqueta: <?php single_tag_title(); ?> </div>
                    <div class="share"><?php the_news_sharing() ?></div>
                </div> 
            
                <section id="noticies-arxiu">
                    <div class="row">
                        <div class="bloc"><?php
                            if (have_posts()) :
                                while (have_posts()) : the_post();
                                    get_template_part('templates/noticies/categoria', 'arxiu');
                                endwhile;
                            endif; ?>
                        </div><!-- / bloc -->
                    </div><!-- / row -->
                </section><!-- / noticies-arxiu -->
            </div><!-- / col-md-9 -->
            <?php get_sidebar() ?>
        </div><!-- / row -->
    </div>
</main><!-- / section -->
<?php get_footer() ?>