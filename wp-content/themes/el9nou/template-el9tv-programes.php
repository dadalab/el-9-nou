<?php
/**
 *
 *	Template name: ELS PROGRAMES EL 9TV
 */

get_header('el9tv') ?>

<main id="main" role="main" class="main">
	<section class="video-stream">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1>Els programes</h1>
				</div>
			</div>				
		</div>
	</section>

	<section class="programacio">		
		<div class="container" style="position: relative;">			
			<div class="row">													
				<div class="col-md-12">
					<div class="titol"><h2>Els programes d'EL 9 TV</h2></div>
				</div><?php
				$avui = new WP_Query( array(
					'post_type' => 'programa',
					'posts_per_page' => -1
				)); 
				while ( $avui->have_posts() ) : $avui->the_post(); ?>
					<div class="col-md-3 col-sm-4 programa">
						<a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail() ?>
							<h1><?php the_title() ?></h1>
						</a>
					</div><?php
				endwhile; ?>
			</div>						
		</div>
	</section>				
</main>

<?php get_footer(); ?>