<?php get_header(); ?>

<main id="main" role="main" class="main categoria">
    <div class="container">
        <div class="row">               
            <div class="col-md-12 page-404">
            		<span>!</span><br>
                	<h2>La pàgina que busques no existeix.</h2>
                	<a href="<?php echo site_url() ?>">Tornar a la portada</a>
			</div>
            <?php // get_sidebar() ?>
        </div><!-- / row -->
    </div>
</main>
<!-- / section -->
<?php get_footer()  ?>