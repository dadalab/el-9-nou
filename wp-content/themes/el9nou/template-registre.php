<?php
/**
 *
 *	Template name: Registre usuaris
 */
global $ads;
$ads = false;

get_header(); ?>

<main id="main" role="main" class="main registre">
    <div class="container">
        <div class="row">
        	<div class="col-md-6 col-md-offset-3" id="registre">
				<h2>Registra't al 9Nou</h2>
            	<p>
					Registreu-vos per poder comentar les notícies i treure el màxim rendiment d’aquesta web.
            	</p>
        		<?php echo do_shortcode('[ru_custom_form]') ?>
        	</div>
        </div>
	</div>
</main>

<?php get_footer(); ?>