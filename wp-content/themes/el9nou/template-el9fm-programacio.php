<?php
/**
 *
 *	Template name: PROGRAMACIÓ EL 9FM
 */

get_header('el9fm') ?>

<main id="main" role="main" class="main">
	<section class="audio-stream">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1>La programació</h1>
				</div>
			</div>				
		</div>
	</section>

	<section class="programacio">
		<div class="container">
			<div class="row"><?php
				$dies = array('diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte');
				$diesOrdenats = array();
				$dia = date('w');
				for ($i = 0; $i < 7; $i++) :
					$diesOrdenats[] = $dies[$dia];
					$dia++;
					if ($dia > 6) $dia = 0;
				endfor; ?>	
				<div class="col-md-12">
					<div class="titol">
						<h2>Programació</h2>												
						<div class="navigation-owl"> 
								<a href="#" class="left-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
								<a href="#" class="right-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>

					<div id="owl-demo"><?php
						$count = 0;
						foreach($diesOrdenats as $dia) :
							$avui = new WP_Query( array(
								'post_type' => 'programacio_radio',
								'name' => $dia
							)); ?>
					  		<div class="item">
								<div class="border col-md-12">
									<div class="row">
										<h1><?php echo $dia . ' ' . date('d-m-Y', strtotime("+" . $count . " days")) ?></h1>		
										<h3><span>Cada hora a partir 7.00 h. </span>Butlletí informatiu + Selecció musical</h3><?php 
										while ( $avui->have_posts() ) : $avui->the_post();
											the_content();
										endwhile; ?>
									</div>
								</div>
						  	</div><?php 
					  		$count++;
						endforeach ?>
					</div>
					<div class="titol">
						<hr>
						<h3>Desplaçar-se cap a les bandes per a veure la resta de la programació</h3>
						<div class="navigation-owl bottom"> 
							<a href="#" class="left-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
							<a href="#" class="right-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>					
				</div><!-- col-md-4 -->
			</div>
		</div>
	</section>				
</main>
<?php get_footer(); ?>