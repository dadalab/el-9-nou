<div class="row formulari">
   <div class="col-md-4 col-sm-4">
       <header>
            <h1>Comentaris</h1>   
       </header>     
   </div>
   <div class="col-md-8 col-sm-8"><?php 
   		comment_form( array(
   			'title_reply' => '',
   			'class_submit'=> 'btn send',
   			'label_submit' => 'Enviar',
   			'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ),  
   				site_url('/entra?r=')  . get_permalink() ) . '</p>'
   		)); ?> 
   </div>
</div><?php 

if ( have_comments() ) : ?>
	<h3 id="comments"><?php
		if ( 1 == get_comments_number() ) {
			printf( __( 'One response to %s' ),  '&#8220;' . get_the_title() . '&#8221;' );
		} else {
			printf( _n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number() ),
					number_format_i18n( get_comments_number() ),  '&#8220;' . get_the_title() . '&#8221;' );
		} ?>
	</h3>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

	<ol class="commentlist"><?php wp_list_comments('callback=custom_comment_template'); ?></ol>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
 <?php else :
	if ( comments_open() ) :
		// Comments;
	 else : ?>
		<p class="nocomments"><?php _e('Comments are closed.'); ?></p><?php
	endif;
endif; ?>