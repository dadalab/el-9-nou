
<?php get_header();
$category = get_category(get_query_var('cat')); ?>
<main id="main" role="main" class="main categoria">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria"><?php echo $category->name ?></div>
                    <div class="share">Compartir <i class="icon-share"></i></div>
                </div> 

                <!-- noticies actuals -->
                <section id="noticies-recents">     
                    <div class="row"><?php 
                        $noticia = new WP_Query(array(
                            'post__in' => get_option('sticky_posts'),
                            'cat' => $category->term_id,
                            'posts_per_page' => 1,
                            'ignore_sticky_posts' => 1
                        ));
                        if ($noticia->have_posts()) :
                            while ($noticia->have_posts()) : $noticia->the_post();
                                if (is_sticky()) {
                                    get_template_part('templates/noticies/categoria', 'destacada');
                                }
                            endwhile;
                        endif; ?>
                            
                        <!-- bloc -->
                        <div class="bloc"><?php
                            $noticies = new WP_Query(array('post__not_in' => get_option('sticky_posts'), 'cat' => $category->term_id, 'posts_per_page' => 3 ));
                            if ($noticies->have_posts()) :
                                while ($noticies->have_posts()) : $noticies->the_post();
                                    get_template_part('templates/noticies/categoria', 'recent');
                                endwhile;
                            endif; ?>
                        </div><!-- bloc -->
                    </div>
                </section><!-- / noticies recents -->
                
                <section id="noticies-arxiu">
                    <div class="row">
                        <div class="bloc"><?php
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            var_dump(get_query_var('paged'));
                            $noticies = new WP_Query(array('post__not_in' => get_option('sticky_posts'), 'cat' => $category->term_id, 'posts_per_page' => 2, 'paged' => $paged ));
                            if ($noticies->have_posts()) :
                                while ($noticies->have_posts()) : $noticies->the_post();
                                    get_template_part('templates/noticies/categoria', 'arxiu');
                                endwhile;
                            endif; ?>

                            <?php if ($noticies->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
  <nav class="prev-next-posts">
    <div class="prev-posts-link">
      <?php echo get_next_posts_link('Older Entries', $noticies->max_num_pages); // display older posts link ?>
    </div>
    <div class="next-posts-link">
      <?php echo get_previous_posts_link('Newer Entries'); // display newer posts link ?>
    </div>
  </nav>
<?php 
} ?>
                        </div>
                    </div>
                </section>
            </div>

            <?php get_sidebar() ?>
        </div><!-- / row -->
    </div>
</main>
<!-- / section -->
<?php get_footer()  ?>