<?php get_header() ?>
<main id="main" role="main" class="main fitxa">
   <div class="container">
        <div class="row">
           <div class="col-md-9"><?php
                while (have_posts()) : the_post(); ?>
                <article>
                    <header>
                        <div class="barra-superior clearfix hidden-print">
                            <div class="breadcrumb"><?php the_post_category_breadcrumb() ?></div>
                            <ul class="compartir hidden-xs">
                                 <li>Compartir</li>
                                 <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                 <li><a href="https://twitter.com/intent/tweet?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                 <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                 <?php if (wp_is_mobile()) : ?>
                                 <li><a href="whatsapp://send?text=<?php the_permalink() ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                <?php endif ?>
                                 <li><a href="#" class="single-share-url" data-url="<?php the_permalink() ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                 <li><a href="#" onclick="printFunction()"><i class="fa fa-print" aria-hidden="true"></i></a></li>
                             </ul>
                        </div>
                        <h1 itemprop="headline"><?php the_title() ?></h1>
                        <p><?php the_excerpt() ?></p>
                        
						
	                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<!-- DespresTitolMobil -->
							<ins id="adddespresmobil" class="adsbygoogle"
							     style="display:none"
							     data-ad-client="ca-pub-9944811849436984"
							     data-ad-slot="2836135615"
							     data-ad-format="auto"></ins>
							<script>
								if (document.referrer.includes('facebook') || document.referrer.includes('twitter')) {
									console.log('show ad');
									document.getElementById('adddespresmobil').style.display = 'block';
									(adsbygoogle = window.adsbygoogle || []).push({});
								}
								else {
									console.log('donot show');
								}
							</script>
                        
                    </header>

                    <div class="content fitxa-content" itemscope>
                        <ul class="information-article">
                            <li class="author"><i class="icon-user hidden-print"></i> <?php get_the_authors() ?></li>
                            <li class="date"><i class="icon-calendar hidden-print"></i> <span><?php the_date() ?></span> | <?php the_time() ?></li>
                            <?php if (get_post_city()) : ?>
                                <li class="location hidden-print"><i class="icon-location"></i> <?php echo get_post_city() ?></li>
                            <?php endif ?>
                        </ul>

                        <?php include('templates/noticies/single-featured-images.php') ?>

                        <ul class="compartir visible-xs hidden-print">
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="whatsapp://send?text=<<?php the_permalink() ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="single-share-url" data-url="<?php the_permalink() ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                        </ul>


                        <section class="article-body" itemprop="articleBody">
                            <div class="row"><?php
                                if (! get_field('amplada_sencera') && ! get_field('es_premium')) : ?>
                                    <div class="col-md-5  aside-article-body"><?php
                                        if (get_field('es_patrocinada')) : ?>
                                            <div class="noticia_patrocini" style="background: #f6f6f6;display: block;">
                                                <?php $patrocinador = get_field('es_patrocinada') ?>
                                                <a href="<?php the_field('url', $patrocinador->ID)?>">
                                                    <img src="<?php echo get_the_post_thumbnail_url($patrocinador->ID) ?>">
                                                </a>

                                                <p class="text-center">
                                                    <?php foreach(get_the_category() as $category) {
                                                        echo $category->name;
                                                        } ?>
                                                    </p>
                                            </div><?php
                                        else:
                                            echo get_template_part('templates/ads/single', 'aside');
                                        endif; ?>
                                    </div>


                                    <div class="col-md-7 marge"><?php
                                else : ?>
                                    <div class="col-md-12 marge"><?php
                                endif; ?>

                                <?php if (get_field('es_premium')) : ?>
                                    <div id="noticia-premium" style="margin-bottom: 30px;">
                                        <noticia-premium inline-template v-cloak id="<?php the_ID() ?>" >
                                            <div style="text-align: center" v-if="loading">
                                                <i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
                                            </div>
                                            <div id="content-premium-news" v-if="content">

                                            </div>
                                            <div v-if="! content && ! loading">
                                                <p class="lead">Aquest contingut és exclusiu per a subscriptors d'EL 9 NOU i socis d'el9club.</p>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <h4 class="premium">Registra't</h4>
                                                        <div class="premium-register">
                                                            <a href="http://9club.el9nou.cat/osona-ripolles/registre?subscriptor=1&redirect=<?php the_permalink() ?>" class="btn btn-primary btn-club">Ja sóc subscriptor</a>
                                                            <a href="http://9club.el9nou.cat/osona-ripolles/subscriute?2" class="btn btn-primary btn-club">Vull fer-me soci d'el9club</a>
                                                            <a href="/" class="premium-cancel" style="border: none; text-decoration: underline;">Cancel·la</a>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <h4 class="premium">Inicia sessió</h4>
                                                        <div class="premium-access">
                                                           <a href="http://9club.el9nou.cat/osona-ripolles/entra?redirect=<?php the_permalink() ?>" class="btn btn-primary btn-club btn-club-access">Entra</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </noticia-premium>
                                    </div>
                                <?php else : ?>
                                    <?php the_content() ?>

                                    <div class="tags hidden-print">
                                        <?php //ads_before_tags() ?>
                                        <?php echo get_the_tag_list(); ?>
                                    </div>
                                <?php endif ?>

                                </div>

                        </section>

                        <section class="related-articles hidden-print" style="padding: 0">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_related_posts() ?>
                                </div>
                            </div>
                        </section>

                        <?php get_template_part('templates/comments/single', 'noticia') ?>
                        <?php /**<section class="comments hidden-print" id="comments">
                            <?php comments_template(); ?>
                        </section> **/ ?>

                    </div>

                </article><?php
                endwhile ?>
           </div>

           <?php get_sidebar() ?>
        </div>
   </div>
</main>

<?php get_footer(); ?>