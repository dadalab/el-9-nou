<?php
/**
 *
 *	Template name: A LA CARTA 9TV
 */

get_header('el9tv') ?>

<main id="main" role="main" class="main">
	<section class="video-stream margin-no">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1>EL 9 TV a la carta</h1>
				</div>
			</div>				
		</div>
	</section>

	<section class="a-la-carta videos-destacats">		
		<div class="container" style="position: relative;">			
			<div class="row">		
				
			</div>
		</div>
	</section>			


	<section class="a-la-carta llistat-videos">
		<div class="container">			
			<div class="row">		
				<div class="col-md-9">
					<div class="titol"><h2>Programes a la carta</h2></div>
					<div class="row"><?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$alacarta = new WP_Query( array( 
							'post_type' => 'video',
							'paged' => $paged,
							'posts_per_page' => 9
							)
						);
						while ($alacarta->have_posts()) : $alacarta->the_post(); ?>
							<div class="col-md-4 col-sm-4 programa video-element">
								<a href="<?php the_permalink() ?>">
									<figure>
										<div class="play-small"><i class="icon-play"></i></div>
										<div class="bg-capt"></div>
										<?php the_post_thumbnail() ?>
										<!--<div class="durada">01m. 16s</div>-->
									</figure>								
									<header>
										<span class="data"><?php echo get_the_date() ?></span>	
										<h1 class="titol"><?php echo limit_characters(get_the_title(), 75) ?></h1>
										<a href="<?php echo get_permalink(get_field('programa')->ID) ?>">
											<span class="programa"><?php echo get_field('programa')->post_title ?></span>
										</a>		
										<div class="autor"><?php // get_the_authors() ?></div>						
									</header>
								</a>
							</div><?php 
						endwhile; ?>
						
						<?php if ($alacarta->max_num_pages > 1) {  ?>
  							<nav class="prev-next-posts">
    							<div class="prev-posts-link">
      								<?php echo get_next_posts_link('Més antics', $alacarta->max_num_pages); ?>
    							</div>
    							<div class="next-posts-link">
      								<?php echo get_previous_posts_link('Més recents'); ?>
    							</div>
  							</nav>
						<?php } ?>
					
					</div>
				</div><!-- / row -->
						
				<!-- LLISTAT DE PROGRAMES  -->
				<div class="col-md-3 llistat-programes">
					<div class="titol"><h2>Llistat de programes</h2></div>

					<ul class="program-list"><?php
						$programes = new WP_Query( array( 
								'post_type' => 'programa',
								'orderby' => 'title',
								'order' => 'ASC',
								'posts_per_page' => -1
							)
						);

						while ($programes->have_posts()) : $programes->the_post(); ?>
							<li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li><?php
						endwhile; ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>