<!DOCTYPE html>
<html lang="ca">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Osona Ripollès | El 9 Nou</title>
    <link rel="shortcut icon" href="http://el9nou.cat/favicon-el9nou-osona-ripolles.cat.ico">
  <style>
  button {
    margin: auto;
    display: block;
    border:  1px solid #333;
    background: transparent;
    color: #333;
    padding: 15px;
    border-radius: 54px;
    font-size: 13px;
    text-transform: uppercase;
    margin-bottom: 20px;
    position: absolute;
    top: 40px;
    right: 0;
    cursor: pointer;
}

@media screen and (max-width: 769px) {
    body {
        max-width: 600px !important;
    }
}

@media screen and (max-width: 639px) {
    button {
        position: relative;
        top: 0;
        margin-bottom: 30px;
    }
}
  </style>
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>
    <script>
    googletag.cmd.push(function() {
        var mapping = googletag.sizeMapping().
            addSize([900, 690], [800, 600]).
            addSize([640, 480], [600, 450]).
            addSize([0, 0], [320, 240]).
            build();

        googletag.defineSlot('/119069366/interestitial-osona', [[800, 600], [600, 450], [320, 240]], 'div-gpt-ad-1495022258756-0').defineSizeMapping(mapping).addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
  </script>
</head>

<body style="text-align: center;position: relative;max-width: 800px;margin: auto">

    <a href="/"><img src="http://el9nou.cat/wp-content/themes/el9nou/img/el9nou.cat.svg" style="width: 200px;margin: 50px; margin-bottom: 40px;"></a>

    <button id="accedir" onclick="self.location.reload()">Accedir al diari</button>

    <!-- /119069366/interestitial-osona -->
    <div id='div-gpt-ad-1495022258756-0' style="">
        <script>
          googletag.cmd.push(function() { googletag.display('div-gpt-ad-1495022258756-0'); });
      </script>
  </div>


</body>
</html>