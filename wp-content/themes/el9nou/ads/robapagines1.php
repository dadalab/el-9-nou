<style>
    .anunci {
        margin: 20px 0;
    }
</style>
<div class="anunci">
    <div id="video-player-div" style="height:250px;width:300px;vertical-align: middle;position:relative;">
    <img src="http://www.acpc.cat/capsules012/capcalera012.png">
    <div style="background-color: tomato; margin-top: -10px; padding-bottom: 1px; padding-top: 1px;">
    <p style="font:11px 'Helvetica Neue',Helvetica,sans-serif;text-align:center;font-weight:bold;margin-top: 13px;"><a href="#" target="_blank" id="enllac" style="color: #FFF;">Informació d’interès al ciutadà: <span id="titol">#</span></a></p>
    </div>
    <img id="captura" src="#" style="position: absolute;z-index: 1;left: 0;top:58px;cursor:pointer;">
    <video width="300" controls muted id="video" style="position: absolute;left:0;top:58px;">
      <source src="#" type="video/mp4">
      Your browser does not support HTML5 video.
    </video>
    </div>

    <script>
    var vid = document.getElementById("video");
    var img = document.getElementById("captura");
    var titol = document.getElementById("titol");
    var enllac = document.getElementById("enllac");
    var dir = "http://www.acpc.cat/capsules012/videos/";

    vid.onplay = function() {
        img.style.visibility='hidden';
    };

    vid.onended = function()
    {
        img.style.visibility='visible';
    };

    img.onclick = function () {    
        vid.muted = false;
        vid.play();
    }

    var n = Math.floor((Math.random() * 5) + 1);

    switch (n) {
      case 1:
        titol.innerHTML = "Salut";
        enllac.setAttribute("href", "http://canalsalut.gencat.cat/ca/salut-a-z/g/grip/grip/");
        img.src = dir + "061 VACUNACIO.png";
        vid.src = dir + "061 VACUNACIO.mp4";    
        break;
      case 2:
        titol.innerHTML = "Drets";
        enllac.setAttribute("href", "http://lgbti.gencat.cat/");
        img.src = dir + "012 HOMOFOBIA2.png";
        vid.src = dir + "012 HOMOFOBIA2.mp4";
        break;
      case 3:
        titol.innerHTML = "Habitatge";
        enllac.setAttribute("href", "http://agenciahabitatge.cat/");
        img.src = dir + "012 HABITATGE.png";
        vid.src = dir + "012 HABITATGE.mp4";
        break;
      case 4:
        titol.innerHTML = "Alimentació";
        enllac.setAttribute("href", "http://agricultura.gencat.cat/");
        img.src = dir + "012 LLET.png";
        vid.src = dir + "012 LLET.mp4";
        break;
     case 5:
        titol.innerHTML = "Acollida infants";
        enllac.setAttribute("href", "http://gencat.cat/acollimentfamiliar");
        img.src = dir + "012 ACOLLIDA.png";
        vid.src = dir + "012 ACOLLIDA.mp4";
        break;
    }
    </script>
</div>

