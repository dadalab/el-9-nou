<style>
    .anunci {
        margin: 20px 0;
    }
</style>

<div class="anunci" style="border: 1px solid #e2e2e2;">
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
	<script>
	  var googletag = googletag || {};
	  googletag.cmd = googletag.cmd || [];
	</script>
	 
	<script>
	  googletag.cmd.push(function() {
	    googletag.defineSlot('/30029749/el9nou.cat', [300, 250], 'div-gpt-ad-1478249329012-0').addService(googletag.pubads());
	    googletag.pubads().enableSingleRequest();
	    googletag.enableServices();
	  });
	</script>
	 
	<!-- /30029749/el9nou.cat -->
	<div id='div-gpt-ad-1478249329012-0'>
	<script>
	googletag.cmd.push(function() { googletag.display('div-gpt-ad-1478249329012-0'); });
	</script>
	</div>
	 
</div>
