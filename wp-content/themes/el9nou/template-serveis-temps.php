<?php
/**
 *
 *	Template name: Test Weather
 */
get_header() ?>


<main id="main" role="main" class="main">
	
	<div id="templateeltemps">
	<template-el-temps inline-template>
		<div class="container" id="eltemps">
			<div class="col-md-9">

   			<div class="barra-superior clearfix">
	            <div class="breadcrumb">El temps</div>
	        </div>

	        <div class="row" v-cloak>
	        	<div class="col-md-6">

				<!-- 1era fila temp -->
				<div class="temperatura-actual">
    				<div class="row">
        				<div class="col-md-7">
							<div class="name-city">{{ ciutat }}</div>
						</div>
						<div class="col-md-5 text-right">
							<div class="temp-max-min">
							<!-- <span style="font-family: 'Open Sans',sans-serif; font-weight: 400; font-size: 14px;">TEMP. ACTUAL</span> -->
							{{ parseFloat(previsio.currently.temperature).toFixed(1) }}º<br>
							</div>
						</div>
					</div>
				</div><!-- / temperatura-actual -->


    			<div class="temperatura-actual-2">
    				<div class="row">
        				<div class="col-md-7">
							<div class="informacio-temps">
								<p style="font-weight: 600;">{{ previsio.currently.summary }}</p>
								Humitat: {{ previsio.currently.humidity * 100 }}%<br>
								Velocitat Vent: {{ previsio.currently.windSpeed }} m/s<br>
								<i class="wi wi-horizon-alt"></i>Sortida sol: {{ showDate(previsio.daily.data[0].sunriseTime * 1000) }}<br>
								<i class="wi wi-horizon"></i>Posta sol: {{ showDate(previsio.daily.data[0].sunsetTime * 1000) }}<br>
							</div>
						</div>
						<div class="col-md-5 text-right">
							<div class="icona-temps">
								{{{ setWeatherIcon(previsio.daily.data[0].icon) }}}
							</div>
						</div>
					</div>
				</div><!-- / temperatura-actual-2 -->

	        	</div>

	        	<div class="col-md-6">
	        		<div class="cerca-ciutat">

						<h2>Vull saber el temps que fa a:</h2>

		        		<form v-on:submit.prevent="searchCity">
			           		<input type="text" placeholder="Població/Ciutat" v-model="ciutat" value="<?php echo (edicio_actual() == 'osona-ripolles') ? 'Vic' : 'Granollers'; ?>">
			           		<!--<input type="submit" value="Cercar">-->
			           		<button type="submit">Cercar <i v-if="loading" class="fa fa-spinner fa-spin fa-fw"></i></button>
						</form>
					</div>
	        	</div><!-- / col-md-6 -->

	           	<div class="col-md-12">

					<h3 class="titol-previsio">Previsió de la setmana</h3>
					<div class="row">
						<div class="col-md-6 col-sm-6 dia-previst-{{ $index }}" v-for="dia in previsio.daily.data">
							<div class="previsio">
								<div class="row">
									<div class="col-md-12 col-sm-12 icona-prev">
										<span class="icona">{{{ setWeatherIcon(dia.icon) }}}</span>
									</div>
									<div class="col-md-6 col-sm-6 dia-previsio">
										<span class="dia">{{ diesSetmana[showDay(dia.time * 1000)] }}</span>
										<small>{{ dia.summary }}</small>
									</div>
									<div class="col-md-5 col-sm-5 dades-previsio">
										<span class="temp bold">{{ parseFloat(dia.temperatureMin).toFixed(1) }}º / {{ parseFloat(dia.temperatureMax).toFixed(1) }}º</span>
										<span class="temp">Vent: {{ dia.windSpeed }} m/s</span>
										<span class="temp">Pressió: {{ dia.pressure }} hpa</span>
										<span class="temp">Prob. pluja: {{ parseInt(dia.precipProbability * 100) }} %</span>
									</div>
								</div>
							</div>
	           			</div>
           			</div>
	            </div><!-- / col-md-12 -->
	        </div><!-- / row -->

	</template-el-temps>
	</div>
    <?php get_sidebar() ?>
   </div>
</main>
<!-- / section -->

<script src="<?php echo get_template_directory_uri() ?>/js/vendor/superfish/superfish.js"></script>

<script>
// Obtenim nom de la ciutat en funció de les coordenades donades.
function locationSuccess(position) {
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;
	var geoAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat+ ',' + lon + '&key=AIzaSyDPYtRNSQhvIsoNGiQlAZ4cWz-PRKXEC2s&language=ca';

	$.get( geoAPI, function( data ) {
		var poblacioActual = data.results[1].formatted_address;
		temps.$set('ciutat', poblacioActual);
		temps.getWeatherForCity();
	});
}

function locationError(error){
	switch(error.code) {
		case error.TIMEOUT:
			showError("A timeout occured! Please try again!");
			break;
		case error.POSITION_UNAVAILABLE:
			showError('We can\'t detect your location. Sorry!');
			break;
		case error.PERMISSION_DENIED:
			showError('Please allow geolocation access for this to work.');
			break;
		case error.UNKNOWN_ERROR:
			showError('An unknown error occured!');
			break;
	}
}
</script>

<?php get_footer(); ?>