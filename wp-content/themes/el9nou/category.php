<?php get_header();
global $wp_query;
$category = get_category(get_query_var('cat')); 
?>
<main id="main" role="main" class="main categoria">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria"><?php echo $category->name ?></div>
                    <div class="share"><?php the_news_sharing() ?></div>
                </div><?php 
                if (! is_paged()) : ?>
                    <section id="noticies-recents">     
                        <div class="row"><?php 
                            $noticia = new WP_Query(array(
                                'post__in' => get_option('sticky_posts'),
                                'cat' => $category->term_id,
                                'posts_per_page' => 1,
                                'ignore_sticky_posts' => 1,
                                'tax_query' => edicio_query_per_llistats(),
                            ));
                            if ($noticia->have_posts()) :
                                while ($noticia->have_posts()) : $noticia->the_post();
                                    if (is_sticky()) {
                                        get_template_part('templates/noticies/categoria', 'destacada');
                                    }
                                endwhile;
                            endif; ?>
                                
                            <!-- bloc -->
                            <div class="bloc" <?php if ($category->term_id == 10989) : ?> style="display:block;"<?php endif ?>>
                                <div class="alsada">
                                    <?php
                                    $noticies = new WP_Query(
                                        array(
                                            'post__not_in' => get_option('sticky_posts'), 
                                            'cat' => $category->term_id,
                                            'posts_per_page' => 3,
                                            'tax_query' => edicio_query_per_llistats(),
                                        )
                                    );
                                    if ($noticies->have_posts()) :
                                        while ($noticies->have_posts()) : $noticies->the_post();
                                            get_template_part('templates/noticies/categoria', 'recent');
                                        endwhile;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </section><?php
                endif ?>

                <section id="noticies-arxiu">
                    <div class="bloc" <?php if ($category->term_id == 10989) : ?> style="display:block;"<?php endif ?>><?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $noticies = new WP_Query(
                            array(
                                'post__not_in' => get_option('sticky_posts'), 
                                'cat' => $category->term_id,
                                'posts_per_page' => 30,
                                'offset' => ($paged - 1) * 30 + 3,
                                'paged' => $paged,
                                'tax_query' => edicio_query_per_llistats(),
                            )
                        );
                        if ($noticies->have_posts()) :
                            $count = 0;
                            while ($noticies->have_posts()) : $noticies->the_post();
                                if ($count % 2 == 0) echo "<div class='row'>";
                                    get_template_part('templates/noticies/categoria', 'arxiu');
                                if ($count % 2 == 1) echo "</div>";
                                $count++;
                            endwhile;
                        endif; ?>

                        <?php if ($noticies->max_num_pages > 1) : ?>
                            <div class="row">
                                <nav class="prev-next-posts">
                                    <div class="prev-posts-link">
                                        <?php echo get_next_posts_link('Notícies més antigues', $noticies->max_num_pages); ?>
                                    </div>
                                    <div class="next-posts-link">
                                        <?php echo get_previous_posts_link('Notícies més recents'); ?>
                                    </div>
                                </nav>
                            </div>
                        <?php endif ?>
                    </div>
                </section>
            </div>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>

<?php get_footer() ?>