<?php
/**
 *
 *	Template name: EL 9TV
 */
get_header('el9tv'); ?>

<main id="main" role="main" class="main">
   <section class="video-stream">
	   <div class="container">
	   	
	        <div class="bloc">               
	           <div class="col-md-8 col-sm-12 col-xs-12 fit-vertical">
		           	<div class="row">
		           		<div class="content-video">
							<iframe class="video" src="/9tv/iframe" width="100%" height="475px" style="border:0"></iframe>
							<?php //canviar a .htaccess ?>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12 fit-vertical">
					<div class="row">
						<div class="info-video">
							<span class="hora">Ara mateix</span>
							<h1>EL 9 TV en directe</h1>							
							<p><?php el9tv_programa_actual() ?></p>
							<ul class="info-article">		
								<li class="compartir">Compartir</li>				           
					            <li class="sharer"><?php the_news_sharing() ?></li>
					        </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<section class="videos-destacats">
				<div class="col-md-8">
					<h2>Vídeos recents <span><a class="all" href="#">VEURE TOTS ELS VÍDEOS</a></span></h2>
					<div class="row"><?php
                        $videos = new WP_Query(array(
                            'post_type' => 'video',
                            'posts_per_page' => 12
                        ));
                        if ($videos->have_posts()) :
                            while ($videos->have_posts()) : $videos->the_post(); ?>
								<div class="col-md-6 col-sm-6">
									<div class="video mb">
										<article>
											<figure>
												<a href="<?php the_permalink() ?>">
													<div class="play"><i class="icon-play"></i></div>
                                                    <div class="bg-capt"></div>
													<?php the_post_thumbnail() ?>
												</a>
											</figure>
											<header>
												<span class="data"><?php echo get_the_date() ?></span>
												<a class="titol" href="<?php the_permalink() ?>"><?php the_title() ?></a>
												
												<a href="<?php echo get_permalink(get_post_meta(get_the_ID(), 'programa', true)) ?>">
													<span class="programa"><?php echo get_the_title(get_post_meta(get_the_ID(), 'programa', true)) ?></span></a>
												<div class="autor"><?php get_the_authors() ?></div>
											</header>
										</article>
									</div>
								</div><?php
                            endwhile;
                        endif; ?>
					</div>
				</div>
			</section>
			
			<section class="programacio">
				<div class="col-md-4">
					<h2>Programació <span><a class="all" href="<?php url_site('el9tv') ?>programacio">VEURE TOTA LA PROGRAMACIÓ</a></span></h2>
					<div class="border">
						<div class="col-md-12">
							<div class="row">
								<h1>AVUI <?php echo date('d-m-Y') ?></h1><?php
                                $dies = array('diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte');
                                $avui = new WP_Query(array(
                                    'post_type' => 'programacio_tv',
                                    'name' => $dies[date('w')]
                                ));
                                while ($avui->have_posts()) : $avui->the_post();
                                    the_content();
                                endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
   </div>
</main>
<?php get_footer(); ?>