<?php
/**
 *
 *	Template name: SECCIONS 9FM
 */

get_header('el9fm') ?>

<main id="main" role="main" class="main">
	<section class="audio-stream">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1>SECCIONS</h1>
				</div>
			</div>				
		</div>
	</section><?php 
		$seccions = new WP_Query([
			'post_type' => 'seccio_radio',
			'posts_per_page' => 9999
		]);
	?>
	<section class="programacio audio-destacats">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="titol"><h2>Les seccions de l'hora de la veritat</h2></div>
				</div><?php
				while ($seccions->have_posts()) : $seccions->the_post(); ?>
					<div class="col-md-3 col-xs-6 programa"> 
						<a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail() ?>
							<h1><?php the_title() ?></h1>
						</a>
					</div><?php 
				endwhile; ?>
			</div>
		</div>
	</section>				
</main>
<?php get_footer(); ?>