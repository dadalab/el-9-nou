<?php get_header('el9tv') ?>

<main id="main" role="main" class="main">

<section class="video-stream">
	   <div class="container">
	        <div class="bloc">     		           	
					<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post(); ?>
							<div class="col-md-8 col-sm-12 col-xs-12 fit-vertical">
								<div class="row">
									<div class="content-video">
										<iframe id="video-9tv" class="video" style="background-color: #FFF; border-bottom: 1px solid #333;" width="100%" height="427px" src="<?php echo get_post_meta(get_the_ID(), 'video_url', true) ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 fit-vertical">
								<div class="row">
									<div class="info-video single-video">
										<a href="<?php echo get_permalink(get_post_meta(get_the_ID(), 'programa', true)) ?>">
										<span class="programa"><?php echo get_the_title(get_post_meta(get_the_ID(), 'programa', true)) ?></span></a>
										<h1><?php the_title() ?></h1>										
										<p><?php the_content(); ?></p>
										<ul class="info-article">	
											<li class="compartir">Compartir</li>				           
								            <li class="sharer"><?php the_news_sharing() ?></li>
								        </ul>										
									</div>
								</div>
							</div><?php
						endwhile;
					endif;?>
				</div>	

			</div>
		</div>
	</section>

   <section class="videos-destacats">

   		<div class="container">
   			<div class="row">
				<div class="col-md-12 relacionats">

					<h2>Vídeos relacionats </h2>
						<div class="row"><?php
							$relacionats = new WP_Query( array( 
								'post_type' => 'video',
								'meta_key'		=> 'programa',
								'meta_value'	=> get_post_meta(get_the_ID(), 'programa', true),
								'posts_per_page' => 8
								)
							); 

							while ($relacionats->have_posts()) : $relacionats->the_post();
								 get_template_part('templates/videos/miniatura', 'relacionats');
							endwhile;
							wp_reset_query(); ?>
						</div>
					</div>
				</div>
			</div>
	</section>
</main>
<?php get_footer(); ?>