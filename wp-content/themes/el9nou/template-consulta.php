<?php
/**
 *
 *	Template name: Consulta
 */
global $ads;
$ads = false;

get_header(); ?>

<main id="main" role="main" class="main registre">
    <div class="container">
        <div class="row">
        	<div class="col-md-6 col-md-offset-3" id="consulta">
				<!--<h2>Consulta sobre preferències dels subscriptors</h2>-->
				
				<p>Diversos subscriptors i lectors d’EL 9 NOU s’han adreçat als serveis d’atenció al client per demanar que no se’ls encarti més <i>El Periódico</i>, perquè estan disconformes amb la cobertura informativa que ha fet aquest diari dels atemptats de Barcelona i Cambrils, de les investigacions posteriors i de la seva actual línia editorial.</p>
<p>Després de rebre les primeres queixes, EL 9 NOU va publicar <a href="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/09/ComunicatProsa.pdf" target="_blank">aquest comunicat</a>.</p>
<p>Per atendre totes les sensibilitats, EL 9 NOU pregunta ara als seus subscriptors si volen que continuï encartant de forma gratuïta <i>El Periódico</i> o si prefereixen que no.</p>
						
				<hr>
				<consulta inline-template v-cloak>
					<div id="pas1" v-if="! nom && ! finish">
						
						<div class="form-group">
							<label style="line-height: initial;">DNI del subscriptor</label>
							<input type="text" class="form-control" v-model="dni">
						</div>
						<div class="form-group">
							<button class="btn btn-primary" v-on:click="comprovarDni()">Comprovar DNI <i class="fa fa-spinner fa-spin fa-fw" v-if="loading"></i></button>
						</div>
					</div>
					
					<div id="pas2" v-if="nom && ! finish">
						<p>
							Hola, {{ nom }}<br>
							Agraïm la confiança que ens heu demostrat fent-vos subscriptor d'EL 9 NOU i també que ara ens feu arribar la vostra opinió.<br>
							<b>Voleu que EL 9 NOU continuï encartant de forma gratuïta <i>El Periódico?</i></b>
						</p>
						<div class="form-group">
							<label style="line-height: initial;">Correu electrònic</label>
							<input type="text" class="form-control" v-model="email">
						</div>
						<div class="form-group">
							<div class="radio">
							  <label style="line-height: initial;">
							    <input type="radio" name="optionsRadios" id="optionsRadios1" v-model="opcio" value="Sí">
							    Sí
							  </label>
							</div>
							<div class="radio">
							  <label style="line-height: initial;">
							    <input type="radio" name="optionsRadios" id="optionsRadios2" v-model="opcio" value="No">
							    No
							  </label>
							</div>
							<div class="radio">
							  <label style="line-height: initial;">
							    <input type="radio" name="optionsRadios" id="optionsRadios3" v-model="opcio" value="M’és indiferent">
							    M’és indiferent
							  </label>
							</div>
						</div>
						<div class="form-group">
							<button class="btn btn-primary" v-on:click="enviarVotacio()">Enviar opinió <i class="fa fa-spinner fa-spin fa-fw" v-if="loading"></i></button>
						</div>
					</div>
					
					<div class="alert alert-danger" v-if="error">{{ error }}</div>
					<div class="alert alert-success" v-if="success">{{ success }}</div>
				</consulta>
            	
            	<hr>
            	<p>Els subscriptors poden fer arribar la seva opinió a través d’aquesta pàgina web i, si no els és possible, també ho poden fer trucant <b>93 889 49 49</b>.</p>
        		
        	</div>
        </div>
	</div>
</main>

<?php get_footer(); ?>