<?php
get_header(); ?>

<?php
if (have_posts()) :
	while ( have_posts() ) : the_post() ?>
		<main id="main" role="main" class="main home">
		    <div class="container">
		        <div class="row">               
		            <div class="col-md-9">
		                <?php the_content() ?>
		            </div>
		            <?php get_sidebar() ?>
		        </div>
		    </div>
		</main><?php
		return;
	endwhile;
endif; ?>
<?php get_footer(); ?>