<?php
/**
 *
 *	Template name: EL TALLS 9FM
 */

get_header('el9fm');

if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
		<main id="main" role="main" class="main">
		  <section class="audio-stream">
				<div class="container">	              
					<div class="col-md-12">
						<div class="row">
							<h1><?php the_title() ?></h1>
						</div>
					</div>				
				</div>
			</section>

			<div class="container">
				<section class="audio-destacats">
					<div class="row">
							<div class="col-md-8">
								<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/<?php the_field('llista_id') ?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
								<?php /** <iframe width='100%' height='500' scrolling='no' frameborder='no' uwhisp_playlist_id="<?php the_field('llista_id') ?>" src='https://uwhisp.com/embed?playlist_id=<?php the_field('llista_id') ?>'></iframe> **/ ?>
							</div>
							<div class="col-md-4 col-sm-4 audio">
								<?php the_content() ?>
							</div>
						</div>
					</section>

		        </div>
   			</div>
		</main><?php
	endwhile;
endif;

get_footer(); ?>