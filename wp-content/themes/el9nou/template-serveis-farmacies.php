<?php
/**
 *
 *	Template name: Farmàcies
 */
get_header() ?>

<style type="text/css">
	/* MAPS */
	#map_canvas {
	width: 100%;
	height: 500px;
	}
</style>

<main id="main" role="main" class="main categoria">
	<div class="container" id="farmacies">

		<div class="row">

			<div class="col-md-12">

				<div class="barra-superior clearfix">
					<div class="breadcrumb">Farmàcies de guàrdia</div>
				</div>

				<div class="row avui-dema">
					<div class="col-md-6 text-left"><span>21 NOVEMBRE</span> - AVUI</div>
					<div class="col-md-6 text-right"><span>22 NOVEMBRE</span> - DEMÀ</div>
				</div>

				<div class="row">					
					<div class="col-md-4">
						<div class="farmacies-list">
							<h4>Vic</h4>
							<p>TERRICABRAS - VILELLA C. Candi Bayés, 60</p>

							<h4>Manlleu</h4>
							<p>MATARÓ Avda. Roma, 102</p>

							<h4>Torelló</h4>
							<p>NOFRE C. Manlleu, 31</p>

							<h4>Roda de Ter</h4>
							<p>BERNET C. Jacint Verdaguer, 1</p>

							<h4>Sant Quirze de Besora</h4>
							<p>NOFRE C. Manlleu, 31</p>

							<h4>Ripoll</h4>
							<p>BERNET C. Jacint Verdaguer, 1</p>

							<h4>Sant Joan de les Abadesses</h4>
							<p>MATARÓ Roma, 102</p>

						</div>
					</div>
					<div class="col-md-8">
						<div id="map_canvas"></div>
					</div>

				</div><!-- / row -->

				<div class="row calendari">
					<div class="col-md-4">
						<img src="<?php echo get_template_directory_uri(); ?>/img/calendari.png" style="max-width: 60px;">
						<p>Selecciona el dia per a saber quines farmàcies de guàrdia hi ha a la comarca.</p>

					</div>
					<div class="col-md-8">						
						
						 <div id='calendar'></div>
					</div>
				</div><!-- / row -->

			</div><!-- col-md-12 -->

		</div><!-- row -->

		<?php // get_sidebar() ?>
	</div>
</main>
<!-- / section -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyDvwiaiyeJAVlTDGzG2SuxnY3wTWAkn4XQ"></script> 

<script>
	function initialize() {
	// Create an array of styles.
	var nou9Styles = [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}];
	// Create a new StyledMapType object, passing it the array of styles,
	// as well as the name to be displayed on the map type control.
	var nou9MapType = new google.maps.StyledMapType(nou9Styles,
		{name: "nou9"});
	/* MAPA GENERAL */
	var myLatlng = new google.maps.LatLng(42.1, 2.222437);
	var myOptions = {
		zoom: 10,
		center: myLatlng,
		scrollwheel: false,
		disableDefaultUI: false,
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	/* -----------------------------------------------
	VIC
	------------------------------------------------*/
	var marker4 = new google.maps.Marker({
		position: new google.maps.LatLng(41.921436, 2.250247),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Baró Joiers"
	});
	var contentStringBarna = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">TERRICABRAS - VILELLA</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">C. Candi Bayés, 60</p>' +
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringBarna
	});
	marker4.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringBarna);//update the content for this marker
		infowindow.open(map, marker4);
	});
	/* -----------------------------------------------
	MANLLEU
	------------------------------------------------*/
	var marker6 = new google.maps.Marker({
		position: new google.maps.LatLng(42.0053, 2.288862),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Joyería Calvo"
	});
	var contentStringCoruna = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">MATARÓ</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">Avda. Roma, 102</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringCoruna
	});
	marker6.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringCoruna);//update the content for this marker
		infowindow.open(map, marker6);
	});
	/* -----------------------------------------------
	Torelló
	------------------------------------------------*/
	var marker7 = new google.maps.Marker({
		position: new google.maps.LatLng(42.045848, 2.266614),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Joyería Calvo"
	});
	var contentStringLugo = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">NOFRE</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">C. Manlleu, 31</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringLugo
	});
	marker7.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringLugo);//update the content for this marker
		infowindow.open(map, marker7);
	});
	/* -----------------------------------------------
	Roda de Ter
	------------------------------------------------*/
	var marker2 = new google.maps.Marker({
		position: new google.maps.LatLng(41.975892, 2.303808),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Estrada Joiers"
	});
	var contentStringManresa = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">BERNET</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">C. Jacint Verdaguer, 1</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringManresa
	});
	marker2.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringManresa);//update the content for this marker
		infowindow.open(map, marker2);
	});
	/* -----------------------------------------------
	Sant Quirze de Besora
	------------------------------------------------*/
	var marker8 = new google.maps.Marker({
		position: new google.maps.LatLng(42.1, 2.222437),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Joyería Calvo"
	});
	var contentStringLeon = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">NOFRE</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">C. Manlleu, 31</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringLeon
	});
	marker8.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringLeon);//update the content for this marker
		infowindow.open(map, marker8);
	});
	/* -----------------------------------------------
	Ripoll
	------------------------------------------------*/
	var marker3 = new google.maps.Marker({
		position: new google.maps.LatLng(42.199459, 2.190762),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Ramoncita Tarragona"
	});
	var contentStringTarragona = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">BERNET</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">C. Jacint Verdaguer, 1</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringTarragona
	});
	marker3.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringTarragona);//update the content for this marker
		infowindow.open(map, marker3);
	});
	/* -----------------------------------------------
	Sant Joan de les Abadesses
	------------------------------------------------*/
	var marker5 = new google.maps.Marker({
		position: new google.maps.LatLng(42.234129, 2.286307),
		icon: 'http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/map-marker.svg',
		map: map,
		title:"Joieria Ramoncita"
	});
	var contentStringTortosa = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">MATARÓ</h1>'+
	'<div id="bodyContent">'+
	'<p style="margin-top: 5px;">Roma, 102</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentStringTortosa
	});
	marker5.addListener('click', function() {
		infowindow.close();//hide the infowindow
		infowindow.setContent(contentStringTortosa);//update the content for this marker
		infowindow.open(map, marker5);
	});
	
	// Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('nou9', nou9MapType);
	map.setMapTypeId('nou9');
}
	//setTimeout(function () { google.maps.event.addDomListener(window, 'load', initialize); }, 450);
	google.maps.event.addDomListener(window, 'load', initialize); 
</script>

<?php get_footer(); ?>

