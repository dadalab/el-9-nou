<?php get_header(); ?>
<main id="main" role="main" class="main">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">Resultats per: <?php echo get_search_query(); ?></div>
                    <div class="share"></div>
                </div> 

                <!-- noticies actuals -->
                <section id="noticies-recents">     
                    <div class="row">
                        <div class="bloc" style="display: block"><?php
                            if (have_posts()) :
                                while (have_posts()) : the_post();
                                    if ($_GET['post_type'] == 'seccio_radio') :
                                        get_template_part('templates/noticies/resultats', 'buscador-radio');
                                    else :
                                        get_template_part('templates/noticies/resultats', 'buscador');
                                    endif;
                                endwhile;
                            endif; ?>
                        </div>
                    </div>
                </section><?php
                global $wp_query;
                if ($wp_query->max_num_pages > 1) : ?>
                    <div class="row">
                        <nav class="prev-next-posts">
                            <div class="prev-posts-link">
                                <?php echo get_next_posts_link('Notícies més antigues', $wp_query->max_num_pages); ?>
                            </div>
                            <div class="next-posts-link">
                                <?php echo get_previous_posts_link('Notícies més recents'); ?>
                            </div>
                        </nav>
                    </div>
                <?php endif ?>
            </div>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>
<?php get_footer()  ?>