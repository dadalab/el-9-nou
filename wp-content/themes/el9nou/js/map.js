function initialize() {
// Create an array of styles.
var mimohStyles = [
{
featureType: 'all',
stylers: [
{ saturation: -100 }
]
}
];
// Create a new StyledMapType object, passing it the array of styles,
// as well as the name to be displayed on the map type control.
var mimohMapType = new google.maps.StyledMapType(mimohStyles,
{name: "mimoh"});
/* MAPA GENERAL */
var myLatlng = new google.maps.LatLng(42.24602, -2.98131);
var myOptions = {
zoom: 7,
center: myLatlng,
scrollwheel: false,
disableDefaultUI: false,
mapTypeControl: false,
mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
/* -----------------------------------------------
BARCELONA
------------------------------------------------*/
var marker4 = new google.maps.Marker({
position: new google.maps.LatLng(41.40284, 2.16034),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Baró Joiers"
});
var contentStringBarna = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Baró Joiers</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Travessera de Gràcia, 198<br>' +
'08012 Barcelona<br>'+
'T 932 137 577</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringBarna
});
marker4.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringBarna);//update the content for this marker
infowindow.open(map, marker4);
});
/* -----------------------------------------------
A CORUÑA
------------------------------------------------*/
var marker6 = new google.maps.Marker({
position: new google.maps.LatLng(43.36910, -8.40238),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Joyería Calvo"
});
var contentStringCoruna = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Joyería Calvo</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Rúa Nova, 6<br>' +
'15003 Coruña (Coruña)<br>'+
'T 981 229 908</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringCoruna
});
marker6.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringCoruna);//update the content for this marker
infowindow.open(map, marker6);
});
/* -----------------------------------------------
LUGO
------------------------------------------------*/
var marker7 = new google.maps.Marker({
position: new google.maps.LatLng(43.01011, -7.55729),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Joyería Calvo"
});
var contentStringLugo = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Joyería Calvo</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Rúa Conde Pallares, 5<br>' +
'27001 Lugo<br>'+
'T 982 230 321</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringLugo
});
marker7.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringLugo);//update the content for this marker
infowindow.open(map, marker7);
});
/* -----------------------------------------------
Manresa
------------------------------------------------*/
var marker2 = new google.maps.Marker({
position: new google.maps.LatLng(41.72881, 1.82377),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Estrada Joiers"
});
var contentStringManresa = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Estrada Joiers</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Passeig de Pere III, 45<br>' +
'08242 Manresa (Barcelona)<br>'+
'T 938 776 053</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringManresa
});
marker2.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringManresa);//update the content for this marker
infowindow.open(map, marker2);
});
/* -----------------------------------------------
PONFERRADA
------------------------------------------------*/
var marker8 = new google.maps.Marker({
position: new google.maps.LatLng(42.54766, -6.60071),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Joyería Calvo"
});
var contentStringLeon = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Joyería Calvo</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Av. España, 22<br>' +
'24400 Ponferrada (León)<br>'+
'T 987 413 958</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringLeon
});
marker8.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringLeon);//update the content for this marker
infowindow.open(map, marker8);
});
/* -----------------------------------------------
TARRAGONA
------------------------------------------------*/
var marker3 = new google.maps.Marker({
position: new google.maps.LatLng(41.11519, 1.25454),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Ramoncita Tarragona"
});
var contentStringTarragona = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Ramoncita Tarragona</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Carrer Sant Agustí, 21<br>' +
'43003 Tarragona<br>'+
'T 977 249 743</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringTarragona
});
marker3.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringTarragona);//update the content for this marker
infowindow.open(map, marker3);
});
/* -----------------------------------------------
TORTOSA
------------------------------------------------*/
var marker5 = new google.maps.Marker({
position: new google.maps.LatLng(40.81161, 0.52109),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Joieria Ramoncita"
});
var contentStringTortosa = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Joieria Ramoncita</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Carrer d´en Carbó, 14<br>' +
'43500 Tortosa (Tarragona)<br>'+
'T 977 441 662</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringTortosa
});
marker5.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringTortosa);//update the content for this marker
infowindow.open(map, marker5);
});
/* -----------------------------------------------
VIC
------------------------------------------------*/
var marker1 = new google.maps.Marker({
position: new google.maps.LatLng(41.93075,  2.25210),
icon: 'http://www.mimoh.net/media/tmp/catalog/product/c/o/cor-mimoh.png',
map: map,
title:"Frasquet Joiers"
});
var contentStringVic = '<div id="content">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 style="text-align:left; margin-bottom: 0px; margin-top: 0px;">Frasquet Joiers</h1>'+
'<div id="bodyContent">'+
'<p style="margin-top: 5px;">Carrer Jacint Verdaguer, 14<br>' +
'08500 Vic (Barcelona)<br>'+
'T 938 854 950</p>'+
'</div>'+
'</div>';
var infowindow = new google.maps.InfoWindow({
content: contentStringTortosa
});
marker1.addListener('click', function() {
infowindow.close();//hide the infowindow
infowindow.setContent(contentStringVic);//update the content for this marker
infowindow.open(map, marker1);
});
// Associate the styled map with the MapTypeId and set it to display.
map.mapTypes.set('mimoh', mimohMapType);
map.setMapTypeId('mimoh');
}
//setTimeout(function () { google.maps.event.addDomListener(window, 'load', initialize); }, 450);
google.maps.event.addDomListener(window, 'load', initialize); 