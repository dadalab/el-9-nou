var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];

  googletag.cmd.push(function() {
    googletag.defineSlot('/119069366/lateral-1-osona-ripolles', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-0').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-1-valles-oriental', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-1').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-2-osona-ripolles', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-2').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-2-valles-oriental', [[300, 300], [300, 600], [234, 60], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-3').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-3-osona-ripolles', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-4').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-3-valles-oriental', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-5').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-4-osona-ripolles', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-6').addService(googletag.pubads());
    googletag.defineSlot('/119069366/lateral-4-valles-oriental', [[300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1516178654378-7').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
