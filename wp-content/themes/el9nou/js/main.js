var user = null;

$( document ).ready(function() {

    /////////////////////////////////////
    // HAMBURGER MENU
    var $hamburger = $("header .hamburger, #navegation-site .hamburger, #navigation .hamburger, .smaller .hamburger");
    var $body = $("body");
    var $overlay = $("#navegation-site");
    var $share = $(".sharer, .share, .overlay-share .hamburger");
    var $overlayShare = $(".overlay-share");
    var $search = $(".search-btn,  .overlay-search .hamburger");
    var $overlaySearch = $(".overlay-search");
    var $edition = $(".select-edition");
    var $overlayEdition = $(".overlay-edition");
    var $plus = $(".esquela-item");


    $hamburger.on("click", function(e) {
      e.preventDefault();
      $body.toggleClass("in");
      $overlay.toggleClass("in");
      tempsVue.actualitzaTemps();
  });

    $share.on("click", function(e) {
      e.preventDefault();
      $body.toggleClass("in");
      $overlayShare.toggleClass("in");
  });

    $search.on("click", function(e) {
      e.preventDefault();
      $body.toggleClass("in");
      $overlaySearch.toggleClass("in");
     // FocusOnInput();
 });

    $plus.on("click", function(e) {
      e.preventDefault();
      $(this).find(".esquela-content").toggleClass("open");
      $grid.masonry();

      if ($(this).find('.esquela-content').hasClass('open')) {
        $(this).find(".fa-plus").removeClass('fa-plus').addClass('fa-minus');
    }
    else {
        $(this).find(".fa-minus").removeClass('fa-minus').addClass('fa-plus');
    }

});

   // ESC amagem menus i buscador.
   $(document).keyup(function(e) {
    if (e.which == 27) {
      $overlaySearch.removeClass("in");
      $overlay.removeClass("in");
  }
});

   /*$edition.on("click", function(e) {
      e.preventDefault();
      console.log('up');
  }); */
   /*$('.selector-edicio').on("click", function(e) {
      console.log('aa');
  }) */

    ///////////////////////////////////////
    // PERIODISME CIUTADÀ
    var $bannerpc = $(".periodisme-ciutada");
    var $overlaypc = $("#PC-Overlay");
    var $hamburger2 = $(".hamburger2");

    $(".periodisme-ciutada, .hamburger2, .digues-la-teva").on("click", function(e) {
      e.preventDefault();
      $body.toggleClass("in");
      $overlaypc.toggleClass("in");
  });

    var windowWidth = $(window).width();

    //////////////////////////////////////////
    // MENU DESPLEGALBE MOBILE
    /////////////////////////////////////////

    if(windowWidth < 768) {

      $("h2.options").on("click", function(e) {
         e.preventDefault();

         if ( $(this).hasClass( "minus" ) ) {
            $(this).removeClass("minus");
            $(this).next("ul.menu").removeClass("down");
        } else {
            $("h2.options").removeClass("minus");
            $("ul.menu").removeClass("down");
            $(this).addClass("minus");
            $(this).next("ul.menu").addClass("down");
        }

      // $("ul.menu").slideUp(300);
      // $(this).next("ul.menu").slideToggle(300);
      // $(this).toggleClass("minus");
  });

      $("h2.title").click(function(e) {
        e.preventDefault();
        $(".toggle").slideToggle(250);
    });

      $(".grup-corporatiu h2").click(function(e) {
        e.preventDefault();
        //$(".logos-corporative").slideToggle(250);

        if ( $(this).hasClass( "minus2" ) ) {
          $(this).removeClass("minus2");
          $(".logos-corporative").slideToggle(250);
      } else {
          $(this).removeClass("minus2");
          $(this).addClass("minus2");
          $(".logos-corporative").slideToggle(250);
      }

  });

  }
});

/////////////////////////////////////////
// SEARCH FOCUS INPUT
function FocusOnInput()
{
 // document.getElementById("search").focus();
}

/////////////////////////////////////////
// SELECT HOME EDITION
$( document ).ready(function() {
  // Run code
  var $edicioOverlay = $(".overlay-edition");
  var $body = $("body");

  // center center
  var leftH = $(".left").height();
  var leftW = $(".left").width();
  var overlayh2 = $('.overlay-edition .cc').outerHeight();
  var overlayw2 = $('.overlay-edition .cc').width();

  $('.overlay-edition .cc').css('top', ((leftH - overlayh2) / 2) + 'px');
  $('.overlay-edition .cc').css('left', ((leftW - overlayw2) / 2) + 'px');

});


///////////////////////////////////////////////
// ONLOAD
$(window).on("load",function(){

    // center overlay
    var windowh = $(window).height();
    var windoww = $(window).width();
    var overlayh = $('.overlay-share .overlayInner').outerHeight();
    var overlayw = $('.overlay-share .overlayInner').width();

    var overlayh3 = $(' #navegation-site .overlayInner').outerHeight();
    var overlayw3 = $(' #navegation-site .overlayInner').width();

    $('.overlay-share .overlayInner').css('top', ((windowh - overlayh) / 2) + 'px');
    $('.overlay-share .overlayInner').css('left', ((windoww - overlayw) / 2) + 'px');

    $('#navegation-site .overlayInner').css('top', ((windowh - overlayh3) / 2) + 'px');
    $('#navegation-site .overlayInner').css('left', ((windoww - overlayw3) / 2) + 'px');

 //   $('iframe').find('img').css({with: '262px', 'height': 'auto'});

});

///////////////////////////////////////////////
// RESIZE
$(window).on("resize",function(){

    // center overlay
    var windowh = $(window).height();
    var windoww = $(window).width();

    var overlayh = $('.overlay-share .overlayInner').outerHeight();
    var overlayw = $('.overlay-share .overlayInner').width();

    $('.overlay-share .overlayInner').css('top', ((windowh - overlayh) / 2) + 'px');
    $('.overlay-share .overlayInner').css('left', ((windoww - overlayw) / 2) + 'px');

    // center center
    var leftH = $(".left").height();
    var leftW = $(".left").width();

    var overlayh2 = $('.overlay-edition .cc').outerHeight();
    var overlayw2 = $('.overlay-edition .cc').width();

    $('.overlay-edition .cc').css('top', ((leftH - overlayh2) / 2) + 'px');
    $('.overlay-edition .cc').css('left', ((leftW - overlayw2) / 2) + 'px');
});


// Scroll Down HEADER ANIMATION
$(function(){
  $(window).scroll(function(){
    if ($(this).scrollTop() > 190){
      $('.smaller').addClass('in');
  }
  else{
      $('.smaller').removeClass('in');
  }
});
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

//Superfish
jQuery('ul.sf-menu').superfish();


$(document).ready(function() {

  var owl = $("#owl-demo");

  owl.owlCarousel({
    items : 4,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [979,2],
    itemsTablet: [768,1],
    navigation: false,
       // loop: true,
       navigationText: [
       "<span class='icon9fm-angle-left'></span>",
       "<span class='icon9fm-angle-right'></span>"
       ]
   });

     // Custom Navigation Events
     $(".left-arrow").click(function(e){
      e.preventDefault();
      owl.trigger('owl.prev');
  });

     $(".right-arrow").click(function(e){
      e.preventDefault();
      owl.trigger('owl.next');
  });

     $('.owl-carousel-noticia').owlCarousel({
      items: 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,1],
      itemsTablet: [768,1],
      navigation: true,
      navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
  });


     /* /////////////// */

    /** $('#calendar').fullCalendar({
      header: {
        left: 'title',
        center: 'month',
        right: 'prev,next today'
    },
    lang: 'ca',
            navLinks: true // can click day/week names to navigate views
        });

        **/
    });

/* /// PRINT FUNCTION ////// */
function printFunction() {
  window.print();
}

/* //////////////////////////
$('iframe').load(function() {
    $(this).contents().find('img').css({with: '100%', 'height': 'auto'});
})
*/



var SidebarBlogosfera = Vue.extend({
  template: ''
  + '<div class="most-viewed-news blogosfera hidden-xs hidden-sm">'
  +   '<a href="http://blogosfera.el9nou.cat"><h1>Blogosfera</h1></a>'
  +       '<div class="news-items">'
  +           '<a href="{{ item.url }}" target="_blank" v-for="item in content">'
  +               '<div class="news"><div class="icon">{{ item.data }}</div><h2>{{ item.nom }}</h2></div>'
  +           '</a>'
  +       '</div>'
  +   '<div class="gradient"></div>'
  +'</div>'
  +'<a href="http://blogosfera.el9nou.cat" class="veure-tota-hemeroteca">Anar a la Blogosfera</a>',

  data: function () {
    return {
      content: '',
      loading: true,
  }
},

ready: function() {
    this.$http.get('http://blogosfera.el9nou.cat/wp-json/api/ultimes').then(function(response) {
      this.content = response.json();
  });
}
});

/**var SidebarAgenda = Vue.extend({
    props: ['edicio'],
    data: function () {
        return {
            content: '',
            loading: true,
        }
    },

    ready: function() {
        this.$http.get('http://agenda.el9nou.cat/api/exportEvents.php?edicio=' + this.edicio).then(response => {
            this.content = response.json();
        });
    }
})**/

var TemplateElTemps = {

    data: function () {
      return {
        'ciutat' : null,
        'previsio' : null,
        'actual': null,
        'icons': null,
        'diesSetmana' : ['Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte', 'Diumenge' ],
        'loading': false,
    }
},

ready: function() {
  if (! this.ciutat) {
    this.getGeolocation();
}
this.getWeatherForCity();
},

methods: {
  getGeolocation: function() {
            //this.ciutat = "<?php echo (edicio_actual() == 'osona-ripolles') ? 'Vic' : 'Granollers'; ?>";
        },

        showDate: function(data) {
            var d = new Date(data);
            var hours = d.getHours();
            var minutes = d.getMinutes();
            if (hours < 10) hours = '0' + hours;
            if (minutes < 10) minutes = '0' + minutes;
            return hours + ':' + minutes;
        },
        showDay: function(data) {
            var d = new Date(data);
            var day = d.getDay();
            return day;
        },

        getWeatherForCity: function() {
            this.loading = true;
            var geoAPI = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.ciutat + '&key=AIzaSyDPYtRNSQhvIsoNGiQlAZ4cWz-PRKXEC2s&language=ca';
            var that = this;

            this.$http.get(geoAPI, {credentials: false}).then(function(response) {
	            response = response.json();
				var lat = response.results[0].geometry.location.lat;
              var lng = response.results[0].geometry.location.lng;
              that.ciutat = response.results[0].address_components[0].long_name;
              jQuery.ajax({
                url : "/wp-admin/admin-ajax.php",
                type : 'post',
                dataType: 'json',
                data : {
                  action : 'get_temps',
                  lat : lat,
                  lng : lng
              },
              success : function( data ) {
                  that.previsio = data;
                  that.loading = false;
              }
  			});
            //$.get( geoAPI, function( data ) {

          //});
          });
        },
        searchCity: function() {
            this.getWeatherForCity();
        },

        setWeatherIcon: function(code) {
            var icons = {
              'clear-day': 'day-sunny',
              'clear-night': 'night-clear',
              'rain': 'rain',
              'snow': 'snow',
              'sleet': 'sleet',
              'wind': 'strong-wind',
              'fog': 'fog',
              'cloudy': 'cloudy',
              'partly-cloudy-day': 'day-cloudy',
              'partly-cloudy-night': 'night-cloudy',
              'hail': 'hail',
              'thunderstorm': 'thunderstorm',
              'tornado': 'tornado',
          };

          return '<i class="wi wi-' + icons[code] + '"></i>';
      },
  }
};

new Vue({
    el: '#templateeltemps',
    components: {
      'template-el-temps': TemplateElTemps
  }
});
// register
//Vue.component('external-content', ExternalContent);
//Vue.component('sidebar-blogosfera', SidebarBlogosfera);
//Vue.component('sidebar-agenda', SidebarAgenda);
//Vue.component('template-el-temps', TemplateElTemps);

// create a root instance
//new Vue({
//  el: '#main'
//})


/***** SIDE AGENDA VUEJS ELEMENT ****/
var SideAgenda = {
	props: ['edicio'],

  data: function () {
    return {
      content: '',
      loading: true,
  }
},

ready: function() {
    this.$http.get('http://agenda.el9nou.cat/api/exportEvents.php?edicio=' + this.edicio).then(function(response) {
      this.content = response.json();
  });
}
}

new Vue({
  el: '#sidebaragenda',
  components: {
    'sidebar-agenda': SideAgenda
}
});


/***** EXTERNAL CONTENT VUEJS ELEMENT ****/
var ExternalContent = {
  template: '<section class="articles-petits"><h3>{{ title }}</h3>'
  + '<div class="col-md-4 col-sm-4 col-xs-12 item article-imatge" v-for="item in content">'
  +        '<a href="{{ item.url }}">'
  +            '<figure class="row" style="background-image: url({{ item.urlDestacada }});-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;background-repeat: no-repeat; background-position: 50% 50%;position:relative">'
  +              '<div class="video-hover" v-if="video == 1"><i class="icon-play" aria-hidden="true"></i></div>'
  +            '</figure>'
  +            '<h1>{{ item.nom }}</h1>'
  +        '</a>'
  +    '</div></section>',

  props: ['title', 'apiUrl', 'video'],

  data: function () {
    return {
      content: '',
      loading: true,
  }
},

ready: function() {
    this.$http.get(this.apiUrl).then(function(response) {
      this.content = response.json();
  });
}
}

new Vue({
  el: '.externalcontent',
  components: {
    'external-content': ExternalContent
}
});

new Vue({
  el: '#externalFotogaleries',
  components: {
    'external-content': ExternalContent
}
});

new Vue({
  el: '#externalMagazin',
  components: {
    'external-content': ExternalContent
}
});


/***** SIDEBAR BLOGOSFERA VUEJS ELEMENT ******/
var SidebarBlogosfera = {
  template: ''
  + '<div class="most-viewed-news blogosfera hidden-xs hidden-sm">'
  +   '<a href="http://blogosfera.el9nou.cat"><h1>Blogosfera</h1></a>'
  +       '<div class="news-items">'
  +           '<a href="{{ item.url }}" target="_blank" v-for="item in content">'
  +               '<div class="news"><div class="icon">{{ item.data }}</div><h2>{{ item.nom }}</h2></div>'
  +           '</a>'
  +       '</div>'
  +   '<div class="gradient"></div>'
  +'</div>'
  +'<a href="http://blogosfera.el9nou.cat" class="veure-tota-hemeroteca">Anar a la Blogosfera</a>',

  data: function () {
    return {
      content: '',
      loading: true,
  }
},

ready: function() {
    this.$http.get('http://blogosfera.el9nou.cat/wp-json/api/ultimes').then(function(response) {
      this.content = response.json();
  });
}
};

new Vue({
  el: '#sidebarblogosfera',

  components: {
    'sidebar-blogosfera': SidebarBlogosfera
}
});


/***** User Logged In ****/
var LoggedIn = {
  props: ['edicio'],

  data: function () {
    return {
      user: '',
      loading: true,
  }
},

    ready: function() {
        $.ajaxSetup({
            xhrFields: {
                withCredentials: true
            }
        });

        var that = this;
        $.ajax({ url: "http://9club.el9nou.cat/api/usuari", }).done(function(data) {
            that.user = data.user;
            user = data.user;
            that.loading = false;
        });
    }
}

new Vue({
  el: '#user-logged-in',

  components: {
    'user-logged-in': LoggedIn
}
});

/** Hemeroteca **/

var Hemeroteca = {

  data: function () {
	return {
	      user: '',
	      loading: false,
	      diari: '',
  		}	
	},

	methods: {
	    descarrega : function() {
		   //url = this.element;
		   console.log('asasasa');
	       console.log(this.diari);
	      this.loading = true;
	      var that = this;
	      if (! user) {
	        window.location.href = 'http://9club.el9nou.cat/osona-ripolles/entra?type=restricted&redirect=' + window.location.href;
	        this.loading = false;
	        return;
	    }
	
	//    $.get({ url: "/wp-json/api/hemeroteca/generar-link?id=" + url + "&fitxer=" + fitxer + "&api_token=" + user.api_token}).done(function(data) {
		    $.get({ url: "/wp-json/api/hemeroteca/generar-link?id=" + this.diari + "&fitxer=1&api_token=" + user.api_token}).done(function(data) {
		        that.loading = false;
		        console.log(data);
		        if (data.status == 'success') {
		          if (data.url) {
		            window.location.href = data.url;
		        	}
		    	}
				else {
					$('#no-permission').modal()
				}
			});
		}
	},
}
new Vue({
  el: '#hemeroteca',

  components: {
    'hemeroteca-diari': Hemeroteca
}
});

/***** Comments ****/
var Comments = {
  props: ['site', 'id', 'edicio'],
  data: function () {
    return {
      initialized: false,
      comments: '',
      text: '',
      loading: false,
      success: '',
      acting_as: ''
  }
},

ready: function() {
    console.log('edicio');
    console.log(this.edicio);
    var that = this;
    var loadPosition = $('#comments').offset().top - $(window).height() - 300;
    $( document ).scroll(function(value) {
      if (! this.initialized && loadPosition < $(window).scrollTop()) {
        this.initialized = true;
        that.getComments();
    }
});

},
methods: {
    getComments: function() {
      this.loading = true;
      this.$http.get('http://9club.el9nou.cat/api/comentaris?site=' + this.site + '&id=' + this.id, {credentials: true}).then(function(response) {
        this.acting_as = response.json().acting_as;
        this.comments = response.json().comments;
        this.loading = false;
        this.loadingButton = false;
    });
  },

  anarAComentari: function() {
      $('html,body').animate({scrollTop: $('#commentform').offset().top});
  },

  enviarComentari: function() {
      this.loadingButton = true;
      this.$http.post('http://9club.el9nou.cat/api/comentaris?site=' + this.site + '&id=' + this.id, { 'text' : this.text }, {credentials: true}).then(function(response) {
        this.comments.push(response.json());
        this.success = 'Comentari publicat correctament!';
        this.loadingButton = false;
        this.text = '';
    });
  }
}
}

new Vue({
    el: '#comments',

    components: {
        'comments': Comments
    },
    ready: function() {
        this.$http.get('http://blogosfera.el9nou.cat/wp-json/api/ultimes').then(function(response) {
          this.content = response.json();
      });
    }
});

new Vue({
  el: '#sidebarblogosfera',

  components: {
    'sidebar-blogosfera': SidebarBlogosfera
}
});



var Consulta = {

  data: function () {
    return {
      dni: '',
      nom: '',
      email: '',
      opcio: '',
      loading: false,
      error: '',
      success: '',
      finish: false
  }
},

methods: {
    comprovarDni: function() {
       this.loading = true;
       this.error = '';
       var that = this;
       this.$http.post('http://el9nou.cat/wp-json/api/el9nou/consulta', {dni: this.dni}).then(function(response) {
          this.loading = false;
          var data = response.json();
          console.log(data);
          if (data.status == 'error')
          {
            that.error = data.message;
        }
        else
        {
            that.nom = data.nom;
        }

    });

   },

   enviarVotacio: function() {
       this.loading = true;
       this.error = '';
       var that = this;
       this.$http.post('http://el9nou.cat/wp-json/api/el9nou/consulta/resposta', {dni: this.dni, email: this.email, opcio: this.opcio}).then(function(response) {
          this.loading = false;
          var data = response.json();
          if (data.status == 'error')
          {
            that.error = data.message;
        }
        else
        {
            that.success = data.message;
            that.finish = true;
        }

    });
       console.log(this.dni);
   },

},
};

/***** SIDEBAR BLOGOSFERA VUEJS ELEMENT ******/
var SidebarBlogosfera = {
  template: ''
  + '<div class="most-viewed-news blogosfera hidden-xs hidden-sm">'
  +   '<a href="http://blogosfera.el9nou.cat"><h1>Blogosfera</h1></a>'
  +       '<div class="news-items">'
  +           '<a href="{{ item.url }}" target="_blank" v-for="item in content">'
  +               '<div class="news"><div class="icon">{{ item.data }}</div><h2>{{ item.nom }}</h2></div>'
  +           '</a>'
  +       '</div>'
  +   '<div class="gradient"></div>'
  +'</div>'
  +'<a href="http://blogosfera.el9nou.cat" class="veure-tota-hemeroteca">Anar a la Blogosfera</a>',

  data: function () {
    return {
      content: '',
      loading: true,
  }
},

ready: function() {
    //this.$http.get('http://blogosfera.el9nou.cat/wp-json/api/ultimes').then(response => {
    //  this.content = response.json();
  	//});
  	this.$http.get('http://blogosfera.el9nou.cat/wp-json/api/ultimes').then(function(response) {
      this.content = response.json();
  	});
}
};

new Vue({
  el: '#sidebarblogosfera',

  components: {
    'sidebar-blogosfera': SidebarBlogosfera
}
});



/**var Consulta = {

  data: function () {
    return {
      dni: '',
      nom: '',
      email: '',
      opcio: '',
      loading: false,
      error: '',
      success: '',
      finish: false
  }
},

methods: {
    comprovarDni: function() {
       this.loading = true;
       this.error = '';
       var that = this;
       this.$http.post('http://el9nou.cat/wp-json/api/el9nou/consulta', {dni: this.dni}).then(response => {
          this.loading = false;
          var data = response.json();
          console.log(data);
          if (data.status == 'error')
          {
            that.error = data.message;
        }
        else
        {
            that.nom = data.nom;
        }

    });

   },

   enviarVotacio: function() {
       this.loading = true;
       this.error = '';
       var that = this;
       this.$http.post('http://el9nou.cat/wp-json/api/el9nou/consulta/resposta', {dni: this.dni, email: this.email, opcio: this.opcio}).then(response => {
          this.loading = false;
          var data = response.json();
          if (data.status == 'error')
          {
            that.error = data.message;
        }
        else
        {
            that.success = data.message;
            that.finish = true;
        }

    });
       console.log(this.dni);
   },

},
};

new Vue({
  el: '#consulta',

  components: {
    'consulta': Consulta
}
});**/


/** NoticiaPremium **/

var NoticiaPremium = {
    props: ['id'],
  data: function () {
    return {
        user: '',
        loading: false,
        content: '',
    }
},

ready: function() {
        this.loading = true;
        var that = this;
        //console.log(this.id);
        $.ajaxSetup({
            xhrFields: {
                withCredentials: true
            }
        });

        var that = this;
        $.ajax({ url: "http://9club.el9nou.cat/api/usuari/permisos?pot-premium=1", }).done(function(data) {

            if (data.allowed)
            {
	            $( "#noticia-premium" ).load( 'http://el9nou.cat/wp-json/api/el9nou/obte-contingut-premium?api_token=' + data.user.api_token + '&id=' + that.id, function( response, status, xhr ) {
		            console.log(data);
					  if ( status == "error" ) {
					    var msg = "Sorry but there was an error: ";
					    $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
					  }
					  ga('send', 'event', 'Noticia Premium', 'llegida', data.user.nom + ' ' + data.user.cognoms );
					});

            }
            else {
                that.content = false;
                that.loading = false;
            }

        });
    },
}
new Vue({
    el: '#noticia-premium',

    components: {
        'noticia-premium': NoticiaPremium
    }
});