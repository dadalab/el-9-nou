<?php get_header() ?>

<main id="main" role="main" class="main">
   <div class="container">
        <div class="row">               
           <div class="col-md-12">

				<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					endif;
				?>
			 </div>

        </div><!-- / row -->
   </div>
</main>
<!-- / section -->

<?php get_footer(); ?>