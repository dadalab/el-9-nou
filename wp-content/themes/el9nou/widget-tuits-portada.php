<?php
/*
 *  Template name: Tuits Mòdul
 */
?>
<!doctype html>
<html class="no-js" lang="">
 <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo wp_title() ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://use.fontawesome.com/f0cbaa2dc7.js"></script>        
        <?php wp_head() ?> 

        <style>
            #loader-home {
                position: absolute;
                left: 0;
                right: 0;
                margin: auto;
                display: block;
                width: 70px;
                top: 50%;
                margin-top: -35px;
            }
        </style>       
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <main id="main" role="main" class="main home">
            <div class="container">
                <div class="row">               
                    <div class="col-md-9">



                
                    <section class="articles-compostos">
                        <h3>L'ESTRENA DEL NOU DIARI DIGITAL</h3>
                        <div class="col-md-4 col-sm-4 col-xs-12 item">    
                                <p>Quin gust, pels que som d'El 9 Nou de tota la vida, saber que des d'ara us tindrem més,  més a prop i més sovint. Com diria Zuckerberg, m'agrada.<br>
                                <span>#estàspreparat?</span>
                                </p>
                                <header>
                                    <h1>Carles Capdevila</h1>
                                    <h2>director-fundador del diari Ara</h2>
                                    <figure><img src="<?php echo get_template_directory_uri() ?>/img/carles-capdevila.jpg"></figure>                          
                                </header>                            
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 item">  
                                <p>Desitjo un bon inici per el nou format d’EL 9 NOU. Que sigui tant exitós com fins ara i que segueixi apostant per l’hoquei<br>
                                <span>#estàspreparat?</span>
                                </p>
                                <header>
                                    <h1>NATASHA LEE</h1>
                                    <h2>jugadora d’hoquei patins del CP Voltregà</h2>
                                    <figure><img src="<?php echo get_template_directory_uri() ?>/img/natasha-lee.jpg"></figure>                          
                                </header>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 item">   
                                <p>Em fa una especial il.lusió com a fundador d’ EL 9 NOU donar la benvinguda al diari digital. Us segueix-ho molt orgullós pel vostre Periodisme<br>
                                <span>#estàspreparat?</span>
                                </p>
                                <header>
                                    <h1>VICENÇ LOZANO</h1>
                                    <h2>periodista de TV3</h2>
                                    <figure><img src="<?php echo get_template_directory_uri() ?>/img/carles-capdevila.jpg"></figure>                          
                                </header>                            
                        </div>                       
                    </section>





                </div>
            </div>
        </div>
    </main>


        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
