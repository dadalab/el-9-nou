<?php
/*
 *	Template name: Escull edició
 */
?>
<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo wp_title() ?></title>
        <meta name="description" content="Periòdic independent d'Osona i el Ripollès i el Vallès Oriental.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="5IDL0iqBCq0ihG6_8prRFwKTX06f8DgDSHucOETPmGg" />
        <script src="https://use.fontawesome.com/f0cbaa2dc7.js"></script>        
		<?php wp_head() ?> 

		<style>
			#loader-home {
				position: absolute;
			    left: 0;
			    right: 0;
			    margin: auto;
			    display: block;
			    width: 70px;
			    top: 50%;
			    margin-top: -35px;
			}
		</style>   

         <?php analytics_code() ?>    
    </head>

<body class="in" >
<div class="overlay-edition in">              
    <div class="left selector-edicio" id="osona-ripolles" style="background-image: url('https://s3-eu-west-1.amazonaws.com/el9nou/statics/osona-ripolles.jpg')">
        <a href="#" class="select-edition">
            <div class="bg-white"></div>
            <div class="cc">
                <img src="<?php echo get_template_directory_uri() ?>/img/el9nou-osona-ripolles.svg">
                <span>Edició Osona i Ripollès</span>
            </div>
        </a>
    </div>
    <div class="center">
    	<h1 id="text-tria-edicio">Tria la teva edició predeterminada</h1><!-- 3  -->
		<div id="loader-home" class="loader loader--style3" style="display:none" title="2">
  			<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="70px" height="70px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
  				<path fill="#fff" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
			    <animateTransform attributeType="xml"
			      attributeName="transform"
			      type="rotate"
			      from="0 25 25"
			      to="360 25 25"
			      dur="0.6s"
			      repeatCount="indefinite"/>
			    </path>
  			</svg>
		</div>
	</div>

    <div class="right selector-edicio" id="valles-oriental" style="background-image: url('https://s3-eu-west-1.amazonaws.com/el9nou/statics/valles-oriental.jpg')">
        <a href="#" class="select-edition">
            <div class="bg-white"></div>
            <div class="cc">
                <img src="<?php echo get_template_directory_uri() ?>/img/el9nou-valles-oriental.svg">
                <span>Edició Vallès Oriental</span>
            </div>
        </a>
    </div>

</div>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/superfish/superfish.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>

<?php wp_footer(); ?>

<script>
$('.selector-edicio').click( function(e) {
	e.preventDefault();
	var id = $(this).attr('id');
	$('#text-tria-edicio').hide();
	$('#loader-home').fadeIn();
  	setTimeout(function(){
        window.location.href = "<?php echo site_url() ?>/" + id + "/?canvi_edicio=" + id;
    },1000); 
  });
	//$.get( "<?php echo site_url() ?>/" + id, function( data ) {
  		//history.pushState(null, null, "<?php echo site_url() ?>/" + id);
  		//$( "html" ).html( data );
	//});
//})
</script>
</body>
</html>