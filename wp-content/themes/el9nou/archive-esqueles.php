<?php
get_header(); ?>
<main id="main" role="main" class="main categoria hemeroteca">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">ESQUELES I DEFUNCIONS</div>
                </div><?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $esqueles = new WP_Query(
                    array(
                        'post_type' => 'esqueles',
                        'posts_per_page' => 9, 
                        'paged' => $paged,
                        'tax_query' => edicio_query_per_llistats(),
                    )
                ); ?>
                
                <!-- noticies actuals -->
                <section id="noticies-recents"> 

                    <div class="row" style="display: none;">
                        <div class="col-md-12" style="margin-bottom: 40px;"><img src="<?php echo get_template_directory_uri(); ?>/img/esqueles.jpg"></div>
                    </div>    
                    <div class="row grid"><?php
                        if ($esqueles->have_posts()) :
                            while ($esqueles->have_posts()) : $esqueles->the_post(); ?>
                                <div class="col-md-6 grid-item">
                                    <div class="esquela-item" style="cursor: pointer;">
                                        <div class="creu" style="<?php if (get_field('mostra_creu')) : ?>background-image: url(<?php echo get_template_directory_uri(); ?>/img/creu.png); background-size: 15px 23px; background-position: center center;<?php endif ?> background-color: #000;"></div>
                                        <?php if (get_field('imatge')) : ?>
                                            <figure class="imatge-esquela"><img src="<?php the_field('imatge') ?>"><?php endif ?></figure>
                                        <?php if (get_field('entradeta_superior')) : ?> 
                                            <h5><?php the_field('entradeta_superior') ?></h5>
                                        <?php endif ?>
                                        <h4><?php the_title() ?></h4>
                                        <h6><?php the_excerpt() ?></h6>
                                        <div class="esquela-content">
                                            <?php the_content() ?>
                                            <div class="gradient"></div>
                                        </div>
                                        <?php if (get_field('data_signatura')) : ?> 
                                            <span class="data"><?php the_field('data_signatura') ?></span>
                                        <?php endif ?>
                                        <a href="#" class="plus-on"><div class="plus"><i class="fa fa-plus" aria-hidden="true"></i></div></a>
                                    </div>
                                </div>
                                <?php $count++;
                            endwhile;
                        endif; ?>
                    </div>
                    <?php if ($esqueles->max_num_pages > 1) : ?>
                        <div class="row">
                            <nav class="prev-next-posts">
                                <div class="prev-posts-link">
                                    <?php echo get_next_posts_link('Esqueles més antigues', $esqueles->max_num_pages); ?>
                                </div>
                                <div class="next-posts-link">
                                    <?php echo get_previous_posts_link('Esqueles més recents'); ?>
                                </div>
                            </nav>
                        </div>
                    <?php endif ?>
                </section>
            
            </div>

            <div class="col-md-3 defuncions">

                <h2>LLISTAT DE DEFUNCIONS</h2>
                <p><?php
                    $page = null;
                    if (edicio_actual() == 'valles-oriental')
                        $page = get_page_by_title( 'Defuncions Vallès Oriental' );
                    else 
                        $page = get_page_by_title( 'Defuncions Osona i Ripollès' );
                    if ( $page )
                        echo nl2br($page->post_content); ?>
                </p>
            </div>
        </div>
    </div>
</main>

<?php get_footer() ?>