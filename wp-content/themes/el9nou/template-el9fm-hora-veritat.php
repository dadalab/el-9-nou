<?php
/**
 *
 *	Template name: L'HORA DE LA VERITAT 9FM
 */

get_header('el9fm') ?>

<?php 
while(have_posts()): the_post() ?>
<main id="main" role="main" class="main">
  <section class="audio-stream">
		<div class="container">	              
			<div class="col-md-12">
				<div class="row">
					<h1><?php the_title() ?></h1>
				</div>
			</div>				
		</div>
	</section>

	<div class="container">
		<div class="row">
			<section class="audio-destacats">
				<div class="col-md-12">
					
					<div class="col-md-8">
						<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/264555053&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
					</div>
					
					<div class="col-md-4">
						<?php the_content() ?>
						<hr class="row">					
					</div>
				</div>
			</section>

        </div>
   </div>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>