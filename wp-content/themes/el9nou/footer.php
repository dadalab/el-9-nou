
<?php /** div del box-content **/ ?>
</div>

<footer class="hidden-print">
 <!-- sitemap -->
    <section class="sitemap">
        <div class="container">
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>">EL9NOU.CAT</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat">Actualitat</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports">Esports</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent">Cultura</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li>SUPLEMENTS</li>
                    <li><a href="<?php url_site('9magazin') ?>">9magazín</a></li>
                    <li><a href="<?php url_site('fotogaleries') ?>">Fotogaleria</a></li>
                    <li><a href="<?php url_site('agenda') ?>">Agenda</a></li>
                    <li><a href="<?php url_site('blogosfera') ?>">Blogosfera</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li>SUBSCRIPCIONS</li>
                    <li><a href="http://club.el9nou.cat/pdf_club.php" target="_blank">Edicio paper</a></li>
                    <li><a href="<?php enllac_club() ?>" target="_blank">Club del subscriptor</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/contacte/subscripcions">Contacte</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li>PUBLICITAT</li>
                    <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9noucat.pdf">Tarifes EL9NOU.CAT</a></li>
                    <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9nou-conjunta-2016-17.pdf">Tarifes EL 9 NOU</a></li>
                    <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9tv-2016-17.pdf">Tarifes EL 9 TV</a></li>
                    <li><a target="_blank" href="https://s3-eu-west-1.amazonaws.com/el9nou/statics/el9fm-2016-17.pdf">Tarifes EL 9 FM</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/contacte/publicitat">Contacte</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li>PREMSA D’OSONA</li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/qui-som">Qui som</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/on-som">On som</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/codi-deontologic">Codi deontològic</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/premis">Premis</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <ul class="footer-list">
                    <li>ALTRES</li>
                    <li><a href="<?php url_site('el9nou') ?>/contacte">Contacte</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/avis-legal">Avís legal</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/politica-de-privacitat">Política de privacitat</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/politica-de-cookies">Política de cookies</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/feed/?post_type=post&edicio=<?php echo edicio_actual() ?>">RSS</a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="group">
        <div class="container">
            <div class="col-md-8 col-sm-8 element">
                <h1>EL GRUP</h1>
                <ul class="logos-corp">
                    <li style="max-width: 125px;"><a href="<?php url_site('el9nou') ?>/osona-ripolles"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/el9nou.cat.png"></a></li>
                    <li style="max-width: 125px;"><a href="<?php url_site('el9nou') ?>/valles-oriental"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/el9nou-valles.cat.png"></a></li>
                    <li style="max-width: 62px;"><a href="<?php url_site('el9tv') ?>"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/el-9tv.png"></a></li>
                    <li style="max-width: 62px;"><a href="<?php url_site('el9fm') ?>"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/el-9fm.png"></a></li>
                    <li style="max-width: 85px;"><a href="<?php url_site('9clics') ?>"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/9clics.png"></a></li>
                    <li style="max-width: 45px;"><a href="<?php url_site('ctl') ?>"><img src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/ctl-overlay.png"></a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4 element">
                <h1>AMB LA COL·LABORACIÓ</h1>
                <figure><img class="gencat" src="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/img/gencat.jpg"></figure>
            </div>
        </div>
    </section>

    <div class="socket">
        <div class="container">
            <div class="col-md-10 col-sm-12 text-left"><span><b>PREMSA D´OSONA, S.A.</b> Plaça de la Catedral, 2. Vic. Tel. 93 889 49 49 | Carrer Girona, 34, Granollers. Tel. 93 860 30 20</span></div>

            <div class="col-md-2 col-sm-12 text-center visible-xs">
                <ul class="politiques">
                    <li><a href="<?php url_site('el9nou') ?>/grup/avis-legal">Avís legal</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/politica-de-privacitat">Política de privacitat</a></li>
                    <li><a href="<?php url_site('el9nou') ?>/grup/politica-de-cookies">Política de cookies</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-sm-12 text-right">
                <div class="social-cont">
                    <ul class="footer-socials"><?php
                        if (edicio_actual() == 'valles-oriental') : ?>
                            <li><a href="https://www.facebook.com/el9nouVOR"><i class="icon-facebook-circled" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/EL9NOU_VOr"><i class="icon-twitter-circled" aria-hidden="true"></i></a></li>
                        <?php else : ?>
                            <li><a href="https://www.facebook.com/el9nou"><i class="icon-facebook-circled" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/el9nou"><i class="icon-twitter-circled" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/el9nou/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCObpBdpXZ2kEqtZh4EQXQrQ" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php get_template_part('templates/menus/search', 'overlay'); ?>

<link rel="stylesheet" id="owl-css-css" href="http://nova.el9nou.cat/wp-content/plugins/addon-so-widgets-bundle/css/owl.carousel.css?ver=4.5.3" type="text/css" media="all">
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/superfish/superfish.js"></script>

<!-- PHOTOSWIPE -->
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/photoswipe/photoswipe.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/photoswipe/photoswipe-ui-default.min.js"></script>

<!-- FULLCALENDAR
<script src='<?php echo get_template_directory_uri() ?>/js/vendor/fullcalendar/moment.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js"></script>
<script src='<?php echo get_template_directory_uri() ?>/js/vendor/fullcalendar/lang-all.js'></script>-->

<script src="<?php echo get_template_directory_uri() ?>/js/vendor/masonry.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/main.js?v=25"></script>
<script type="text/javascript" src="http://el9nou.cat/wp-content/plugins/addon-so-widgets-bundle/js/owl.carousel.min.js?ver=4.5.3"></script>

<script type="text/javascript">
var $grid = $('.grid').masonry({
  itemSelector: '.grid-item',
    resize: true,
    percentPosition: true
});

// PHOTOSWIPE
var initPhotoSwipeFromDOM = function(gallerySelector) {

        // parse slide data (url, title, size ...) from DOM elements
        // (children of gallerySelector)
        var parseThumbnailElements = function(el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                figureEl,
                linkEl,
                size,
                item;

            for(var i = 0; i < numNodes; i++) {

                figureEl = thumbElements[i]; // <figure> element

                // include only element nodes
                if(figureEl.nodeType !== 1) {
                    continue;
                }

                linkEl = figureEl.children[0]; // <a> element

                size = linkEl.getAttribute('data-size').split('x');

                // create slide object
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };



                if(figureEl.children.length > 1) {
                    // <figcaption> content
                    item.title = figureEl.children[1].innerHTML;
                }

                if(linkEl.children.length > 0) {
                    // <img> thumbnail element, retrieving thumbnail url
                    item.msrc = linkEl.children[0].getAttribute('src');
                }

                item.el = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);
            }

            return items;
        };

        // find nearest parent element
        var closest = function closest(el, fn) {
            return el && ( fn(el) ? el : closest(el.parentNode, fn) );
        };

        // triggers when user clicks on thumbnail
        var onThumbnailsClick = function(e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            // find root element of slide
            var clickedListItem = closest(eTarget, function(el) {
                return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
            });

            if(!clickedListItem) {
                return;
            }

            // find index of clicked item by looping through all child nodes
            // alternatively, you may define index via data- attribute
            var clickedGallery = clickedListItem.parentNode,
                childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;

            for (var i = 0; i < numChildNodes; i++) {
                if(childNodes[i].nodeType !== 1) {
                    continue;
                }

                if(childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }



            if(index >= 0) {
                // open PhotoSwipe if valid index found
                openPhotoSwipe( index, clickedGallery );
            }
            return false;
        };

        // parse picture index and gallery index from URL (#&pid=1&gid=2)
        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
            params = {};

            if(hash.length < 5) {
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if(!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');
                if(pair.length < 2) {
                    continue;
                }
                params[pair[0]] = pair[1];
            }

            if(params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;

            items = parseThumbnailElements(galleryElement);

            // define options (if needed)
            options = {

                // define gallery index (for URL)
                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function(index) {
                    // See Options -> getThumbBoundsFn section of documentation for more info
                    var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect();

                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                }

            };

            // PhotoSwipe opened from URL
            if(fromURL) {
                if(options.galleryPIDs) {
                    // parse real index when custom PIDs are used
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for(var j = 0; j < items.length; j++) {
                        if(items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    // in URL indexes start from 1
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if( isNaN(options.index) ) {
                return;
            }

            if(disableAnimation) {
                options.showAnimationDuration = 0;
            }

            // Pass data to PhotoSwipe and initialize it
            gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        };

        // loop through all gallery elements and bind events
        var galleryElements = document.querySelectorAll( gallerySelector );

        for(var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i+1);
            galleryElements[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    };

    // execute above function
    initPhotoSwipeFromDOM('.my-gallery');

</script>

<?php wp_footer(); ?>


<div class="modal fade" tabindex="-1" role="dialog" id="no-permission">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">No teniu suficients permisos</h4>
            </div>
            <div class="modal-body">
                <p>Amb la vostra subscripció actual no teniu prous permisos.<br>
                Per qualsevol dubte podeu contactar amb nosaltres al 93 889 49 49.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>