<?php
/*** PORTADES EL 9 NOU ***/
add_action('my_hourly_event', 'diarisPendents');

function my_activation() {
	if ( !wp_next_scheduled( 'my_hourly_event' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'my_hourly_event');
	}
}
add_action('wp', 'my_activation');




add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/diaris-pendents', array(
        'methods' => 'GET',
        'callback' => 'diarisPendents',
    ) );
} );

add_action( 'importa-diaris', 'diarisPendents' );
add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'hemeroteca/importa-diaris', array(
        'methods' => 'GET',
        'callback' => 'diarisPendents',
    ) );
} );

function diarisPendents() {
    $edicions = ['osona', 'valles'];
    $tipusContinguts = ['diari', 'magazin', 'esportiu'];
	
    foreach($tipusContinguts as $tcontingut)
    {
	    
		foreach($edicions as $edicio)
		{
			$files = scandir('wp-content/themes/el9nou/includes/pdfs');
			$indexed = [];
			$loop = new WP_Query([
				'post_type' => 'hemeroteca',
				'posts_per_page' => -1,
				'tax_query' => array(
		            array(
		                'taxonomy' => 'edicio',
		                'field'    => 'slug',
		                'terms'    => $edicio == 'osona' ? 'osona-ripolles' : 'valles-oriental',
		            )
		        ),
		        
				'meta_query' => array(
		          array(
		            'key' => $tcontingut, // per cada custom field on hi ha d'haver contingut, mirem si estar buit.
		            'value' => '',
		            'compare' => '=='
		            )
		          ),
				
				'date_query' => [
					[
						'after' => '-30 days',
						'column' => 'post_date',
					],
				]
			]);
			
			$array = [];
			while ( $loop->have_posts() ) : $loop->the_post();
			 
		        $array[] = [
		            'id' => get_the_ID(),
		            'title' => get_the_title(),
		            'data' => get_field('data_diari'),
		            'diari' => get_field('diari'),
		            'magazin' => get_field('magazin'),
		            'esportiu' => get_field('esportiu')
		        ];
		
		    endwhile;
		    
		    
		    
		    if ($array)
		    {
			    var_dump('abans');
			    var_dump($files);
				foreach($files as $index => $file) {
					var_dump($file);
					var_dump(strpos($file, $tcontingut) !== false);
					if (strpos($file, $tcontingut) === false) {
						unset($files[$index]);
					}
				}
				var_dump('despres');
				var_dump($files);
				//return;
				var_dump($tcontingut);
				foreach($files as $i => $file)
			    {
			        $file = explode('_', $file);
			        if ($tcontingut == 'diari')
			        {
				        if ($file[2] == 'diari' && $file[1] == $edicio)
				            $indexed[$file[3]] = $file;
				    } else if ($tcontingut == 'magazin')
				    {
					    if ($file[1] == '9magazin' && ( ($edicio == 'osona' && $file[3] == 'OSO.pdf') || ($edicio == 'valles' && $file[3] == 'VAL.pdf') ) )
				            $indexed[$file[2]] = $file;
				    } else if ($tcontingut == 'esportiu')
				    {
					    if ($file[1] == 'esportiu' )
				            $indexed[str_replace('.pdf', '', $file[2])] = $file;
				    }
			    }
			
			    foreach($array as $diari)
			    {
			        if (isset($indexed[$diari['data']]))
			        {			        
			            $postId = $diari['id'];
			            $fitxer = implode('_', $indexed[$diari['data']]);
			            var_dump($fitxer);
			            $filePath = 'wp-content/themes/el9nou/includes/pdfs/' . $fitxer;
			       
						$uploaded = wp_upload_bits($fitxer, null, file_get_contents($filePath));
						
						if ($uploaded['error'] !== false) {
							wp_die('Upload failed');
						}
			
						$attachmentId = wp_insert_attachment([
						    'post_mime_type' => wp_check_filetype(basename($filePath))['type'],
						    'post_title' => $fitxer,
						    'post_content' => '',
						    'post_status' => 'inherit'
						], $uploaded['file']);
			            
			            require_once( ABSPATH . 'wp-admin/includes/image.php' );
						$attachmentData = wp_generate_attachment_metadata( $attachmentId, $uploaded['file'] );
						
						wp_update_attachment_metadata( $attachmentId,  $attachmentData );
			            
			            update_field($tcontingut, $attachmentId, $postId);

			            $filePath = get_template_directory() . '/includes/pdfs/' . $fitxer;
						
						//if ($tcontingut != 'esportiu')
							//unlink($filePath);       
			        }
			    }
			}
		}
	}
    exit();
}



function add_your_meta_box(){

add_meta_box('hemeroteca_enviaportades', 'Enviar portada', 'function_of_metabox', 'hemeroteca', 'side', 'low');}

add_action('add_meta_boxes', 'add_your_meta_box'); 

function function_of_metabox()
{?>
    <input type="button" class="button button-primary button-large" value="Enviar la portada" id="envia-portada"/>
    <img id="loading-enviant" src="/wp-admin/images/spinner-2x.gif" style="max-width: 20px;display: none">  
    <div id="missatge-ok" style="margin-top:10px;display: none;">La campanya s'ha enviat correctament a través de Mailchimp</div>
    
    <script>
		jQuery('#envia-portada').click(function(e) {
			if (confirm('Segur que vols enviar la portada?'))
			{
				jQuery('#loading-enviant').fadeIn();
				console.log('Enviar mailchimp!');
				var data = {
					'action': 'envia_portades',
					'post_id': jQuery("#post_ID").val()
				};

				jQuery.post(ajaxurl, data, function(response) {
					jQuery('#loading-enviant').fadeOut();
					if (response == 'success')
					{
						jQuery('#missatge-ok').fadeIn();
					}
					
				});
			}
		});    
    </script>
<?php }
	


add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/enviamailchimp', array(
        'methods' => 'GET',
        'callback' => 'envia_portades',
    ) );
} );

add_action( 'wp_ajax_envia_portades', 'envia_portades' );
include('mailchimp/Mailchimp.php');
function envia_portades($request) {
		
	$postId = $_POST['post_id'];
	
	if (! $postId) return;
	
	$portada = get_the_post_thumbnail_url($postId);
	
	if (! $portada) return;
	
	$terms = wp_get_post_terms($postId, 'edicio');
	
	$edicio = $terms[0];
	
	$list = '74c8ff7d93';
	$logo = 'http://el9nou.cat/wp-content/themes/el9nou/img/el9nou.cat.png';
	$apiKey = 'd1b130a1d40a666c0343c8e5cad5ba59-us17';
	$templateId = 42893;

	if ($edicio->slug == 'valles-oriental') {
		$logo = 'http://el9nou.cat/wp-content/themes/el9nou/img/el9nou-valles.cat.png';
		$list = '1660fb0851';
		$apiKey = 'fd6259b139537b323bc0a8b9de7c5ad5-us18';
		$templateId = 10361;
	}
	
	
	$mc = new MailChimp($apiKey);
	
	$template_content = array(
        'template' => array( 
            'id' => $templateId,
			'sections'  => array(         
               'imatge_portada' => '<a href="http://el9nou.cat/' . $edicio->slug . '"><img alt="" src="' . $portada . '" width="564" style="border:1px solid #eee;padding:0px;max-width:100%; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage"></a>',
               'imatge_logo' => '<a href="http://el9nou.cat/' . $edicio->slug . '"><img align="center" alt="" src="' . $logo . '" width="170" style="max-width: 200px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage"></a>'
            )
        )
    );
	
	$campaign = $mc->post('campaigns', [
            'type' => 'regular',
            'recipients' => [
            'list_id' => $list,
            ],
            'settings' => [
            	'subject_line' => 'Portada El 9 Nou',
				'from_name' => 'El 9 Nou',
				'reply_to' => 'mailing@el9nou.cat',
				'template_id' => $templateId
            ]
            ]);

	$mc->put('campaigns/' . $campaign['id'] . '/content', 
       	$template_content
    );
	
    $mc->post('campaigns/' . $campaign['id'] . '/actions/send');
	
	echo 'success';
	
	wp_die(); // this is required to terminate immediately and return a proper response
}