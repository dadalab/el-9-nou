<?php

function ads_header_tags() {
    ?>
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];

        googletag.cmd.push(function() {

            var mapTop = googletag.sizeMapping().
                addSize([1024, 100 ], [[1140, 90], [970, 90], [728, 90], [920, 220]]).
                addSize([768, 100 ], [[728, 90], [468, 60],[320,100]]).
                addSize([500, 100 ], [[468, 60], [320,100]]).
                addSize([1, 1 ], [[320, 50], [320,100]]).
                build();

            var skinBanner = googletag.sizeMapping().
                addSize([1520, 100 ], [2200, 1080]).
                addSize([768, 1 ], [2020, 1080]).
                build();

            var mapEntrenoticies = googletag.sizeMapping().
                addSize([1200, 100 ], [[728, 90], [468, 60]]).
                addSize([768, 100], [[468, 60], [300, 250], [300, 300], [320, 50], [250, 250]]).
                addSize([500, 500], [[234, 60], [468, 60], [300, 250], [300, 300], [320, 50], [250, 250]]).
                addSize([350, 1], [[234, 60], [320, 50], [250, 250], [300, 300], [300, 250]]).
                addSize([1, 1], [[234, 60], [250, 250]]).
                build();

            var sideNew = googletag.sizeMapping().
                addSize([1200, 100 ], [[300, 300], [300,600], [300, 250], [250, 250]]).
                addSize([768, 100 ], [[250, 250], [234, 60]]).
                addSize([500, 100 ], [250, 250]).
                addSize([1, 1 ], [320, 50]).
                build();

            <?php if (edicio_actual() == 'valles-oriental') : ?>
                googletag.defineSlot('/119069366/portada-valles-oriental-top', [[320, 50], [468, 60], [970, 90], [728, 90], [1140, 90]], 'div-gpt-ad-1479713888696-11').defineSizeMapping(mapTop).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                //if (jQuery( document ).width() > 990) { // Només si es veu la barra lateral
                    googletag.defineSlot('/119069366/entrenoticies-portada-valles-oriental-1', [[250, 250], [320, 50], [468, 60], [728, 90]], 'div-gpt-ad-1479713888696-6').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-1-valles-oriental', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-7').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-2-valles-oriental', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-8').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-3-valles-oriental', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-9').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-4-valles-oriental', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-10').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
					googletag.defineSlot('/119069366/lateral-5-valles-oriental', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-1').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
					googletag.defineSlot('/119069366/lateral-6-valles-oriental', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-3').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
					googletag.defineSlot('/119069366/lateral-7-valles-oriental', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-5').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                //}
                <?php if ( nou_home() ) { // Només si som a la home ?>
                    googletag.defineSlot('/119069366/entrenoticies-1-valles-oriental', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1480318459286-1').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/entrenoticies-2-valles-oriental', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1480318459286-3').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/entrenoticies-3-valles-oriental', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1480318459286-5').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/entrenoticies-4-valles-oriental', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1489998504832-2').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/entrenoticies-5-valles-oriental', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1489998504832-0').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                <?php } else { // Només si no som a la home (noticia interior) ?>
                    googletag.defineSlot('/119069366/interior-noticia-valles-oriental', [[234, 60], [300, 300], [250, 250], [300, 250]], 'div-gpt-ad-1491557944598-1').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                <?php } ?>
            <?php else : ?>
                //googletag.defineSlot('/119069366/skinbàner', [2200, 1080], 'div-gpt-ad-1508740757417-0').addService(googletag.pubads());

               // googletag.defineSlot('/119069366/portada-osona-ripolles-top', [[320, 50], [468, 60], [970, 90], [728, 90], [1140, 90]], 'div-gpt-ad-1479713888696-4').defineSizeMapping(mapTop).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                //if (jQuery( document ).width() > 990) {

            	googletag.defineSlot('/119069366/skinbàner', [[2200, 1080], [2020, 1080]], 'div-gpt-ad-1508740757417-0').defineSizeMapping(skinBanner).addService(googletag.pubads());
                googletag.defineSlot('/119069366/portada-osona-ripolles-top', [[320, 50], [320,100], [468, 60], [970, 90], [728, 90], [920, 220], [1140, 90]], 'div-gpt-ad-1479713888696-4').defineSizeMapping(mapTop).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
               // if (jQuery( document ).width() > 990) {
                    googletag.defineSlot('/119069366/lateral-1-osona-ripolles', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-0').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-2-osona-ripolles', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-1').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-3-osona-ripolles', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-2').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                    googletag.defineSlot('/119069366/lateral-4-osona-ripolles', [[234, 60], [300, 300], [300, 600], [300, 250], [250, 250]], 'div-gpt-ad-1479713888696-3').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
              
                     googletag.defineSlot('/119069366/lateral-5-osona-ripolles', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-0').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
					 googletag.defineSlot('/119069366/lateral-6-osona-ripolles', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-2').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
					 googletag.defineSlot('/119069366/lateral-7-osona-ripolles', [[300, 300], [300, 250], [300, 600], [250, 250]], 'div-gpt-ad-1525708451885-4').addService(googletag.pubads()).setCollapseEmptyDiv(true,true);

                //}
                <?php //if ( nou_home() ) { // Només si som a la home ?>
                    googletag.defineSlot('/119069366/entrenoticies-1-osona-ripolles', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1480318459286-0').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);;
                    googletag.defineSlot('/119069366/entrenoticies-2-osona-ripolles', [[234, 60], [300, 250], [728, 90], [300, 300], [320, 50], [468, 60], [250, 250]], 'div-gpt-ad-1480318459286-2').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);;
                    googletag.defineSlot('/119069366/entrenoticies-3-osona-ripolles', [[234, 60], [300, 250], [728, 90], [300, 300], [468, 60], [250, 250], [320, 480]], 'div-gpt-ad-1480318459286-4').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);;
                    googletag.defineSlot('/119069366/entrenoticies-4-osona-ripolles', [[234, 60], [300, 250], [728, 90], [300, 300], [468, 60], [250, 250], [320, 480]], 'div-gpt-ad-1489998504832-3').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);;
                    googletag.defineSlot('/119069366/entrenoticies-5-osona-ripolles', [[234, 60], [300, 250], [728, 90], [300, 300], [468, 60], [250, 250], [320, 480]], 'div-gpt-ad-1489998504832-1').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);;
                <?php //} else { ?>
                    googletag.defineSlot('/119069366/dins-noticia-osona-ripolles', [[234, 60], [300, 300], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1491557944598-0').defineSizeMapping(sideNew).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
                <?php //} ?>
            <?php endif ?>

            <?php if ( nou_home() ) { // Només si som a la home ?>
            	googletag.defineSlot('/119069366/entre-noticies-compartit-1', [[300, 250], [300, 300], [468, 60], [320, 480], [728, 90], [250, 250]], 'div-gpt-ad-1490001639964-0').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
				googletag.defineSlot('/119069366/entre-noticies-compartit-2', [[300, 250], [468, 60], [320, 480], [728, 90], [300, 600], [250, 250]], 'div-gpt-ad-1490001639964-1').defineSizeMapping(mapEntrenoticies).addService(googletag.pubads()).setCollapseEmptyDiv(true,true);
            <?php } ?>

            //googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
</script><?php
}


function ads_mostra_anunci($idOsona, $idValles) {
    if (edicio_actual() == 'valles-oriental') : ?>
        <div id='div-gpt-ad-<?php echo $idValles ?>' class="anunci">
            <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $idValles ?>'); });</script>
        </div><?php
     else : ?>
         <div id='div-gpt-ad-<?php echo $idOsona ?>' class="anunci">
            <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $idOsona ?>'); });</script>
        </div><?php
    endif;
}

function ads_the_top_banner() { ?>
    <div class="top-banner"><?php
        ads_mostra_anunci('1479713888696-4', '1479713888696-11'); ?>
    </div><?php
}

function ads_lateral_1() { ?>
    <div class="anunci" style="background: #e9e9ea;text-align: center;padding: 20px 0;"><?php
        echo ads_mostra_anunci('1479713888696-0', '1479713888696-7'); ?>
    </div><?php
}

function ads_lateral_2() { ?>
    <div class="anunci" style="background: #e9e9ea;text-align: center;padding: 20px 0;"><?php
        ads_mostra_anunci('1479713888696-1', '1479713888696-8'); ?>
    </div><?php
}

function ads_lateral_3() { ?>
    <div class="anunci" style="background: #e9e9ea;text-align: center;padding: 20px 0;"><?php
        ads_mostra_anunci('1479713888696-2', '1479713888696-9'); ?>
    </div><?php
}

function ads_lateral_4() { ?>
    <div class="anunci" style="background: #e9e9ea;text-align: center;padding: 20px 0;"><?php
        ads_mostra_anunci('1479713888696-3', '1479713888696-10'); ?>
    </div><?php
}

function ads_before_tags() { ?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

	<ins class="adsbygoogle hidden-md hidden-lg hidden-print"
	     style="display:block; height: 300px;margin: 20px auto"
	     data-ad-client="ca-pub-9944811849436984"
	     data-ad-slot="6783380954"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script><?php
}

function ads_get_entrenoticies($pi) {
    anuncis_portada($pi);
    banners_propis($pi);
}

function opinio() {
    get_template_part('templates/opinio');

    //include('templates/opinio.php');
}

function anuncis_portada($pi) {
    if ($pi > 1 && $pi % 3 == 0 && $pi < 22) : ?><div class="anunci-entrenoticies"><?php endif;
    if ($pi == 3) ads_mostra_anunci('1480318459286-0', '1480318459286-1'); // Entrenoticies 1
    if ($pi == 6) ads_mostra_anunci('1480318459286-2', '1480318459286-3'); // Entrenoticies 2
    if ($pi == 9) ads_mostra_anunci('1490001639964-0', '1490001639964-0'); // Comú edicions 1
    if ($pi == 12) ads_mostra_anunci('1480318459286-4', '1480318459286-5'); // Entrenoticies 3
    if ($pi == 15) ads_mostra_anunci('1489998504832-3', '1489998504832-2'); // Entrenoticies 4
    if ($pi == 18) ads_mostra_anunci('1490001639964-1', '1490001639964-1'); // Comú edicions 2
    if ($pi == 21) ads_mostra_anunci('1489998504832-1', '1489998504832-0'); // Entrenoticies 5
    if ($pi > 1 && $pi % 3 == 0 && $pi < 22) : ?></div><?php endif;
}

function banners_propis($pi) {
    $contents = [
        '<div class="externalcontent"><external-content title="EL 9 TV" api-url="http://el9nou.cat/wp-json/api/el9tv/ultims" video="1"></external-content></div>',
        '<div id="externalMagazin"><external-content title="9Magazin" api-url="http://9magazin.el9nou.cat/wp-json/api/osona-ripolles/ultimes?count=3" video="0"></external-content></div>',
        '<div id="externalFotogaleries"><external-content title="Fotogaleries" api-url="http://fotogaleries.el9nou.cat/api/' . edicio_actual() . '/ultimes?count=3" video="0"></external-content></div>'
    ];
    if ($pi == 3) opinio();
    //shuffle($contents);
    if ($pi == 3) {
        $num = date('d') % 3;
        if ( ! ($num == 0 && edicio_actual() == 'valles-oriental') )
            echo $contents[$num];
    }

    if ($pi == 6) {
        $num = (date('d') + 1) % 3;
        if ( ! ($num == 0 && edicio_actual() == 'valles-oriental') )
            echo $contents[$num];
    }

    if ($pi == 9) {
        $num = (date('d') + 2) % 3;
        if ( ! ($num == 0 && edicio_actual() == 'valles-oriental') )
            echo $contents[$num];
    }

    if ($pi == 12) { ?>
    	<div class="anunci" style="overflow: hidden;padding: 20px;padding-bottom: 0;">
	    	<div class="row">
			    <div class="col-xs-6" style="padding-bottom: 20px">
				    <a href="/el-temps"><img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/05/eltemps.jpg"></a>
			    </div>
			    <div class="col-xs-6" style="padding-bottom: 20px">
				    <a href="/<?php echo edicio_actual() ?>/defuncions"><img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/05/defuncions.jpg"></a>
			    </div>
	    	</div>
    	</div>
	    <?php
	}
}

function ads_interestitial() {


    ?>
    <script>

        $(document).ready(function() {
            var interestitial_available = 0;
            var edicio_actual = "<?php echo edicio_actual() ?>";
            console.log(edicio_actual);

            if (navigator.cookieEnabled)
            {
                if (document.cookie.search('interestitial_seen') < 0 && interestitial_available && (edicio_actual == 'osona-ripolles' || edicio_actual == 'valles-oriental')) // interestitial not seen
                {
                    var now = new Date();
                    now.setTime(now.getTime() + 12 * 3600 * 1000);
                   document.cookie = "interestitial_seen=1; expires=" + now.toUTCString() + "; path=/";
                    showInterestitial(edicio_actual);
                }
                else {
                    console.log('interestitial seen');
                }
            }

            function showInterestitial(edicio)
            {
	            console.log(edicio);
                googletag.cmd.push(function() {
                var mapping = googletag.sizeMapping().
                    addSize([900, 690], [800, 600]).
                    addSize([640, 480], [600, 450]).
                    addSize([0, 0], [320, 240]).
                    build();

					<?php if (edicio_actual() == 'osona-ripolles') : ?>
	                    googletag.defineSlot('/119069366/interestitial-osona', [[800, 600], [600, 450], [320, 240]], 'div-gpt-ad-1495022258756-0').defineSizeMapping(mapping).addService(googletag.pubads());
					<?php else : ?>
						googletag.defineSlot('/119069366/interestitial-valles', [[800, 600], [600, 450], [320, 240]], 'div-gpt-ad-1499062769606-0').defineSizeMapping(mapping).addService(googletag.pubads());

                    <?php endif ?>
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                });

                //$(document).ready( function() {
                    setTimeout((function() {
                        $('#interestitial').fadeIn();
                    }), 2000);

                    $('#interestitial').click( function() {
                        $(this).fadeOut();
                    })

               // })
            }
        });

    </script>

    <!-- /119069366/interestitial-osona -->
    <div id="interestitial"><?php
	    if (edicio_actual() == 'osona-ripolles') :
			$tag = 'div-gpt-ad-1495022258756-0';
		else :
		    $tag = 'div-gpt-ad-1499062769606-0';
		endif ?>

        <div id='<?php echo $tag ?>' class="anunci-interestitial">
            <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>

            <script>
                $(document).ready( function() {
                    googletag.cmd.push(function() { googletag.display("<?php echo $tag ?>"); });
                })
            </script>
        </div>
    </div>

    <?php
}

add_action('wp_footer','ads_interestitial');



//Insert ads after second paragraph of single post content.

add_filter( 'the_content', 'prefix_insert_post_ads' );


function prefix_insert_post_ads( $content ) {

    $ad_code = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle visible-xs"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-9944811849436984"
     data-ad-slot="2048042136"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>';

    if ( is_single() && ! is_admin() ) {
        return prefix_insert_after_paragraph( $ad_code, 2, $content );
    }

    return $content;
}

// Parent Function that makes the magic happen

function prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {

    if (get_field('es_premium'))
        return $content;

    $closing_p = '</p>';
    $paragraphs = explode( $closing_p, $content );
    foreach ($paragraphs as $index => $paragraph) {

        if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
        }

        if ( $paragraph_id == $index + 1 ) {
            $paragraphs[$index] .= $insertion;
        }
    }

    return implode( '', $paragraphs );
}