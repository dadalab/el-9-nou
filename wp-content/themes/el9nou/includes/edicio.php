<?php
// Totes les funcionalitats per gestionar les dues edicions del diari.

/**
 * Redireccionem l'usuari a la seva edició preferida, en cas contrari
 * li surt l'espai per seleccionar quina prefereix.
 **/
function edicio_redireccio_home() {
    $url = parse_url(site_url());
    if ($_SERVER['REQUEST_URI'] == $url['path'] . '/')
    {
        $cookie_name = "edicio";
        if (isset($_COOKIE[$cookie_name]))
        {
            wp_redirect ( site_url() . '/' . $_COOKIE[$cookie_name]);
            die();
        }
    }
}
add_action( 'init', 'edicio_redireccio_home');

/**
 *  Afegim la classe corresponent al body per les dues edicions
 **/
add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( edicio_actual() ) );
} );

/**
 * Establim l'edició que l'usuari prefereix
 **/
function estableix_edicio($edicio) {
    /** Agafem les edicions del diari creades i n'extraiem la URL **/
    $edicions_slug = array_map(function($val){
        return $val->slug;
    }, get_terms('edicio'));

    /** Comprovem que l'edicio que es vol establir existeix al sistema **/
    if (in_array( $edicio, $edicions_slug) ) :
        $path = parse_url(get_option('siteurl'), PHP_URL_PATH);
        $host = parse_url(get_option('siteurl'), PHP_URL_HOST);
        setcookie('edicio', $edicio, time() + ( 365 * DAY_IN_SECONDS ), '/');
    endif;
}

/**
 * L'usuari vol canviar la seva edició preferida
 **/
function comprova_edicio() {
     if (isset($_GET['canvi_edicio']))
        estableix_edicio($_GET['canvi_edicio']);
}
add_action( 'init', 'comprova_edicio' );

/**
 *  Determina l'edicio actual en funció del paràmetre de la URL.
 **/
function edicio_actual() {

	if (isset($_GET['edicio'])) return $_GET['edicio'];

    if (is_single() && has_term( 'osona-ripolles', 'edicio') && has_term('valles-oriental', 'edicio') ) // si és una notícia de les dues edicions.
        return isset($_COOKIE['edicio']) ? $_COOKIE['edicio'] : 'osona-ripolles';
    if (strpos($_SERVER["REQUEST_URI"], '/valles-oriental/') !== false)
        return 'valles-oriental';
    else if (strpos($_SERVER["REQUEST_URI"], '/osona-ripolles/') !== false)
        return 'osona-ripolles';
    else if (isset($_COOKIE['edicio']))
        return $_COOKIE['edicio'];

    return 'osona-ripolles';
}

function edicio_get() {
    $name = isset($_COOKIE['edicio']) ? $_COOKIE['edicio'] : 'osona-ripolles';
    return get_term_by('slug', $name, 'edicio');
}

function edicio_get_slug() {
    return edicio_get()->slug;
}
function edicio_get_nom() {
    return edicio_get()->name;
}

function edicio_obte_link_de_canvi() {
    if (edicio_actual() == 'valles-oriental')
        return '<a href="' . site_url() . '/osona-ripolles/?canvi_edicio=osona-ripolles">Edició Osona Ripollès</a>';
    return '<a href="' . site_url() . '/valles-oriental/?canvi_edicio=valles-oriental">Edició Vallès Oriental</a>';
}

/**
 * Redireccions segons edició.
 **/
function custom_rewrite_tag() {
    add_rewrite_rule('osona-ripolles/hemeroteca/page/([0-9]{1,})/?$', 'index.php?post_type=hemeroteca&paged=$matches[1]', 'top');
    add_rewrite_rule('valles-oriental/hemeroteca/page/([0-9]{1,})/?$', 'index.php?post_type=hemeroteca&paged=$matches[1]', 'top');
    add_rewrite_rule('((osona-ripolles|valles-oriental)/hemeroteca)', 'index.php?post_type=hemeroteca', 'top');
    add_rewrite_rule('osona-ripolles/defuncions/page/([0-9]{1,})/?$', 'index.php?post_type=esqueles&paged=$matches[1]', 'top');
    add_rewrite_rule('valles-oriental/defuncions/page/([0-9]{1,})/?$', 'index.php?post_type=esqueles&paged=$matches[1]', 'top');
    add_rewrite_rule('((osona-ripolles|valles-oriental)/defuncions)', 'index.php?post_type=esqueles', 'top');
    //add_rewrite_rule('(osona-ripolles\Z)', 'index.php?post_type=portades_osona', 'top');
    //add_rewrite_rule('(valles-oriental\Z)', 'index.php?post_type=portades_valles', 'top');
    add_rewrite_rule('(osona-ripolles\Z)', 'index.php?pagename=portada-osona-ripolles', 'top');
    add_rewrite_rule('(valles-oriental\Z)', 'index.php?pagename=portada-valles-oriental', 'top');
    add_rewrite_tag('%custom_edicio%', '(osona-ripolles|valles-oriental)', 'custom_edicio=');

    //add_rewrite_rule('(noticies_)', 'redireccions.php', 'top');
    // http://www.el9nou.cat/noticies_o_0/48901/govern-torello-compromet-pressionar-sagales-per-recuperar-parada
}
add_action('init', 'custom_rewrite_tag', 10, 0);

/**
 *  Afegeix el prefixe de l'edició en funció d'on pertany la notícia.
 **/
function afegeix_edicio_a_url( $url, $post ) {
    $url = str_replace(site_url(), '', $url);
    if (get_field('forçar_edicio_vallès', $post->ID))
    {
	   return site_url() . '/valles-oriental' . $url; 
    }
    
    $terms = get_the_terms($post->ID, 'edicio');
    	
    return site_url() . '/' . $terms[0]->slug . $url;
}
add_filter( 'post_link', 'afegeix_edicio_a_url', 1, 3 );

function hemeroteca_prefix( $url, $post ) {
    if ( 'hemeroteca' == get_post_type( $post ) ) {
        $terms = get_the_terms($post->ID, 'edicio');
        $url = str_replace(site_url(), '', $url);

        return site_url() . '/' . $terms[0]->slug . $url;
    }
    return $url;
}
add_filter( 'post_type_link', 'hemeroteca_prefix', 10, 2 );

/**
 *  Prepara els enllaços als arxius en funció de l'edició on et trobes actualment.
 **/
function term_link_filter( $url, $term, $taxonomy ) {
    $url = str_replace('%custom_edicio%', edicio_actual(), $url);

    return $url;
}
add_filter('term_link', 'term_link_filter', 1, 3);

/**
 *  Funció d'ajuda per tal d'obtenir les notícies d'una determinada edició.
 **/
function edicio_query_per_llistats() {
    return
        array(
            array(
                'taxonomy' => 'edicio',
                'field'    => 'slug',
                'terms'    => edicio_actual(),
            )
        );
}

/**
 * Retorna el logo en funció de l'edició on ens trobem
 **/
function edicio_the_logo() {
    echo (edicio_actual() == 'osona-ripolles')
        ? '<img src="' . get_template_directory_uri() . '/img/el9nou.cat.svg">'
        : '<img src="' . get_template_directory_uri() . '/img/el9nou-valles.cat.svg">';
}