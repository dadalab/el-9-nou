<?php 

function the_news_sharing()
{
    echo '<a href="#" class="share-button" data-toggle="tooltip" data-url="' . get_the_permalink() . '" data-placement="left" title="Comparteix"><i class="icon-share"></i></a>';
}

function shares_metatags() {
	if (is_single()) : ?>
		<!-- Open Graph Data -->
		<meta property="og:type" content="article"/>  
		<?php if (has_post_thumbnail()) : ?><meta property="og:image" content="<?php the_post_thumbnail_url() ?>"/>  <?php endif ?>
		<meta property="og:title" content="<?php the_title() ?>"/>  
		<meta property="og:description" content="<?php echo get_the_excerpt() ?>"/>
		<meta property="og:image:width" content="640" /> 
		<meta property="og:image:height" content="442" />

		<!-- Twitter Card -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="<?php the_title() ?>">
		<meta name="twitter:description" content="<?php echo get_the_excerpt() ?>">
		<?php if (has_post_thumbnail()) : ?><meta name="twitter:image" content="<?php the_post_thumbnail_url() ?>">  <?php endif ?>
		<?php 
	endif;
}
add_action('wp_head', 'shares_metatags');

function shares_scripts() { 
	get_template_part('templates/menus/share', 'overlay') ?>
	<script>
		$(document).ready( function() {
    		$('.share-url').click( function() {
        		prompt('Copia l\'adreça per compartir', "<?php the_permalink() ?>");
    		});

    		$('.single-share-url').click( function(e) {
    			e.preventDefault();
        		prompt('Copia l\'adreça per compartir', $(this).attr('data-url'));
    		});

    		$('.share-button').click(function() {
    			$('#url_to_share').val( $(this).attr('data-url') );
    		});

    		$('.copy-link-overlay').click(function(e) {
    			prompt('Copia l\'adreça per compartir', $('#url_to_share').val());
    		});
    		$('.share-now-button').click(function(e) {
    			window.open( $(this).attr('data-share-url') + $('#url_to_share').val());
    		});


		})
	</script> <?php
}
add_action('wp_footer', 'shares_scripts');