<?php
function author_contact_methods( $contactmethods ) {
	$contactmethods['twitter'] = 'Twitter';
	$contactmethods['facebook'] = 'Facebook';
	$contactmethods['instagram'] = 'Instagram';
 
	return $contactmethods;
}
add_filter('user_contactmethods', 'author_contact_methods', 10, 1);