<?php

/*
|--------------------------------------------------------------------------
| EL 9 TV
|--------------------------------------------------------------------------
|
*/
add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9tv/ultims', array(
        'methods' => 'GET',
        'callback' => 'ultimsVideos',
    ) );
} );


function ultimsVideos(WP_REST_Request $request) {
    $total = $request->get_param( 'count' ) ?: 3;

    $posts = wp_get_recent_posts([
        'numberposts' => $total,
        'post_status' => 'publish',
        'post_type' => 'video'
    ]);

    $output = [];
    foreach($posts as $post)
    {
        $object = [
            'nom' => $post['post_title'],
            'url' => get_permalink($post['ID']),
            'urlDestacada' => get_the_post_thumbnail_url($post['ID'])
        ];

        array_push($output, $object);

    }

    return $output;
}

/** Generate URL for newspaper **/
add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'hemeroteca/generar-link', array(
        'methods' => 'GET',
        'callback' => 'generateUrl',
    ) );
} );

function generateUrl(WP_REST_Request $request) {
    $result = wp_remote_get( 'http://9club.el9nou.cat/api/usuari/permisos?api_token=' . $request->get_param( 'api_token' ) . '&file=' . $request->get_param('fitxer') );

    $user = json_decode( wp_remote_retrieve_body( $result ) );
	
    if (! $user->allowed)
    {
        return [
            'status' => 'error',
            'message' => 'error.permissions',
        ];
        exit();
    }

    global $as3cf;

    $id = $request->get_param( 'id' );

    return [
        'status' => 'success',
        'url' => $as3cf->get_secure_attachment_url( $id, 60),
    ];
    exit();
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9tv/data', array(
        'methods' => 'GET',
        'callback' => 'perDia',
    ) );
} );


function perDia(WP_REST_Request $request) {
    $dia = $request->get_param( 'dia' ) ?: 3;

	$day = strtotime($dia);

	$args = [
	    'posts_per_page'  => 10,
	    'post_type'       => 'video',
	    'post_status'     => 'publish',
	    'date_query' => [
	        'year' => date('Y', $day) ,
	        'month' => date('m', $day),
	        'day' => date('d', $day)
		],
	];
	$posts = get_posts($args);

    $output = [];

    foreach($posts as $post)
    {
        $object = [
            'nom' => $post->post_title,
            'url' => get_permalink($post->ID),
            'urlDestacada' => get_the_post_thumbnail_url($post->ID),
        ];

        array_push($output, $object);

    }

    return $output;
}


/*
|--------------------------------------------------------------------------
| EL 9 NOU
|--------------------------------------------------------------------------
|
*/

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/data', array(
        'methods' => 'GET',
        'callback' => 'el9nou_perDia',
    ) );
} );


function el9nou_perDia(WP_REST_Request $request) {
    $dia = $request->get_param( 'dia' ) ?: 3;
    $edicio = $request->get_param('edicio');

	$day = strtotime($dia);

	$args = [
	    'posts_per_page'  => 15,
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'date_query' => [
	        'year' => date('Y', $day) ,
	        'month' => date('m', $day),
	        'day' => date('d', $day)
		],
		'tax_query' => edicio_query_per_llistats()
	];
	$posts = get_posts($args);

    $output = [];

    foreach($posts as $post)
    {
	    $categories = get_the_category($post->ID);
		$categoria = $categories[0]->name;
		foreach($categories as $c) {
			if ($c->category_parent == 0) $categoria = $c->name;
		}
		
		$destacada = get_the_post_thumbnail_url($post->ID);

		if (! $destacada && has_category( 'opinio', $post ) ) :
			$authors = get_coauthors($post->ID);
			$destacada = get_author_image_url($authors[0]->ID);
		endif;
		
        $object = [
            'nom' => $post->post_title,
            'url' => get_permalink($post->ID),
            'urlDestacada' => $destacada,
            'categoria' => $categoria,
			'es_premium' => get_field('es_premium', $post->ID)
        ];

        array_push($output, $object);

    }

    return $output;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/categories', array(
        'methods' => 'GET',
        'callback' => 'el9nou_categories',
    ) );
} );

function el9nou_categories() {
	
	$categories = [];
	$categories = [
					array(cat_ID => 0, name => "Totes"),
    				array(cat_ID => 11, name => "Actualitat"),
    				array(cat_ID => 14, name => "Esports"),
    				array(cat_ID => 13, name => "Cultura")
				  ];
	
	return $categories;

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/ultimes', array(
        'methods' => 'GET',
        'callback' => 'el9nou_ultimes',
    ) );
} );


function el9nou_ultimes(WP_REST_Request $request) {

    $edicio = $request->get_param('edicio');
    $page = $request->get_param('page');

	$args = [
	    'posts_per_page'  => 15,
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'paged'			  => $page,
		'tax_query' 	  => edicio_query_per_llistats()
	];
	$posts = get_posts($args);

    $output = [];

    foreach($posts as $post)
    {
    	$destacada = get_the_post_thumbnail_url($post->ID);

		if (! $destacada && has_category( 'opinio', $post ) ) :
			$authors = get_coauthors($post->ID);
			$destacada = get_author_image_url($authors[0]->ID);
		endif;

    
        $object = [
	        'ID' => $post->ID,
			'post_title' => $post->post_title,
			'permalink' => get_permalink($post->ID),
			'thumbnail' => $destacada, //get_the_post_thumbnail_url($post->ID, '9nou-medium'),
			'excerpt' => get_the_excerpt($post->ID),
			'icon' => post_category_for_icon_with_id($post->ID),
            'data' => get_the_date('d-m-Y', $post->ID),
            'content' => $post->post_content,
            'es_premium' => get_field('es_premium', $post->ID)
            //'coments' => get_comments(array('post_id' => $post->ID))
        ];


        array_push($output, $object);

    }

    return $output;

    //return $output;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/ultimes_category', array(
        'methods' => 'GET',
        'callback' => 'el9nou_ultimes_cat',
    ) );
} );


function el9nou_ultimes_cat(WP_REST_Request $request) {

    $edicio = $request->get_param('edicio');
    $page = $request->get_param('page');
    $cat_id = $request->get_param('cat');

	$args = [
	    'posts_per_page'  => 15,
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'paged'			  => $page,
	    'category' 		  => $cat_id,
		'tax_query' 	  => edicio_query_per_llistats()
	];
	$posts = get_posts($args);

	$output = [];    

    foreach($posts as $index => $post)
    {
    	$destacada = get_the_post_thumbnail_url($post->ID);

		if (! $destacada && has_category( 'opinio', $post ) ) :
			$authors = get_coauthors($post->ID);
			$destacada = get_author_image_url($authors[0]->ID);
		endif;
		
	    $categoria = [];
	    $categories = get_the_category($post->ID);
	    $categoria = $categories[0]->name;
    	foreach ($categories as $cat) {
	        if ($cat->category_parent != 0) {
	            $categoria = $cat->name;
	        }
		}

        $object = [
	        'type' => 'noticia',
	        'ID' => $post->ID,
	        'cat' => $categoria,
			'post_title' => $post->post_title,
			'permalink' => get_permalink($post->ID),
			'thumbnail' => $destacada, //get_the_post_thumbnail_url($post->ID, '9nou-medium'),
			'excerpt' => get_the_excerpt($post->ID),
            'data' => get_the_date('d-m-Y', $post->ID),
            'content' => apply_filters( 'the_content', $post->post_content),
            'es_premium' => get_field('es_premium', $post->ID)
        ];

        array_push($output, $object);
        
        if ($banners = bannersAppEnPosicio($index)) :
        	foreach($banners as $banner):
        		array_push($output, $banner);
			endforeach;
        endif;

    }

    return $output;
}

function bannersAppEnPosicio($posicio)
{
	$a = [];
	$a[4] = [
		[
		    'type' => 'html',
		    'link' => 'http://el9nou.cat/el-temps',
		    'html' => '
	            <div><img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/05/eltemps.jpg"></div>
			'
		]
	];
	$a[8] = [
		[
		    'type' => 'html',
		    'link' => 'http://el9nou.cat/' . $edicio . '/defuncions',
		    'html' => '
	            <div><img src="http://s3-eu-west-1.amazonaws.com/el9nou/images/2017/05/defuncions.jpg"></div>
			'
		]
	];
	
	if (isset($a[$posicio + 1]))
		return $a[$posicio + 1];
	
	
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/post', array(
        'methods' => 'GET',
        'callback' => 'el9nou_post',
    ) );
} );


function el9nou_post(WP_REST_Request $request) {

    $post = $request->get_param( 'id' );

	
	$post = get_post($post);

    $output = [
        'ID' => $post->ID,
		'post_title' => $post->post_title,
		'permalink' => get_permalink($post->ID),
		'thumbnail' => get_the_post_thumbnail_url($post->ID, '9nou-medium'),
		'excerpt' => get_the_excerpt($post->ID),
        'data' => get_the_date('d-m-Y', $post->ID),
        'content' => apply_filters( 'the_content', $post->post_content),
    ];
    

    return $output;

}


/*
	VOTACIONS EL PERIODICO
*/

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/consulta', array(
        'methods' => 'POST',
        'callback' => 'consultaDNI',
    ) );
} );


function consultaDNI(WP_REST_Request $request) {
    $dni = $request->get_param( 'dni' );

    if (! $dni)
	{
		return [
			'status' => 'error',
			'message' => 'Heu d\'introduir un DNI'
		];
	}


	$mydb = new wpdb('el9nou','nounou99','prosa','el9nou.caoxwb4eckof.eu-west-1.rds.amazonaws.com');

	$rows = $mydb->get_results("select * from sgi_votacions_consulta where dni = '$dni' LIMIT 1");

	if ($rows)
	{
		return [
			'status' => 'error',
			'message' => 'Ja s\'ha votat amb aquest número de DNI'
		];
	}

	$rows = $mydb->get_results("select * from sgi_vigents where vigents_nif = '$dni' LIMIT 1 ");
	if (! $rows)
	{
		return [
			'status' => 'error',
			'message' => 'No hem trobat cap subscripció amb aquest DNI. Per qualsevol problema podeu contactar amb nosaltres.'
		];
	}

	return [
		'status' => 'success',
		'nom' => $rows[0]->vigents_nom ?: $rows[0]->vigents_cognom,
	];


}

/*
	VOTACIONS EL PERIODICO
*/

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/consulta/resposta', array(
        'methods' => 'POST',
        'callback' => 'votacioConsulta',
    ) );
} );


function votacioConsulta(WP_REST_Request $request) {

    $dni = $request->get_param( 'dni' );

    if (! $request->get_param( 'dni' ) || ! $request->get_param( 'email' ) || ! $request->get_param( 'opcio' ) )
	{
		return [
			'status' => 'error',
			'message' => 'Tots els camps són obligatoris.'
		];
	}


	$mydb = new wpdb('el9nou','nounou99','prosa','el9nou.caoxwb4eckof.eu-west-1.rds.amazonaws.com');

	$rows = $mydb->get_results("select * from sgi_votacions_consulta where dni = '$dni' LIMIT 1");

	if ($rows)
	{
		return [
			'status' => 'error',
			'message' => 'Ja s\'ha votat amb aquest número de DNI'
		];
	}

	$rows = $mydb->get_results("select * from sgi_vigents where vigents_nif = '$dni' LIMIT 1 ");
	if (! $rows)
	{
		return [
			'status' => 'error',
			'message' => 'No hem trobat cap subscripció amb aquest DNI. Per qualsevol problema pots contactar amb nosaltres.'
		];
	}


	$mydb->insert(
	'sgi_votacions_consulta',
	array(
		'dni' => $dni,
		'email' => $request->get_param( 'email' ),
		'resposta' => $request->get_param( 'opcio' ),
		'ip' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '',
		'time' => date('Y-m-d H:i'),
	),
	array(
		'%s',
		'%s',
		'%s',
		'%s',
		'%s'
	)
);

	$missatge = "Benvolgut subscriptor,\n\nLa resposta ha estat registrada. Moltes gràcies per la seva col·laboració i per confiar amb EL 9 NOU.";
	wp_mail( $request->get_param( 'email' ), 'Gràcies per participar', $missatge );

	return [
		'status' => 'success',
		'message' => 'Hem registrat el vot correctament. Moltes gràcies per participar',
	];

	return $dni;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/obte-contingut-premium', array(
        'methods' => 'GET',
        'callback' => 'obtePremium',
    ) );
} );


function obtePremium(WP_REST_Request $request) {
    $api_token = $request->get_param( 'api_token' );
    $id = $request->get_param( 'id' );

    echo apply_filters('the_content', get_post_field('post_content', $id));

    exit();
}

function json_basic_auth_handler( $user ) {
	global $wp_json_basic_auth_error;
	$wp_json_basic_auth_error = null;
	// Don't authenticate twice
	if ( ! empty( $user ) ) {
		return $user;
	}
	// Check that we're trying to authenticate
	if ( !isset( $_SERVER['PHP_AUTH_USER'] ) ) {
		return $user;
	}
	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];
	/**
	 * In multi-site, wp_authenticate_spam_check filter is run on authentication. This filter calls
	 * get_currentuserinfo which in turn calls the determine_current_user filter. This leads to infinite
	 * recursion and a stack overflow unless the current function is removed from the determine_current_user
	 * filter during authentication.
	 */
	remove_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );
	$user = wp_authenticate( $username, $password );
	add_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );
	if ( is_wp_error( $user ) ) {
		$wp_json_basic_auth_error = $user;
		return null;
	}
	$wp_json_basic_auth_error = true;
	return $user->ID;
}
add_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );
function json_basic_auth_error( $error ) {
	// Passthrough other errors
	if ( ! empty( $error ) ) {
		return $error;
	}
	global $wp_json_basic_auth_error;
	return $wp_json_basic_auth_error;
}
add_filter( 'rest_authentication_errors', 'json_basic_auth_error' );
