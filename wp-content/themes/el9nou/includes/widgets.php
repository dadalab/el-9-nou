<?php

include( get_stylesheet_directory() . '/widgets/nou_mes_vistes.php' );
include( get_stylesheet_directory() . '/widgets/nou_banner.php' );
include( get_stylesheet_directory() . '/widgets/nou_hemeroteca_portada.php' );
include( get_stylesheet_directory() . '/widgets/nou_xarxes_socials.php' );
include( get_stylesheet_directory() . '/widgets/nou_publicitat.php' );
include( get_stylesheet_directory() . '/widgets/nou_club_subscriptor.php' );
include( get_stylesheet_directory() . '/widgets/nou_acudits.php' );
include( get_stylesheet_directory() . '/widgets/nou_el9fm.php' );
include( get_stylesheet_directory() . '/widgets/nou_blogosfera.php' );
include( get_stylesheet_directory() . '/widgets/nou_agenda.php' );

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'nou_mes_vistes' );
	register_widget( 'nou_banner' );
	register_widget( 'nou_hemeroteca_portada' );
	register_widget( 'nou_xarxes_socials' );
    register_widget( 'nou_publicitat' );
    register_widget( 'nou_club_subscriptor' );
    register_widget( 'nou_acudits' );
    register_widget( 'nou_el9fm' );
    register_widget( 'nou_blogosfera' );
	register_widget( 'nou_agenda' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

function el9nou_widgets_init() {

    register_sidebar( array(
        'name'          => 'Portada Barra Lateral',
        'id'            => 'home_sidebar',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'el9nou_widgets_init' );