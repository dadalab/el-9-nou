<?php

add_action('wp_ajax_nopriv_get_temps', 'queryADarkSky');
add_action('wp_ajax_get_temps', 'queryADarkSky');

function queryADarkSky()
{
    $request = wp_remote_get('https://api.darksky.net/forecast/9bcfd0059fe86c3737741e18f95fa3ed/' . $_POST['lat'] . ',' . $_POST['lng'] . '?units=si&lang=ca');
    if (is_wp_error($request)) {
        return false;
    }
    //var_dump($request);

    echo wp_remote_retrieve_body($request);
    die();
}

function temps_consulta()
{
    ?>
<script>
  	var tempsVue = new Vue({
  		el: '#temps-vue',
  		data: {
  			'locationTemps': localStorage.getItem('ciutat-preferida') || "<?php echo(edicio_actual()) == 'osona-ripolles' ? 'Vic' : 'Granollers'; ?>",
  			'canviCiutat': false,
  			'icons': null
  		},
  		methods: {
  			buscaCiutat: function() {
  				this.canviCiutat = 0;
  				this.actualitzaTemps();
  				localStorage.setItem('ciutat-preferida', this.locationTemps);
  			},

  			actualitzaTemps: function() {
  				var that = this;
  				var geoAPI = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.locationTemps + '&key=AIzaSyDPYtRNSQhvIsoNGiQlAZ4cWz-PRKXEC2s&language=ca';
  				
  				this.$http.get(geoAPI, {credentials: false}).then(function(response) {
	            	data = response.json();
	            	
	            	var lat = data.results[0].geometry.location.lat;
					var lng = data.results[0].geometry.location.lng;
					jQuery.ajax({
						url : "<?php echo admin_url('admin-ajax.php') ?>",
						type : 'post',
						dataType: 'json',
						data : {
							action : 'get_temps',
							lat : lat,
							lng : lng
						},
						success : function( data ) {
							var dies = ['avui', 'dema', 'dema-passat'];
			    			var resultats = data.list;

			    			//var avui = resultats[0];
			    			var dia = dies[0];

				      		$('#temps-' + dia).find('.imatge').html( that.setWeatherIcon(data.daily.data[0].icon));
				      		$('#temps-' + dia).find('.temperatura-max').html(parseFloat(data.daily.data[0].temperatureMin).toFixed(1) + 'º / ' + parseFloat(data.daily.data[0].temperatureMax).toFixed(1) + 'º');
				      		$('#temps-' + dia).find('.temperatura-min').html('Prob. Pluja: ' + parseFloat(data.daily.data[0].precipProbability * 100).toFixed(0) + '%');
			      			for (var i = 1; i < dies.length; ++i) {
					      		var dia = data.daily.data[i];
					      		$('#temps-' + dies[i]).find('.imatge').html( that.setWeatherIcon(dia.icon) );
					      		$('#temps-' + dies[i]).find('.temperatura-max').html(parseFloat(dia.temperatureMin).toFixed(1) + 'º / ' + parseFloat(dia.temperatureMax).toFixed(1) + 'º');
					      		$('#temps-' + dies[i]).find('.temperatura-min').html('Prob. Pluja: ' + parseFloat(dia.precipProbability * 100).toFixed(0) + '%');
				      		}
						}
					});
				
				});
  				
  				
				/**$.get( geoAPI, function( data ) {
					var lat = data.results[0].geometry.location.lat;
					var lng = data.results[0].geometry.location.lng;
					jQuery.ajax({
						url : "<?php echo admin_url('admin-ajax.php') ?>",
						type : 'post',
						dataType: 'json',
						data : {
							action : 'get_temps',
							lat : lat,
							lng : lng
						},
						success : function( data ) {
							var dies = ['avui', 'dema', 'dema-passat'];
			    			var resultats = data.list;

			    			//var avui = resultats[0];
			    			var dia = dies[0];

				      		$('#temps-' + dia).find('.imatge').html( that.setWeatherIcon(data.daily.data[0].icon));
				      		$('#temps-' + dia).find('.temperatura-max').html(parseFloat(data.daily.data[0].temperatureMin).toFixed(1) + 'º / ' + parseFloat(data.daily.data[0].temperatureMax).toFixed(1) + 'º');
				      		$('#temps-' + dia).find('.temperatura-min').html('Prob. Pluja: ' + data.daily.data[0].precipProbability * 100 + '%');
			      			for (var i = 1; i < dies.length; ++i) {
					      		var dia = data.daily.data[i];
					      		$('#temps-' + dies[i]).find('.imatge').html( that.setWeatherIcon(dia.icon) );
					      		$('#temps-' + dies[i]).find('.temperatura-max').html(parseFloat(dia.temperatureMin).toFixed(1) + 'º / ' + parseFloat(dia.temperatureMax).toFixed(1) + 'º');
					      		$('#temps-' + dies[i]).find('.temperatura-min').html('Prob. Pluja: ' + parseFloat(dia.precipProbability * 100).toFixed(0) + '%');
				      		}
						}
					});
				});**/

	      	},

	      	obteHora: function(hora) {
				var d = new Date(hora * 1000);
				var hours = d.getHours();
				var minutes = d.getMinutes();
				if (hours < 10) hours = '0' + hours;
				if (minutes < 10) minutes = '0' + minutes;
				return hours + ':' + minutes;
	      	},
	      	setWeatherIcon: function(code) {
				var icons = {
					'clear-day': 'day-sunny',
					'clear-night': 'night-clear',
					'rain': 'rain',
					'snow': 'snow',
					'sleet': 'sleet',
					'wind': 'strong-wind',
					'fog': 'fog',
					'cloudy': 'cloudy',
					'partly-cloudy-day': 'day-cloudy',
					'partly-cloudy-night': 'night-cloudy',
					'hail': 'hail',
					'thunderstorm': 'thunderstorm',
					'tornado': 'tornado',
				};

			  	return '<i class="wi wi-' + icons[code] + '"></i>';
			},
  		}
  	});
</script>
<?php
}
add_action('wp_footer', 'temps_consulta'); ?>