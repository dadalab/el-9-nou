<?php
function el9tv_programa_actual() {
    $dies = array('dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge');
    $diaActual = $dies[date('N') - 1];
    $avui = new WP_Query(array(
        'post_type' => 'programacio_tv',
        'name' => $diaActual
    )); 
    while ($avui->have_posts()) : $avui->the_post();
        $info = get_field('panels_data');
        $programa_anterior = null;

        foreach($info['widgets'] as $programa) :
            if ($programa['hora'] > '5.30') : // Si és un programa del mateix dia.
                if (strtotime($programa['hora']) < strtotime(current_time('H.i'))) :
                    $programa_anterior = $programa;
                    continue;
                endif;
            endif;
        endforeach;
        if ($programa_anterior['programa']) :
            $programa = new WP_Query( array(
                'post_type' => 'programa',
                'p' => $programa_anterior['programa'],
                'posts_per_page' => 1
            ));
            while ( $programa->have_posts() ) : $programa->the_post(); ?>
                <a href="<?php the_permalink() ?>" target="_blank">
                    <span class="programa-titol"><?php the_title() ?></span><br>
                    <span class="programa"><?php echo $programa_anterior['subtitol'] ?></span>
                    <p><?php echo $programa_anterior['detall'] ?></p>
                </a><?php
            endwhile;
        else : ?>
        <a href="#">
            <span class="programa-titol"><?php echo $programa_anterior['titol']; ?></span><br>
            <span class="programa"><?php echo $programa_anterior['subtitol'] ?></span>
            <p><?php echo $programa_anterior['detall'] ?></p></a><?php
        endif;
        return;
    endwhile;
}