<?php

function hemeroteca_get_last_portada() {
	$portada = new WP_Query(
		array(
			'post_type' => 'hemeroteca',
			'posts_per_page' => 1,
			'tax_query' => edicio_query_per_llistats()
		)
	);
	while ($portada->have_posts()) : $portada->the_post();
        get_template_part('templates/hemeroteca/latest', 'widget');
	endwhile;
}