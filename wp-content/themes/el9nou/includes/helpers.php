<?php

function nou_home() {
	global $wp;
	$urls = [
		'osona-ripolles',
		'valles-oriental',
		'valles-oriental/?canvi_edicio=valles-oriental',
		'osona-ripolles/?canvi_edicio=osona-ripolles'
	];

	// Per varnish, si hi ha la variable is_home, enviem OK!
	if ($_GET['is_home']) return true;

	return in_array($wp->request, $urls);
}

function sidebar_hidden() {
	return is_mobile() || is_tablet();
}

function current_url() {
	echo site_url() . $_SERVER['REQUEST_URI'];
}