<?php

global $ads;
$ads = false;

get_header(); ?>
<main id="main" role="main" class="main categoria hemeroteca">
    <div class="container">
        <div class="row">
            <div class="col-md-9" id="hemeroteca">

                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">HEMEROTECA</div>
                    <div class="share"><?php the_news_sharing() ?></div>
                </div><?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $hemeroteca = new WP_Query(
                    array(
                        'post_type' => 'hemeroteca',
                        'posts_per_page' => 18,
                        'paged' => $paged,
                        'tax_query' => edicio_query_per_llistats(),
                    )
                ); ?>

                <!-- noticies actuals -->
                <section id="noticies-recents">
                    <div class="row"><?php
                        if ($hemeroteca->have_posts()) :
                            while ($hemeroteca->have_posts()) : $hemeroteca->the_post();
                                get_template_part('templates/hemeroteca/single', 'arxiu');
                                $count++;
                            endwhile;
                        endif; ?>
                    </div>
                    <?php if ($hemeroteca->max_num_pages > 1) : ?>
                        <div class="row">
                            <nav class="prev-next-posts">
                                <div class="prev-posts-link">
                                    <?php echo get_next_posts_link('Portades més antigues', $hemeroteca->max_num_pages); ?>
                                </div>
                                <div class="next-posts-link">
                                    <?php echo get_previous_posts_link('Portades més recents'); ?>
                                </div>
                            </nav>
                        </div>
                    <?php endif ?>
                </section><!-- / noticies recents -->

            </div><!-- / col-md-9 -->

            <?php get_sidebar() ?>
        </div><!-- / row -->
    </div>
</main>
<!-- / section -->
<?php get_footer() ?>