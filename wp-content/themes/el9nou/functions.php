<?php

global $preferences;
$preferences = [
    'home_limit' => 40
];

$path = getcwd();
$folderName = str_replace(dirname($path), '', $path);
$directory = str_replace($folderName, '', $path);

include($directory . '/general-variables.php');

add_theme_support('post-thumbnails');
add_image_size('9nou-medium', 650, 435, true);

global $adsEnabled;
$adsEnabled = false;



/** Gestió de l'edició, redireccions, cookies... **/
include( get_stylesheet_directory() . '/includes/helpers.php' );

/** Gestió de l'edició, redireccions, cookies... **/
include( get_stylesheet_directory() . '/includes/edicio.php' );

include( get_stylesheet_directory() . '/includes/el9tv.php' );

/** Resum de les funcions dels anuncis **/
include( get_stylesheet_directory() . '/includes/ads.php' );

/** Funcions de l'hemeroteca i gestió de portades **/
include( get_stylesheet_directory() . '/includes/hemeroteca.php' );

/** Funcions de temps d'arreu **/
include( get_stylesheet_directory() . '/includes/temps.php' );

/** Funcions de l'hemeroteca i gestió de portades **/
include( get_stylesheet_directory() . '/includes/author.php' );

/** Funcions de compartir a les xarxes socials **/
include( get_stylesheet_directory() . '/includes/shares.php' );

/** Funcions de widgets i posicions **/
include( get_stylesheet_directory() . '/includes/widgets.php' );

/** Resum de les funcions api **/
include( get_stylesheet_directory() . '/includes/api.php' );

/** Incloure importació autmàtica diaris... **/
include( get_stylesheet_directory() . '/includes/importa-diari.php' );

/**
 *	Afegim estils i scripts a la web
 */
function el9nou_styles() {
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css?v=2');
    if (edicio_actual() == 'valles-oriental') wp_enqueue_style('main-valles', get_template_directory_uri() . '/css/main-valles.css');
    wp_enqueue_style('overlay-general', get_template_directory_uri() . '/css/overlay-general.css');
    wp_enqueue_style('footer', get_template_directory_uri() . '/css/footer.css?v=5');
    wp_enqueue_style('print', get_template_directory_uri() . '/css/print.css');
    wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css?v=16');
    wp_enqueue_style('photoswipe', get_template_directory_uri() . '/js/vendor/photoswipe/photoswipe.css');
    wp_enqueue_style('photoswipe-default', get_template_directory_uri() . '/js/vendor/photoswipe/default-skin/default-skin.css');
    wp_enqueue_style('weather-font', get_template_directory_uri() . '/css/src/libs/weather-icons/weathericons-regular-webfont.ttf');
    wp_enqueue_style('weather-font-2', get_template_directory_uri() . '/css/src/libs/weather-icons/weather-icons.css');
    wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js');
    wp_enqueue_script('vuejs', 'https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js');
    wp_enqueue_script('vuejs-resource', 'https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js');
    wp_enqueue_style('calendar', 'https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.css');
}
add_action('wp_enqueue_scripts', 'el9nou_styles');


function the_thumbnail_title() {
    echo get_post(get_post_thumbnail_id())->post_title;
}

function the_thumbnail_author() {
    echo get_post(get_post_thumbnail_id())->post_excerpt;
}

function get_post_edition_no_echo($id) {
    $terms = get_the_terms($id, 'edicio');
    foreach ($terms as $index => $term) :
        if ($index > 0) echo ' | ';
        return $term->name;
    endforeach;
}

function get_post_edition($id) {
    $terms = get_the_terms($id, 'edicio');
    foreach ($terms as $index => $term) :
        if ($index > 0) echo ' | ';
        echo $term->name;
    endforeach;
}
function the_post_edition() {
    $terms = get_the_terms(get_the_ID(), 'edicio');
    echo $terms[0]->name;
}

function get_post_city() {
    $postCity = get_the_terms(get_the_ID(), 'poblacio');
    $poblacio = $postCity[0];

    return isset($poblacio->name) ? '<a href="' . get_term_link($poblacio->term_id, 'poblacio') . '">' . $poblacio->name . '</a>' : false;
}

function get_the_authors() {
    $authors = get_coauthors(get_the_ID());
    $totalAuthors = count( $authors );
    foreach($authors as $count => $author) :
        if ($author->display_name == 'El 9 Nou') echo $author->display_name;
        else if ($author->display_name == 'EL 9 TV') echo $author->display_name;
        else echo '<a href="' . get_author_posts_url( $author->ID, $author->user_login ) . '">' . $author->display_name . '</a>';
        if ($totalAuthors > 1) :
            echo ', ';
            $totalAuthors--;
        endif;
    endforeach;
}

function change_author_permalinks() {
    global $wp_rewrite;
    $wp_rewrite->author_base = 'autor';
    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '/%author%';
}
add_action('init','change_author_permalinks');

function post_category_for_icon() {
    $child = null;
    foreach ($category = get_the_category() as $cat) {
        if ($cat->category_parent != 0) {
            $child = $cat->slug;
        }
    }

    ($child) ? trim($child, ">") : $category[0]->slug;
    $cultura = ['arts-esceniques', 'cultura-popular', 'llibres', 'calaix'];
    $esports = ['futbol', 'handbol', 'hoquei-patins', 'motor'];
    if (in_array($child, $cultura))
        return 'cultura-i-gent';
    if (in_array($child, $esports))
        return 'esports';
    return $child;
}

function post_category_for_icon_with_id($postId) {
    $child = null;
    foreach ($category = get_the_category($postId) as $cat) {
        if ($cat->category_parent != 0) {
            $child = $cat->slug;
        }
    }

    ($child) ? trim($child, ">") : $category[0]->slug;
    $cultura = ['arts-esceniques', 'cultura-popular', 'llibres', 'calaix'];
    $esports = ['futbol', 'handbol', 'hoquei-patins', 'motor'];
    if (in_array($child, $cultura))
        return 'cultura-i-gent';
    if (in_array($child, $esports))
        return 'esports';
    return $child;
}

function the_post_category_child()
{
    $child = null;
    foreach ($category = get_the_category() as $cat) {
        if ($cat->category_parent != 0) {
            $child = $cat->name;
        }
    }

    echo ($child) ? trim($child, ">") : $category[0]->name;
}

function the_post_category_breadcrumb() {
    $child = null;
    foreach ($category = get_the_category() as $cat) {
        if ($cat->category_parent != 0) {
            $child = $cat->term_taxonomy_id;
        }
    }
    if ($child) :
        $string = get_category_parents($child, true, ' | ');
        echo substr($string, 0, strlen($string) -3); else :
        the_category(' ', 'single');
    endif;
}

function the_post_category_link()
{
    $postCategory = get_the_category();
    echo get_category_link($postCategory[0]->term_id);
}

function the_comments_link() {
    return;
    if (comments_open() && get_comments_number() > 0) :
        echo '| ';
    echo comments_popup_link('0 comentaris', '1 comentari', '% comentaris');
    endif;
}

function el9nou_post_thumbnail() {
    echo the_post_thumbnail('medium', array( 'class' => 'img-responsive' ));
}

/** Wordpress per defecte mostra una sintaxi pels tags que hem de modificar per adaptar a l'estil **/
add_filter('the_tags', 'el9nou_custom_tags_output');
function el9nou_custom_tags_output($list)
{
    $list = str_replace('rel="tag">', 'rel="tag"><span>', $list);
    $list = str_replace('</a>', '</span></a>', $list);
    return $list;
}

function widgets_portada($tabs) {
    $tabs[] = array(
        'title' => __('Gestió Portada', 'widgets_portada'),
        'filter' => array(
            'groups' => array('widgets_portada')
        )
    );

    return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', 'widgets_portada', 20);

function the_short_excerpt($length) {
    return 20;
}
add_filter('excerpt_length', 'the_short_excerpt', 999);


function limit_characters($word, $limit) {
    if (strlen($word) > $limit) {
        return mb_substr($word, 0, $limit) . '...';
    }
    return $word;
}

/* Function which remove Plugin Update Notices */
function disable_plugin_updates( $value ) {
   //unset( $value->response['wordpress-popular-posts/wordpress-popular-posts.php'] );
   //return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );


/** Per optimitzar el buscador, no mirem el contingut de la notícia. Només títol i resum **/
function dont_search_post_content( $search_sql ) {
    global $wpdb;

    if ( strpos( $search_sql, 'post_content LIKE' ) ) {
        if ( is_user_logged_in() ) {
            $search_sql = preg_replace( "|OR \({$wpdb->posts}.post_content LIKE '(.+)'\)|", '', $search_sql );
        } else {
            $search_sql = preg_replace( "|OR \({$wpdb->posts}.post_content LIKE '(.+)'\)|", '))  AND (wp_posts.post_password = \'\')', $search_sql );
        }
    }
    return $search_sql;
}
add_action( 'posts_search', 'dont_search_post_content', 10000, 1 );

// COLUMNA EDICIÓ A L'ADMIN
function nou_columnes_admin($defaults) {
    $defaults['edicio'] = 'Edició';
    return $defaults;
}

// IMATGE DESTACADA A L'ADMIN
function nou_columnes_content($column_name, $post_ID) {
    if ($column_name == 'edicio') {
        echo get_post_edition($post_ID);
    }
}
add_filter('manage_posts_columns', 'nou_columnes_admin');
add_action('manage_posts_custom_column', 'nou_columnes_content', 1, 2);

/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'post'; // change to your post type
    $taxonomy  = 'edicio'; // change to your taxonomy
    if ($typenow == $post_type) {
        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Mostant totes les edicions"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => false,
            'hide_empty'      => true,
        ));
    };
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'post'; // change to your post type
    $taxonomy  = 'edicio'; // change to your taxonomy
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }
}

/**
 * Redirect back to homepage and not allow access to
 * WP admin for Subscribers.
 */
function el9nou_redirect_admin() {
    if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
        wp_redirect( site_url() );
        exit;
    }
}
add_action( 'admin_init', 'el9nou_redirect_admin' );

function enllac_club() {
    echo 'http://9club.el9nou.cat/' . edicio_actual() . '/ofertes';
}

function search_filter($query) {
    if ( ! is_admin() && $query->is_main_query() ) {
        if ($query->is_search) {
            if ($_GET['post_type'] == 'post')
                $query->set('tax_query', edicio_query_per_llistats() );
        }
    }
}
add_action('pre_get_posts','search_filter');

add_action( 'admin_menu' , 'ja_new_admin_menu_items' );
function ja_new_admin_menu_items() {
    global $submenu;
    $url = get_bloginfo('url');
    /**
     * edit.php - the parent slug, in this case will go under Posts. Using index.php would place it under Dashboard, etc.
     * Scheduled - the name of the menu item
     * manage_options - role which will see the link
     * URL - speaks for itself :)
     */
    $submenu['edit.php?post_type=esqueles'][503] = array( 'Defuncions Osona Ripollès', 'publish_posts' , $url . '/wp-admin/post.php?post=75316&action=edit' );
    $submenu['edit.php?post_type=esqueles'][501] = array( 'Defuncions Valles Oriental', 'publish_posts' , $url . '/wp-admin/post.php?post=75315&action=edit' );
}

add_action( 'template_redirect', function() {
    if ( is_singular( 'programa' ) ) {
        global $wp_query;
        $page = ( int ) $wp_query->get( 'page' );
        if ( $page > 1 ) {
            // convert 'page' to 'paged'
            $query->set( 'page', 1 );
            $query->set( 'paged', $page );
        }
        // prevent redirect
        remove_action( 'template_redirect', 'redirect_canonical' );
    }
}, 0 );

function paginacio_propia_single ( $label = NULL, $dir = 'next', WP_Query $query = NULL ) {
    if ( is_null( $query ) ) $query = $GLOBALS['wp_query'];
    $max_page = ( int ) $query->max_num_pages;
    // Only one page for the query, do nothing
    if ( $max_page <= 1 ) return;
    $paged = ( int ) $query->get( 'paged' );
    if ( empty( $paged ) ) $paged = 1;
    $target_page = $dir === 'next' ?  $paged + 1 : $paged - 1;
    // If we are in 1st page and required previous or in last page and required next,
    // do nothing
    if ( $target_page < 1 || $target_page > $max_page ) return;
    if ( null === $label )  $label = __( 'Next Page &raquo;' );
    $label = preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label );
    printf( '<a href="%s">%s</a>', get_pagenum_link( $target_page ), $label );
}

/**
 * Use * for origin
 */
add_action( 'rest_api_init', function() {
    
	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function( $value ) {
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
		header( 'Access-Control-Allow-Credentials: true' );

		return $value;
		
	});
}, 15 );


function edicio_change_post_link($link, $post) {
	//var_dump($link);
	//die();
	
        //replace www part with server1 using the following php function
        //preg_replace ( patter, replace, subject ) syntax
        $link = preg_replace('/osona-ripolles/', '', $link);
        $link = preg_replace('/valles-oriental/', '', $link);
        
        return $link;
}
add_filter('preview_post_link', 'edicio_change_post_link', 10, 2);