<?php
/**
 *
 *	Template name: Textos llargs
 */
get_header() ?>

<main id="main" role="main" class="main textos-legals">
   <div class="container">
        <div class="row">               
           <div class="col-md-8"><?php 
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();?>
                        <h1 class="textos-legals"><?php the_title() ?></h1>
                        <p><?php the_content(); ?></p><?php
                    endwhile;
                endif; ?>
            </div>
			<?php get_sidebar(); ?>
        </div><!-- / row -->
   </div>
</main>
<!-- / section -->
<?php get_footer(); ?>