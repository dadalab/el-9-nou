<?php get_header('el9tv');

global $paged; ?>

<main id="main" role="main" class="main programa-fitxa">
   <div class="container">
        <div class="row">               
           	<div class="col-md-6"><?php 
					if (has_post_thumbnail()) : 
						the_post_thumbnail();
					endif; ?>
			</div>
				
			<div class="col-md-6">

				<ul class="compartir">
				    <li>Compartir</li>
				    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				    <li><a href="https://twitter.com/intent/tweet?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				    <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<?php if (wp_is_mobile()) : ?>
				    	<li><a href="whatsapp://send?text=<?php the_permalink() ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>    
				    <?php endif ?>
				    <li><a href="#" class="single-share-url" data-url="<?php the_permalink() ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></li>
				</ul>
			
				<h1><?php the_title(); ?></h1>
				<p>
				<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					endif;
				?>
				</p>
			</div>
			
			<div class="col-md-12 videos-relacionats">
				<!-- <hr> -->
				<h2>Vídeos del programa </h2>
				<div class="row"><?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

					$relacionats = new WP_Query( array( 
						'post_type' 	=> 'video',
						'meta_key'		=> 'programa',
						'meta_value'	=> get_the_ID(),
						'posts_per_page' => 8,
						'paged' => $paged
					)); 
					$count = 0;
					while ($relacionats->have_posts()) : $relacionats->the_post();
						if ($count % 4 == 0) echo '</div><div class="row">';
						get_template_part('templates/videos/miniatura', 'relacionats');
						$count++;
					endwhile; 
					if ($relacionats->max_num_pages > 1) {  ?>
						<nav class="prev-next-posts">
							<div class="prev-posts-link">
  								<?php echo paginacio_propia_single('Més antics', 'next', $relacionats); ?>
							</div>
							<div class="next-posts-link" style="clear: none;">
  								<?php echo paginacio_propia_single('Més recents', 'prev', $relacionats); ?>
							</div>
						</nav><?php } ?>
					</div>
				</div>
			</div>
        </div><!-- / row -->
   </div>
</main>
<!-- / section -->

<?php get_footer(); ?>