<?php
/**
 *
 *	Template name: Entrada usuaris
 */
global $ads;
$ads = false;

get_header(); ?>

<main id="main" role="main" class="main entrar">
    <div class="container">
        <div class="row">
        	<div class="col-md-6 col-md-offset-3" id="registre">
				<h2>Inicia sessió</h2>
        		<?php custom_login_form() ?>
        		
        		<span>Si no tens usuari de EL 9 NOU, registra't <a href="<?php url_site('el9nou') ?>/registre">aquí</a></span>
        	</div>
        </div>
	</div>
</main>

<?php get_footer(); ?>