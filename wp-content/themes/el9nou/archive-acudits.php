<?php

global $ads;
$ads = false;

get_header(); ?>
<main id="main" role="main" class="main categoria hemeroteca">
    <div class="container">
        <div class="row">               
            <div class="col-md-9">
                
                <!-- barra superior -->
                <div class="barra-superior clearfix">
                    <div class="breadcrumb categoria">ACUDITS</div>
                </div><?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $acudits = new WP_Query(
                    array(
                        'post_type' => 'acudits',
                        'posts_per_page' => 18, 
                        'paged' => $paged
                        //'tax_query' => edicio_query_per_llistats(),
                    )
                ); ?>
                
                <!-- noticies actuals -->
                <section id="noticies-recents">     
                    <div class="row">
                    
                    <!-- gallery -->
                    <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                        <?php
                            if ($acudits->have_posts()) :

                                $i = 0;
                                
                                while ($acudits->have_posts()) : $acudits->the_post();
                                     if ($i == 0) 
                                        get_template_part('templates/acudits/single', 'arxiu-featured');
                                    else 
                                        get_template_part('templates/acudits/single', 'arxiu');             
                                    $i++;
                                endwhile;
                            endif; ?>
                    </div>
                    <!--  -->

                    </div>
                    <?php if ($acudits->max_num_pages > 1) : ?>
                        <div class="row">
                            <nav class="prev-next-posts">
                                <div class="prev-posts-link">
                                    <?php echo get_next_posts_link('Portades més antigues', $acudits->max_num_pages); ?>
                                </div>
                                <div class="next-posts-link">
                                    <?php echo get_previous_posts_link('Portades més recents'); ?>
                                </div>
                            </nav>
                        </div>
                    <?php endif ?>
                </section><!-- / noticies recents -->
            
            </div>

             <?php get_sidebar() ?>

        </div>
    </div>
</main>


<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <!-- <button class="pswp__button pswp__button- -share" title="Share"></button> -->

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<?php get_footer() ?>