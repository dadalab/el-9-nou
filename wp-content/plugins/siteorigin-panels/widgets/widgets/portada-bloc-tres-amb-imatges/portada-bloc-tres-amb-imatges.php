<?php

class SiteOrigin_Panels_Widget_Portada_Bloc_Tres_Amb_Imatges extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Portada bloc de 3 amb imatges', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar 3 notícies amb imatge', 'siteorigin-panels'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			array()
		);
	}
}