<?php 
$noticies = new WP_Query( array(
    'orderby'        => 'rand',
    'posts_per_page' => '3',
    'post__not_in' => get_option('sticky_posts')
));

if ( $noticies->have_posts() ) : ?>
    <section class="articles-petits">
        <h3>Titular genèric a escollir</h3><?php
        while ($noticies->have_posts()) : $noticies->the_post(); ?>
            <a href="<?php the_permalink() ?>">
                <div class="col-md-4 item">                                                                                       
                    <figure class="row"><?php the_post_thumbnail() ?></figure>      
                    <h1><?php the_title() ?></h1>
                </div>
            </a>
        <?php endwhile; ?>
    </section><?php
endif;