<?php

class SiteOrigin_Panels_Widget_Portada_Noticies_Dues_Columnes extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Portada Dues columnes de notícies amb imatge', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar dues notícies, a dues columnes amb imatge', 'siteorigin-panels'),
				'panels_groups' => array('widgets_portada')
			),
			array(
				),
			array(
				'anunci_esquerre' => array(
					'type' => 'checkbox',
					'label' => __('Anunci esquerra', 'siteorigin-panels'),
				),
				'anunci_dreta' => array(
					'type' => 'checkbox',
					'label' => __('Anunci dreta', 'siteorigin-panels'),
				),

		    )
		);
	}
}