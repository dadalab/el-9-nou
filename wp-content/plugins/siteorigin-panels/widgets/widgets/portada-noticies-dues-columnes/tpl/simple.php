<?php 
$noticies = new WP_Query( array(
    'orderby'        => 'rand',
    'posts_per_page' => '2',
    'post__not_in' => get_option('sticky_posts')
));
?>
<section class="noticies-portada">    
    <div class="bloc row"><?php
        if ($noticies->have_posts()) :
            while ($noticies->have_posts()) : $noticies->the_post();
                get_template_part('templates/noticies/portada', '1-2-imatge');
            endwhile;
        endif; ?>                        
    </div>
</section>  
