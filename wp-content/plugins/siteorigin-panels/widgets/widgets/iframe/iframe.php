<?php

class SiteOrigin_Panels_Widget_Iframe extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('En Directe', 'siteorigin-panels'),
			array(
				'description' => __('Publicació en directe de Iframes i directes', 'siteorigin-panels'),
			),
			array(),
			array(
				'video' => array(
					'type' => 'text',
					'label' => __('Enllaç embeded', 'siteorigin-panels'),
				)
			)
		);
	}
}