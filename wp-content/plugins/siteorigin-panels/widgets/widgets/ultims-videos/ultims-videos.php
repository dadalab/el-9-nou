<?php

class SiteOrigin_Panels_Widget_Ultims_Videos extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Últims vídeos', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar els últims', 'siteorigin-panels'),
			),
			array(),
			array(
				'hora' => array(
					'type' => 'text',
					'label' => __('Hora', 'siteorigin-panels'),
				),
				'titol' => array(
					'type' => 'text',
					'label' => __('Títol', 'siteorigin-panels'),
				),
				'subtitol' => array(
					'type' => 'text',
					'label' => __('Subtitol', 'siteorigin-panels'),
				),
			)
		);
	}
}