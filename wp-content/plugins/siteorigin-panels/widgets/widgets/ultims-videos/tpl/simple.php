<h2>Últims vídeos</h2>
<div class="row">
<?php
$videos = new WP_Query([
	'post_type' => 'video',
	'post_per_page' => 6
]);

if ( $videos->have_posts() ) : 
	while ( $videos->have_posts() ) : $videos->the_post(); ?>
		<div class="col-md-6">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail() ?></a>
			<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
		</div>
		<?
	endwhile;
endif; ?>

</div> 