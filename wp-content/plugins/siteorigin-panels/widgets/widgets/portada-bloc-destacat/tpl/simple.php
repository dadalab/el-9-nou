<?php 
$noticies = new WP_Query( array(
    'orderby'        => 'rand',
    'posts_per_page' => '1',
    'post__not_in' => get_option('sticky_posts')
));
?>
<section class="noticies-portada">    
    <div class="row"><?php  // bloc
        if ($noticies->have_posts()) :
            while ($noticies->have_posts()) : $noticies->the_post();
                get_template_part('templates/noticies/categoria', 'destacada');
            endwhile;
        endif; ?>                        
    </div> 
</section>  
