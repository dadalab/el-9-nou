<?php

class SiteOrigin_Panels_Widget_Portada_Bloc_Destacat extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Portada bloc destacat', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar notícia destacada i text a l\'esquerre', 'siteorigin-panels'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			array(
			    'text' => array(
					'type' => 'textarea',
					'label' => __('Text', 'siteorigin-panels'),
					'description' => __('Start each new point with an asterisk (*)', 'siteorigin-panels'),
				),
		    )
		);
	}
}