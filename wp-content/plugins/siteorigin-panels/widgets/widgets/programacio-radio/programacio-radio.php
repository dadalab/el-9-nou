<?php

class SiteOrigin_Panels_Widget_Programacio_Radio extends SiteOrigin_Panels_Widget  {
	function __construct() {

		parent::__construct(
			__('Programa Ràdio', 'siteorigin-panels'),
			array(
				'description' => __('Programa per programació', 'siteorigin-panels'),
			),
			array(),
			array(
				'hora' => array(
					'type' => 'text',
					'label' => __('Hora', 'siteorigin-panels'),
				),
				'titol' => array(
					'type' => 'text',
					'label' => __('Títol', 'siteorigin-panels'),
				),
			)
		);
	}

	function widget_classes($classes, $instance) {
		//$classes[] = 'align-'.(empty($instance['align']) ? 'none' : $instance['align']);
		//return $classes;
	}
}