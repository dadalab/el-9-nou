<?php

class SiteOrigin_Panels_Widget_Portada_Noticia_Gran extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Portada notícia destacada', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar notícia destacada', 'siteorigin-panels'),
			),
			array(),
			array()
		);
	}
}