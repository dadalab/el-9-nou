<section class="noticies-portada">
    <!-- bloc NOTICIA DESTACADA -->
    <div class="bloc row">                           
        <div class="col-md-12 noticia">
            <article class="destacada">
                <figure><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/content/noticia-portada.jpg"></a></figure>
                <header>
                    <h2 class="avant-title">Cras ornare tristique elit </h2>
                    <a href="#"><h1 itemprop="headline">Buscant un clúster de l’esport</h1></a>
                </header>                                    
                <p class="sumari">El Consell Comarcal d’Osona fa néixer un projecte per posar en comú empreses del sector de l’esport i el turisme, millorar la competitivitat i generar activitat </p>
                <ul class="info-article">
                    <li class="author"><span>Dolors Altarriba</span> | <a href="#">0 comentaris</a></li>
                    <li class="sharer"><i class="icon-share"></i></li>
                </ul>
            </article>
        </div>                            
    </div><!-- / bloc -->
</section>