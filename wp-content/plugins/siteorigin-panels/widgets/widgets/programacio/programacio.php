<?php

class SiteOrigin_Panels_Widget_Programacio extends SiteOrigin_Panels_Widget  {
	function __construct() {

		/** Seleccionem tots els programes pel select **/
		$posts = query_posts( array('post_type' => 'programa', 'posts_per_page' => -1) );
		$programas = array();
		$programas[0] = '-';
		foreach($posts as $post) :
			$programas[$post->ID] = $post->post_title;
		endforeach;
		wp_reset_query();
		
		parent::__construct(
			__('Programa', 'siteorigin-panels'),
			array(
				'description' => __('Programa per programació', 'siteorigin-panels'),
			),
			array(),
			array(
				'hora' => array(
					'type' => 'text',
					'label' => __('Hora', 'siteorigin-panels'),
				),
				'titol' => array(
					'type' => 'text',
					'label' => __('Títol', 'siteorigin-panels'),
				),
				'subtitol' => array(
					'type' => 'text',
					'label' => __('Subtitol', 'siteorigin-panels'),
				),
				'detall' => array(
					'type' => 'text',
					'label' => __('Detall', 'siteorigin-panels'),
				),

				'programa' => array(
					'type' => 'select',
					'label' => __('Programa', 'siteorigin-panels'),
					'options' => $programas,
				),
			)
		);
	}

	function widget_classes($classes, $instance) {
		$classes[] = 'align-'.(empty($instance['align']) ? 'none' : $instance['align']);
		return $classes;
	}
}