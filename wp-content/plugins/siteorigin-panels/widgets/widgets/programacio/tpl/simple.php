<?php   
if ($instance['programa']) :
	$programa = new WP_Query( array(
		'post_type' => 'programa',
		'p' => $instance['programa'],
		'posts_per_page' => 1
	));
	while ( $programa->have_posts() ) : $programa->the_post(); ?>
		<div class="hora-programa">
			<a href="<?php the_permalink() ?>">
				<span class="data"><?php echo $instance['hora']; ?> h.</span>
				<span class="title"><?php the_title() ?><br></span>
				<span class="programa"><?php echo $instance['subtitol'] ?></span>
				<span class="detall"><?php echo $instance['detall'] ?></span>
			</a>
		</div><?php
	endwhile;
else :  ?>
	<div class="hora-programa">
		<span class="data"><?php echo $instance['hora']; ?> h.</span>
		<span class="titol-programa"><?php echo $instance['titol']; ?></span>
		<span class="programa"><?php echo $instance['subtitol'] ?></span>
		<?php if ($instance['detall'] != "") : ?><span class="detall"><?php echo $instance['detall'] ?></span><?php endif ?>
	</div>
<?php endif; ?>