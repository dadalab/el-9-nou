<?php

class SiteOrigin_Panels_Widget_Portada_Bloc_Tres_Sense_Imatges extends SiteOrigin_Panels_Widget  {
	function __construct() {
		parent::__construct(
			__('Portada bloc de 3 sense imatges', 'siteorigin-panels'),
			array(
				'description' => __('Mòdul per mostrar 3 notícies sense imatge', 'siteorigin-panels'),
			),
			array(),
			array()
		);
	}
}