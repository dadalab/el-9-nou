<!-- carousel-articles -->
<section class="articles-petits">                     
    <a href="#">
        <div class="col-md-4 item no-foto">                                                                                             
            <h1>Un seminari internacional de la UVic presenta resultats de l’aplicació de la democràcia a les escoles</h1>
            <p>El projecte impulsat per la UVic-UCC i la UdG, analitza l’aplicació de processos democràtics a les escoles</p>
        </div>
    </a>
    <a href="#">
        <div class="col-md-4 item no-foto">                                                
            <h1>El Museu del Ter acull una exposició dedicada al beat manlleuenc Josep Guardiet</h1>
            <p>L’exposició es podrà veure al Museu del Ter del 3 fins al 29 de maig</p>
        </div>
    </a>
    <a href="#">
        <div class="col-md-4 item no-foto">                                                
            <h1>Nova convocatòria del concurs de cartells per la festa major de Vic</h1>
            <p>Les obres es poden presentar fins el proper 17 de maig a l’àrea de Cultura de l’Ajuntament de Vic</p>
        </div>
    </a>           
</section><!-- / related-articles -->