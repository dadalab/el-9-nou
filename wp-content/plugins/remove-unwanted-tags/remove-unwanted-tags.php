<?php
/**
* Plugin Name: Remove unwanted tags
* Description: Eliminem tags que surten a la capçalera de wordpress que no interessen
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
?>