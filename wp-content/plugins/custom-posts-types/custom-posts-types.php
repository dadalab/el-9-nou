<?php
/**
* Plugin Name: Custom posts types pel 9nou
* Description: Plugin de registre dels custom post types pel 9Nou
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

function custom_post_type_programacio_tv() {
    $args = array(
        'public' => true,
        'label'  => 'Programació TV'
    );
    register_post_type( 'programacio_tv', $args );
}
add_action( 'init', 'custom_post_type_programacio_tv' );

function custom_post_type_programacio_radio() {
    $args = array(
        'public' => true,
        'label'  => 'Programació Radio'
    );
    register_post_type( 'programacio_radio', $args );
}
add_action( 'init', 'custom_post_type_programacio_radio' );

function custom_post_type_videos() {
    $args = array(
        'public' => true,
        'label'  => 'Vídeos',
        'supports' => array('title', 'editor', 'thumbnail', 'author')
    );
    register_post_type( 'video', $args );
}
add_action( 'init', 'custom_post_type_videos' );

function custom_post_type_seccio_radio() {
    $args = array(
        'public' => true,
        'label'  => 'Seccions Ràdio',
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite'   => array('slug' => 'el9fm/seccio'),
    );
    register_post_type( 'seccio_radio', $args );
}
add_action( 'init', 'custom_post_type_seccio_radio' );

function custom_post_type_programa() {
    $args = array(
        'public' => true,
        'label'  => 'Programes TV',
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite'   => array('slug' => 'el9tv/programa'),
    );
    register_post_type( 'programa', $args );
}
add_action( 'init', 'custom_post_type_programa' );


function custom_post_type_hemeroteca() {
    $args = array(
        'public' => true,
        'label'  => 'Hemeroteca',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
    );
    register_post_type( 'hemeroteca', $args );
}
add_action( 'init', 'custom_post_type_hemeroteca' );

function custom_post_type_esqueles() {
    $args = array(
        'public' => true,
        'label'  => 'Esqueles',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array('slug' => '/defuncions/'),
    );
    register_post_type( 'esqueles', $args );
}
add_action( 'init', 'custom_post_type_esqueles' );

function custom_post_type_acudits() {
    $args = array(
        'public' => true,
        'label'  => 'Acudits',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
    );
    register_post_type( 'acudits', $args );
}
add_action( 'init', 'custom_post_type_acudits' );

function custom_post_type_contingut_patrocinat() {
    $args = array(
        'public' => true,
        'label'  => 'Patrocinadors',
        'has_archive' => false,
        'supports' => array('title', 'editor', 'thumbnail'),
    );
    register_post_type( 'patrocinadors', $args );
}
add_action( 'init', 'custom_post_type_contingut_patrocinat' );
