<?php
/*
Widget Name: Anuncis portada
Description: Anunci per posar a la portada
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Anuncis_Portada_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'anuncis-portada',
			__('Anuncis Portada', 'so-widgets-bundle'),
			array(
				'description' => __('Bloc per posar anuncis a la portada', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'codi_anunci_osona' => array(
        		'type' => 'text',
        		'label' => __('Codi Osona', 'widget-form-fields-text-domain'),
			),
			'codi_anunci_valles' => array(
				'type' => 'text',
        		'label' => __('Codi Vallès', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('anuncis-portada', __FILE__, 'Anuncis_Portada_Widget');