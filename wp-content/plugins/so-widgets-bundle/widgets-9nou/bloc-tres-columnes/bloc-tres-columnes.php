<?php
/*
Widget Name: Bloc tres columnes
Description: Tres columnes per a tres notícies.
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Bloc_Tres_Columnes_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'bloc-tres-columnes',
			__('Bloc de tres columnes', 'so-widgets-bundle'),
			array(
				'description' => __('Tres columnes per a tres notícies', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'titular' => array(
        		'type' => 'text',
        		'label' => __('Títol destacat', 'widget-form-fields-text-domain'),
			),
			'mostrar_imatges' => array(
        		'type' => 'checkbox',
        		'label' => __('Mostrar imatges dels articles?', 'widget-form-fields-text-domain'),
        		'default' => true
			),
			'noticia_1' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia per l\'esquerra', 'widget-form-fields-text-domain'),
			),
			'noticia_2' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia pel mig', 'widget-form-fields-text-domain'),
			),
			'noticia_3' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia per la dreta', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('bloc-tres-columnes', __FILE__, 'Bloc_Tres_Columnes_Widget');