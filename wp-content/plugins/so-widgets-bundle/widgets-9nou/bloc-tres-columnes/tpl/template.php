<?php
$noticies = new WP_Query( array(
 	'post__in' => array( $instance['noticia_1'], $instance['noticia_2'], $instance['noticia_3'] ),
    'orderby' => 'post__in',
    'ignore_sticky_posts' => 1
));

if ( $noticies->have_posts() ) : ?>
    <section class="articles-petits"><?php
    	if ($instance['titular']) : ?>
        	<h3><?php echo $instance['titular'] ?></h3><?php
        endif;
        while ($noticies->have_posts()) : $noticies->the_post();
        	($instance['mostrar_imatges'])
        	? get_template_part('templates/noticies/portada-3-articles', 'imatge')
        	: get_template_part('templates/noticies/portada-3-articles', 'no-imatge');
    	endwhile; ?>
    </section><?php
endif;