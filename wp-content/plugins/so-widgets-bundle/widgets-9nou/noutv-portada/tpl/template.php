<section class="noticies-portada">
    <div class="row">
    	<div class="col-md-12 noticia sencera">
    		<article>
		        <header>
		            <h2 class="avant-title">EL 9 TV en directe</h2>
		            <a href="<?php the_permalink() ?>"><h1 itemprop="headline" style="margin-bottom: 30px;"><?php echo $instance['titular'] ?> </h1></a>
		        </header>            
        		<figure><iframe class="video" src="//player.cdnmedia.tv/embed/f588a26d" width="100%" height="475px" style="border:0"></iframe></figure>                   
    		</article>
    		<ul class="info-article"></ul>
		</div>                            
    </div> 
</section>
