<?php
/*
Widget Name: 9TV Portada
Description: Bloc de El 9 TV en directe.
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class NouTV_Portada_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'noutv-portada',
			__('9TV portada', 'so-widgets-bundle'),
			array(
				'description' => __('9TV Portada', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'titular' => array(
        		'type' => 'text',
        		'label' => __('Titular', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('noutv-portada', __FILE__, 'NouTV_Portada_Widget');