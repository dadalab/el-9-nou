<?php
    $noTitulars = ($instance['columna_1_titular'] == "") && ($instance['columna_2_titular'] == "") && ($instance['columna_3_titular'] == "");
?>

<section class="articles-petits ddsadsad"><?php
	if ($instance['titular']) : ?>
    	<h3><?php echo $instance['titular'] ?></h3><?php
    endif;
    for ($i = 1; $i < 4; $i++) : ?>
        <div class="col-md-4 col-sm-4 col-xs-12 item article-imatge" <?php if ($noTitulars) : ?>style="min-height: 0" <?php endif ?>>
            <a href="<?php echo $instance['columna_' . $i . '_enllac'] ?>">
                <figure class="row" style="background-image: url('<?php echo $instance['columna_' . $i . '_imatge'] ?>');-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;background-repeat: no-repeat; background-position: 50% 50%;">
                </figure><?php
                $text = $instance['columna_' . $i . '_titular'];

                if ($text != "") : ?>
                    <h1><?php echo wp_trim_words($text, 9); ?></h1><?php
                endif; ?>
            </a>
        </div><?php
    endfor ?>

    </section>