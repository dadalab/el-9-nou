<?php
/*
Widget Name: Portada bloc links externs
Description: Enllaça a contingut extern.
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Bloc_Links_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'bloc-links',
			__('Bloc links', 'so-widgets-bundle'),
			array(
				'description' => __('Destaca contingut diferent a les notícies.', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'titular' => array(
        		'type' => 'text',
        		'label' => __('Títol destacat', 'widget-form-fields-text-domain'),
			),
			'columna_1_titular' => array(
        		'type' => 'text',
        		'label' => __('Bloc 1 - Titular', 'widget-form-fields-text-domain'),
			),
			'columna_1_enllac' => array(
        		'type' => 'text',
        		'label' => __('Bloc 1 - Enllaç', 'widget-form-fields-text-domain'),
			),
			'columna_1_imatge' => array(
        		'type' => 'text',
        		'label' => __('Bloc 1 - URL de la imatge', 'widget-form-fields-text-domain'),
			),

			'columna_2_titular' => array(
        		'type' => 'text',
        		'label' => __('Bloc 2 - Titular', 'widget-form-fields-text-domain'),
			),
			'columna_2_enllac' => array(
        		'type' => 'text',
        		'label' => __('Bloc 2 - Enllaç', 'widget-form-fields-text-domain'),
			),
			'columna_2_imatge' => array(
        		'type' => 'text',
        		'label' => __('Bloc 2 - URL de la imatge', 'widget-form-fields-text-domain'),
			),

			'columna_3_titular' => array(
        		'type' => 'text',
        		'label' => __('Bloc 3 - Titular', 'widget-form-fields-text-domain'),
			),
			'columna_3_enllac' => array(
        		'type' => 'text',
        		'label' => __('Bloc 3 - Enllaç', 'widget-form-fields-text-domain'),
			),
			'columna_3_imatge' => array(
        		'type' => 'text',
        		'label' => __('Bloc 3 - URL de la imatge', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('bloc-links', __FILE__, 'Bloc_Links_Widget');