<?php
/*
Widget Name: Destacats portada
Description: Bloc html per la portada
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Destacats_Portada_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'destacats-portada',
			__('Destacats portada', 'so-widgets-bundle'),
			array(
				'description' => __('Destacats Portada', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'esquerre' => array(
        		'type' => 'textarea',
        		'label' => __('Esquerra', 'widget-form-fields-text-domain'),
			),
			'mig' => array(
        		'type' => 'textarea',
        		'label' => __('Mig', 'widget-form-fields-text-domain'),
			),
			'dreta' => array(
        		'type' => 'textarea',
        		'label' => __('Dreta', 'widget-form-fields-text-domain'),
			),

		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('destacats-portada', __FILE__, 'Destacats_Portada_Widget');