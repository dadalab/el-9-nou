<?php 
$noticies = new WP_Query( array(
    'p'        => $instance['noticia'],
    'ignore_sticky_posts' => 1
));
?>
<section class="noticies-portada">
    <div class="row"><?php // bloc
        if ($noticies->have_posts()) :
            while ($noticies->have_posts()) : $noticies->the_post(); 
        		($instance['imatge_tota_amplada'])
        		? get_template_part('templates/noticies/portada-destacat', 'sencer')
        		: get_template_part('templates/noticies/categoria', 'destacada');
            endwhile;
        endif; ?>                        
    </div> 
</section>