<?php
/*
Widget Name: Portada bloc destacat
Description: Tipus de notícia destacada a tota l'amplada.
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Bloc_Destacat_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'bloc-destacat',
			__('Bloc destacat', 'so-widgets-bundle'),
			array(
				'description' => __('Tipus de notícia destacada a tota l\'amplada.', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'imatge_tota_amplada' => array(
        		'type' => 'checkbox',
        		'label' => __('Mostrar la imatge a tota l\'amplada?', 'widget-form-fields-text-domain'),
			),

			'noticia' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia', 'widget-form-fields-text-domain'),
			),

			'data_publicacio' => array(
        		'type' => 'text',
        		'extra_type' => 'date',
        		'default' => current_time('Y-m-d'),
        		'label' => __('Data publicació', 'widget-form-fields-text-domain'),
			),
			'hora_publicacio' => array(
        		'type' => 'text',
        		'extra_type' => 'time',
        		'default' => current_time('H:i'),
        		'label' => __('Data publicació', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('bloc-destacat', __FILE__, 'Bloc_Destacat_Widget');