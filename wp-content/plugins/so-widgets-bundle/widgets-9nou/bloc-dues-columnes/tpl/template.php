<?php 
$noticies = new WP_Query( array(
    'post__in' => array( $instance['noticia_1'], $instance['noticia_2'] ),
    'orderby' => 'post__in',
    'ignore_sticky_posts' => 1
));
?>
<section class="noticies-portada">    
    <div class="bloc row">
        <div class="alsada">
        <?php
        if ($noticies->have_posts()) :
            while ($noticies->have_posts()) : $noticies->the_post();
                ($instance['mostrar_imatges'])
                    ? get_template_part('templates/noticies/portada-1-2', 'imatge')
                    : get_template_part('templates/noticies/portada-1-2', 'no-imatge');
                
            endwhile;
        endif; ?> 
        </div>                       
    </div>
</section>  
