<?php
/*
Widget Name: Bloc dues columnes
Description: Dues columnes per a dues notícies.
Author: Dadàcomunica
Author URI: http://dadacomunica.com
*/

class Bloc_Dues_Columnes_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'bloc-dues-columnes',
			__('Bloc de dues columnes', 'so-widgets-bundle'),
			array(
				'description' => __('Dues columnes per a dues notícies', 'so-widgets-bundle'),
				'panels_groups' => array('widgets_portada')
			),
			array(),
			false,
			plugin_dir_path(__FILE__)
		);

	}

	function initialize_form() {
		return array(
			'mostrar_imatges' => array(
        		'type' => 'checkbox',
        		'label' => __('Mostrar imatges dels articles?', 'widget-form-fields-text-domain'),
        		'default' => true
			),
			
			'noticia_1' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia per l\'esquerra', 'widget-form-fields-text-domain'),
			),
			'noticia_2' => array(
        		'type' => 'link',
        		'label' => __('Sel·lecciona una notícia per la dreta', 'widget-form-fields-text-domain'),
			),
		);
	}

	function get_template_name($instance) {
		return 'template';
	}

	function get_style_name($instance) {
		if(empty($instance['design']['theme'])) return 'atom';
		return $instance['design']['theme'];
	}
}

siteorigin_widget_register('bloc-dues-columnes', __FILE__, 'Bloc_Dues_Columnes_Widget');