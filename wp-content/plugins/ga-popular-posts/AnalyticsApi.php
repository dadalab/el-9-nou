<?php 
add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'el9nou/analytics', array(
        'methods' => 'GET',
        'callback' => 'el9nou_analytics',
    ) );
} );


function el9nou_analytics(WP_REST_Request $request) {
    global $wpdb;
    
    $edicio_actual = $request->get_param( 'edicio' ) ?: 'osona-ripolles';
    
    $SQL = "SELECT wp.ID, pm.value as visits, wp.post_title FROM wp_analyticbridge_metrics pm
			LEFT JOIN wp_analyticbridge_pages pp ON pm.page_id = pp.id
			LEFT JOIN wp_posts wp ON wp.ID = pp.post_id 			
			WHERE pp.pagepath LIKE '%" . $edicio_actual . "%'
			ORDER BY pm.value desc
			LIMIT 10";
			

	$result = $wpdb->get_results( $SQL );
	
	$output = [];
	
	foreach($result as $post)
	{
		$output[] = [
			'ID' => $post->ID,
			'post_title' => $post->post_title,
			'permalink' => get_permalink($post->ID),
			'thumbnail' => get_the_post_thumbnail_url($post->ID, '9nou-medium'),
			'excerpt' => get_the_excerpt($post->ID),
			'icon' => post_category_for_icon_with_id($post->ID)
		];
	}
	
    
    return $output;
}