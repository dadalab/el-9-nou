<?php
/**
* Plugin Name: Importacions de contingut
* Description: Plugin per importar contingut a la web del 9nou de tercers.
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

// define some constants
if (!defined('IMPORTACIONS_PATH')) {
    define('IMPORTACIONS_PATH', trailingslashit(plugin_dir_path(__FILE__)));
}

add_action('admin_menu', 'menu_importacio');

function menu_importacio()
{
    add_menu_page('Configuració importació', 'Configuració importació', 'administrator', __FILE__, 'configuracio_importacio');
}

function configuracio_importacio()
{
    ?>
<div class="wrap">
<h2>Configuració importació XipTv</h2>

	<table class="form-table">
        
    <?php
        $query = new WP_Query([
            'post_type' => 'programa',
            'posts_per_page' => 9999
        ]);
        
    if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
    global $post;
    ?>
				<tr valign="top" id="<?php echo $post->post_name;
    ?>">
					<th scope="row"><?php the_title() ?></th>
       				<td>Última importació: <span><?php echo date('d/m/Y H:i', strtotime(get_option('last_xiptv_' . $post->post_name . '_checked'))) ?></span></td>
       				<td>
       					<button data-programa="<?php echo $post->post_name;
    ?>" class="importa-xip button-primary">Importa ara </button> 
       					<img style="display:none" src="<?php echo site_url() ?>/wp-admin/images/loading.gif">
       				</td>
        		</tr><?php
            endwhile;
    endif;
    ?>
 
    </table>
</div>
<?php 
}

add_action('admin_head', 'import_post');
function import_post()
{
    ?>
<script type="text/javascript">
jQuery(document).ready(function($) {

    $('.importa-xip').click(function(){
    	var programa = $(this).attr('data-programa');
    	$('#' + programa + ' img').show();
        var data = {
            action: 'importa_programa_xiptv',
            programa: programa
        };

        $.post(ajaxurl, data, function(response) {
            $('#' + programa + ' img').hide();
            $('#' + programa + ' span').html(response);
        });
    });
});
</script>
<?php

}

add_action('wp_ajax_importa_programa_xiptv', 'importa_programa');
add_action('wp_ajax_nopriv_importa_programa_xiptv', 'importa_programa');

function importa_programa()
{
    include_once IMPORTACIONS_PATH . 'class-importacio-xiptv.php';
    include_once(ABSPATH . 'wp-admin/includes/admin.php');

    $importacioXipTv = new importacioXipTv();
    $importacioXipTv->importa([$_POST['programa']]);
    
    exit();
}
