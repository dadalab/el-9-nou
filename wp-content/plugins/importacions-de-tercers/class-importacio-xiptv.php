<?php
class importacioXipTv {

	var $lastUpdated = 0;
	var $feed;
	var $importedCount = 0;

	public function getLastChecked($programa) {
		if ( $this->lastUpdated = get_option('last_xiptv_' . $programa . '_checked'))
			return $this->lastUpdated;

		add_option('last_xiptv_' . $programa . '_checked', '1999-12-12 00:00', '', 'no');
		return '1999-12-12 00:00';
	}

	public function importacioAcabada($programa) {
		update_option('last_xiptv_' . $programa . '_checked', current_time('Y-m-d H:i'));
		echo current_time('d/m/Y H:i') . ' - ' . $this->importedCount . ' vídeos importats.';
	}

	public function getFeed($feedUrl) {
		$feed = new SimpleXMLElement(file_get_contents($feedUrl));
		if ( ! is_wp_error( $feed ) ) :
	    	return $feed;
		endif;
	}

	public function itemshouldBeImported($item) {
		return strtotime($item->pubDate) >= strtotime($this->lastUpdated);
	}

	public function getFeedsToBeImported($feeds = null) {
		$query = null;
		if ($feeds) :
			$query = new WP_Query( [
				'post_type' => 'programa',
				'posts_per_page' => 9999,
				'post_name__in' => $feeds
			]);
		else: 
			$query = new WP_Query( [
				'post_type' => 'programa',
				'posts_per_page' => 9999
			]);
		endif;
		
		if ($query->have_posts()) :
			$data = [];
			while($query->have_posts()) : $query->the_post();
				if (get_field('feed_url')) :
					$data[] = [
						'feedUrl' => get_field('feed_url'),
						'programa' => get_the_ID(),
						'prefix' => basename(get_permalink())
					];
				endif;
			endwhile;
		endif;

		return $data;
	}

	public function importa($feedsSelected = null) {

		$feeds = $this->getFeedsToBeImported($feedsSelected);

		foreach($feeds as $feedData) :
			$last_updated = $this->getLastChecked($feedData['prefix']);
			$feed = $this->getFeed($feedData['feedUrl']);
			$this->importItems($feed, $feedData);
			$this->importacioAcabada($feedData['prefix']);
		endforeach;
		
	}

	public function importItems($feed, $feedData) {
	
		foreach($feed->channel->item as $item) : 
			if ( ! $this->itemshouldBeImported($item))
				return;
				
			$content = file_get_contents($item->link);
			$part1 = explode('data-src="', $content);
			$part2 = explode('"></scriptt></textarea>', $part1[1]);
			
			$post = [
				'post_title' => $item->title,
				'post_content' => $item->description->p,
				'post_status' => 'publish',
				'post_type' => 'video',
				'post_date' => date('Y-m-d H:i:s', strtotime($item->pubDate)),
				'post_author' => '128'
			];
			$this->importedCount++;
        	$postId = wp_insert_post($post);

        	$files = [];
        	$files['name'] =  $feedData['prefix'] . $postId . '-' . date('Y-m-d') . '.jpg';
        	$files['tmp_name'] = download_url((string)$item->enclosure[0]->attributes()->url);
        	//echo $files['tmp_name'];
        	$attachmentId = media_handle_sideload($files, $postId);
        	
        	set_post_thumbnail($postId, $attachmentId);
        	update_field('video_url', 'http://el9tv.alacarta.cat/embed/' . $part2[0], $postId);
        	update_field('programa', $feedData['programa'], $postId);
		endforeach;
	}
}