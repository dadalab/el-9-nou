<?php
/**
* Plugin Name: Exportacions de contingut
* Description: Plugin per exportar contingut personalitzat de la diferent informació
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

// define some constants
if (!defined('EXPORTACIONS_PATH')) {
    define('EXPORTACIONS_PATH', trailingslashit(plugin_dir_path(__FILE__)));
}

add_action('admin_menu', 'menu_exportacio');

function menu_exportacio()
{
    add_menu_page('Exportació Continguts', 'Exportació Continguts', 'administrator', __FILE__, 'exportacio_tv');
}

function exportacio_tv()
{
    ?>
<div class="wrap">
	<h2>Exportació Programació TV</h2>
	<div class="wp-filter"><div class="media-toolbar-secondary" style="padding: 20px;">
		De
		<?php $dies = ['diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte'];
		$dates = new DateTime(date('d-m-Y'));
		$diaSetmana = date('w');
		for ($i = 0; $i < 14; $i++) :
			$diesData[] = $dies[$diaSetmana];
			$diaSetmana++;
			if ($diaSetmana > 6) $diaSetmana = 0;
		endfor ?>
		<select id="des" class="attachment-filters"><?php
			foreach ($diesData as $dia) : ?>
				<option value="<?php echo $dia ?>"><?php echo ucfirst($dia) ?></option><?php
			endforeach; ?>
		</select>
		a
		<select id="fins" class="attachment-filters"><?php
			foreach ($diesData as $dia) : ?>
				<option value="<?php echo $dia ?>"><?php echo ucfirst($dia) ?></option><?php
			endforeach; ?>
		</select>

		<button type="button" id="exporta-tv" class="button media-button  select-mode-toggle-button" style="margin-top: 2px;">Exporta programació</button>
		<button type="button" id="exporta-xml" class="button media-button  select-mode-toggle-button" style="margin-top: 2px;">Exporta XML</button>
		<span class="spinner" id="exporta-loading" style="float: none;"></span>
		<table border='0'><tr><td width='500'>Indesign</td><td>XML</td></tr>
		<tr><td width='500'><textarea id="contingut" style="display: none;width: 500px;margin-top: 20px;height: 500px;"></textarea></td><td>
		<textarea id="contingut_xml" style="display: none;width: 500px;margin-top: 20px;height: 500px;"></textarea></td></tr></table>

</div>
<?php 
}

add_action('admin_head', 'export_programacioTV');
function export_programacioTV()
{
    ?>
<script type="text/javascript">
jQuery(document).ready(function($) {

    $('#exporta-tv').click(function(){

    	$('#exporta-loading').css('visibility', 'visible');
        var data = {
            action: 'exporta_programa',
            des : $('#des').val(),
    		fins : $('#fins').val(),
        };

        $.post(ajaxurl, data, function(response) {
            $('#exporta-loading').css('visibility', 'hidden');
            $('#contingut').val(response).css('display', 'block');
        });
    });
    $('#exporta-xml').click(function(){

    	$('#exporta-loading').css('visibility', 'visible');
        var data = {
            action: 'exporta_programa_xml',
            des : $('#des').val(),
    		fins : $('#fins').val(),
        };

        $.post(ajaxurl, data, function(response) {
            $('#exporta-loading').css('visibility', 'hidden');
            $('#contingut_xml').val(response).css('display', 'block');
        });
    });
});
</script>
<?php
}

add_action('wp_ajax_exporta_programa', 'exporta_programa');
add_action('wp_ajax_nopriv_exporta_programa', 'exporta_programa');

function exporta_programa()
{
    include_once EXPORTACIONS_PATH . 'class-exporta-programacio.php';
    include_once(ABSPATH . 'wp-admin/includes/admin.php');

    $exportaProgramacio = new ExportaProgramacio();
    $exportaProgramacio->exporta($_POST['des'], $_POST['fins']);
    
    exit();
}

add_action('wp_ajax_exporta_programa_xml', 'exporta_programa_xml');
add_action('wp_ajax_nopriv_exporta_programa_xml', 'exporta_programa_xml');

function exporta_programa_xml()
{
    include_once EXPORTACIONS_PATH . 'class-exporta-xml.php';
    include_once(ABSPATH . 'wp-admin/includes/admin.php');

    $exportaProgramacio_xml = new ExportaProgramacio_xml();
    $exportaProgramacio_xml->exporta_xml($_POST['des'], $_POST['fins']);
    
    exit();
}
