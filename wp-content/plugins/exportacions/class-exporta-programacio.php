<?php
class ExportaProgramacio {

	protected $dies = ['diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte'];
	protected $dataActual = "";

	public function exporta($des, $fins) {
		$nomDia = [];
		$nums = [];
		$diesOrdenats = array();
		$dia = array_search($des, $this->dies);
		
		$diaAvui = date('w');

 		$this->dataActual = $this->calculamDiaInicial($des);

		for ($i = 0; $i < 7; $i++) :
			$diesOrdenats[] = $this->dies[$dia];
			$dia++;
			if ($dia > 6) $dia = 0;
		endfor; 

		$count = 0;
		echo "<ASCII-WIN>\n";
		foreach($diesOrdenats as $dia) :
			$this->imprimeixDia($dia);

			if ($dia == $fins) return;
		endforeach;
	}

	private function calculamDiaInicial($des) {
		$diaInicial = array_search($des, $this->dies);
		$avui = date('w');
		$count = 0;
		while($diaInicial != $avui) {
			$count++; $avui++;
			if ($avui > 6) $avui = 0;
		}
		$date = new DateTime(date('Y-m-d'));
		return $date->modify("+$count day");

	}

	private function imprimeixDia($dia) {

		$avui = new WP_Query( array(
			'post_type' => 'programacio_tv',
			'name' => $dia
		));	
		while ($avui->have_posts()) : $avui->the_post();
			$info = get_field('panels_data');
			$numero_dia = $this->dataActual->format('d');
			if (substr($numero_dia,0,1)=="0") $numero_dia = substr($numero_dia,1,1);  ## TREU "0" DE L'ESQUERRA EN EL NOM DEL DIA PUBLICAT
			echo "<ParaStyle:Agenda-data>" . ucfirst($dia) . ' ' . $numero_dia;
			#echo "<ParaStyle:Agenda-data>" . ucfirst($dia) . ' ' . $this->dataActual->format('d');
			#echo "\n<ParaStyle:\, Diari-text><cSize:8.000000>";
			$primera_linia="<ParaStyle:\, Diari-text><cSize:8.000000>";  ## INSERTA AQUESTA ETIQUETA NOMÉS LA PRIMERA VEGADA DESPRES DE CADA DIA
			foreach($info['widgets'] as $programa) :
            	$this->imprimeixPrograma($programa,$primera_linia);
				$primera_linia="";  ## BUIDA EL CONTINGUT D'AQUESTA VARIABLE A PARTIR DE LA SEGONA LINIA DE CADA DIA
        	endforeach;	
        	echo "\n";							 
		endwhile;
		$this->dataActual->modify("+1 day");
	}

	private function imprimeixPrograma($programa,$primera_linia) {
		if ($programa['programa']) :
            $programaR = new WP_Query( array(
                'post_type' => 'programa',
                'p' => $programa['programa'],
                'posts_per_page' => 1
            ));
            while ( $programaR->have_posts() ) : $programaR->the_post();
                echo "\n" . $primera_linia . $programa['hora'] . " <ct:Bold>" . mb_strtoupper(get_the_title(), 'UTF-8') . ". <ct:>";
                if ($programa['subtitol'] != "") echo $programa['subtitol'] . '. ';
                if ($programa['detall'] != "") echo $programa['detall'] . '. ';
            endwhile;
        else :
        	echo "\n" . $primera_linia . $programa['hora'] . " <ct:Bold>" . mb_strtoupper($programa['titol'], 'UTF-8') . ". <ct:>";
        	if ($programa['subtitol'] != "") echo $programa['subtitol'] . '. ';
            if ($programa['detall'] != "") echo $programa['detall'] . '. ';
        endif;
	}
}