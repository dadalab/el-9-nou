<?php
class ExportaProgramacio_xml {

	protected $dies = ['diumenge', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte'];
	protected $dataActual = "";

	public function exporta_xml($des, $fins) {
		$nomDia = [];
		$nums = [];
		$diesOrdenats = array();
		$dia = array_search($des, $this->dies);
		
		$diaAvui = date('w');

 		$this->dataActual = $this->calculamDiaInicial($des);

		for ($i = 0; $i < 7; $i++) :
			$diesOrdenats[] = $this->dies[$dia];
			$dia++;
			if ($dia > 6) $dia = 0;
		endfor; 

		$count = 0;
		$xml_txt="";
		$xml_txt2="";
		foreach($diesOrdenats as $dia) :
			$xml_txt=$this->imprimeixDia($dia,$xml_txt);
			if ($dia == $fins) {
				$lin_dia = explode("\n",$xml_txt);
				$ordre=0;
				for ($lin_prog=0;$lin_prog<sizeof($lin_dia);$lin_prog++) {
					$lin_hora=explode(";",$lin_dia[$lin_prog]);
					if (substr($lin_hora[0],0,6)=="<data>") {$data_xml=substr($lin_hora[0],6,10);continue;}
					if (strlen($lin_hora[2])==0) continue;  ## no procesa linia en blanc
					$linia_data[$ordre]=$data_xml;
					$linia_hora[$ordre]=$lin_hora[1];
					if (strlen($linia_hora[$ordre])==4) $linia_hora[$ordre]="0".$linia_hora[$ordre];
					$linia_prog[$ordre]=$lin_hora[2];
					$linia_desc[$ordre]=$lin_hora[3];
					if (strlen($lin_hora[4])>0) $linia_desc[$ordre].=". ".$lin_hora[4];  ## AFEGEIX DETALL AL SUBTITOL SI EXISTEIX
					$linia_begi[$ordre]=substr($linia_data[$ordre],0,4).substr($linia_data[$ordre],5,2).substr($linia_data[$ordre],8,2).substr($linia_hora[$ordre],0,2).substr($linia_hora[$ordre],3,2)."00";
					$ordre++;
				}
				for ($ordre=0;$ordre < (sizeof($linia_data)-2);$ordre++) {
					$ordre2=$ordre+1;
					$any_1=substr($linia_data[$ordre],0,4);
					$any_2=substr($linia_data[$ordre2],0,4);
					$mes_1=substr($linia_data[$ordre],5,2);
					$mes_2=substr($linia_data[$ordre2],5,2);
					$dia_1=substr($linia_data[$ordre],8,2);
					$dia_2=substr($linia_data[$ordre2],8,2);
					$hor_1=substr($linia_hora[$ordre],0,2);
					$hor_2=substr($linia_hora[$ordre2],0,2);
					$min_1=substr($linia_hora[$ordre],3,2);
					$min_2=substr($linia_hora[$ordre2],3,2);
					if ($ordre==(sizeof($linia_data)-3)) {  ## ULTIMA LINIA
						$hor_2=substr($linia_hora[0],0,2);  ## DEFINIM PRIMERA HORA DEL DIA
						$min_2=substr($linia_hora[0],3,2);  ## DEFINIM PRIMER MINUT DEL DIA
					}
                    $linia_dura[$ordre]=date("H:i", mktime(($hor_2-$hor_1),($min_2-$min_1),0,($mes_2-$mes_1),($dia_2-$dia_1),($any_2-$any_1)));
                    $linia_segons[$ordre]=(substr($linia_dura[$ordre],0,2)*3600)+(substr($linia_dura[$ordre],3,2)*60);
                    $linia_end[$ordre]=date("YmdHis",mktime(($hor_1+substr($linia_dura[$ordre],0,2)),($min_1+substr($linia_dura[$ordre],3,2)),0,$mes_1,$dia_1,$any_1));
                    #echo $linia_begi[$ordre]."--".$linia_dura[$ordre]."--".$linia_segons[$ordre]."-------".$linia_hora[$ordre]."-------".$linia_prog[$ordre]."------->".$linia_desc[$ordre]."\n";
                    $xml_txt2.="<Event beginTime=\"".$linia_begi[$ordre]."\" duration=\"".$linia_segons[$ordre]."\">
<EpgProduction>
<EpgText language=\"spa\">
<Name>".$linia_prog[$ordre]."</Name>
<ShortDescription>".$linia_desc[$ordre]."</ShortDescription>
<Description>".$linia_prog[$ordre]."</Description>
</EpgText>
</EpgProduction>
</Event>\n";
				}
				$data_creacio=date("YmdHis");
				$xml_txt3="<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\" ?>
<BroadcastData creationDate=\"".$data_creacio."\">
<ProviderInfo>
<ProviderId>el 9 tv</ProviderId>
<ProviderName>EL 9 TV</ProviderName>
</ProviderInfo>
<ScheduleData>
<ChannelPeriod beginTime=\"".$linia_begi[0]."\" endTime=\"".$linia_end[$ordre-1]."\">
<ChannelId>el 9 tv</ChannelId>\n";
				$xml_txt4="</ChannelPeriod>
</ScheduleData>
</BroadcastData>\n";

				echo $xml_txt3.$xml_txt2.$xml_txt4;

				return;
			}
		endforeach;
	}

	private function calculamDiaInicial($des) {
		$diaInicial = array_search($des, $this->dies);
		$avui = date('w');
		$count = 0;
		while($diaInicial != $avui) {
			$count++; $avui++;
			if ($avui > 6) $avui = 0;
		}
		$date = new DateTime(date('Y-m-d'));
		return $date->modify("+$count day");

	}

	private function imprimeixDia($dia,$xml_txt) {
		$avui = new WP_Query( array(
			'post_type' => 'programacio_tv',
			'name' => $dia
		));	
		while ($avui->have_posts()) : $avui->the_post();
			$info = get_field('panels_data');
			$numero_dia = $this->dataActual->format('d');
			if (substr($numero_dia,0,1)=="0") $numero_dia = substr($numero_dia,1,1);  ## TREU "0" DE L'ESQUERRA EN EL NOM DEL DIA PUBLICAT
			$xml_txt.="<data>".$this->dataActual->format('Y-m-d')."</data>\n";
			#echo "<ParaStyle:Agenda-data>" . ucfirst($dia) . ' ' . $this->dataActual->format('d');
			#echo "\n<ParaStyle:\, Diari-text><cSize:8.000000>";
			$primera_linia="<ParaStyle:\, Diari-text><cSize:8.000000>";  ## INSERTA AQUESTA ETIQUETA NOMÉS LA PRIMERA VEGADA DESPRES DE CADA DIA
			foreach($info['widgets'] as $programa) :
            	$xml_txt=$this->imprimeixPrograma($programa,$primera_linia,$xml_txt);
				$primera_linia="";  ## BUIDA EL CONTINGUT D'AQUESTA VARIABLE A PARTIR DE LA SEGONA LINIA DE CADA DIA
        	endforeach;	
        	$xml_txt.="\n";							 
		endwhile;
		$this->dataActual->modify("+1 day");
		return $xml_txt;
	}

	private function imprimeixPrograma($programa,$primera_linia,$xml_txt) {
		if ($programa['programa']) :
            $programaR = new WP_Query( array(
                'post_type' => 'programa',
                'p' => $programa['programa'],
                'posts_per_page' => 1
            ));
            while ( $programaR->have_posts() ) : $programaR->the_post();
                $xml_txt.="<hora>;". $programa['hora'] . ";" . mb_strtoupper(get_the_title(), 'UTF-8') .";";
                if ($programa['subtitol'] != "") $xml_txt.=$programa['subtitol'] ;
                $xml_txt.=";";
                if ($programa['detall'] != "") $xml_txt.=$programa['detall'];
                $xml_txt.="\n";
            endwhile;
        else :
        	$xml_txt.="<hora>;". $programa['hora'].";". mb_strtoupper($programa['titol'], 'UTF-8') . ";";
        	if ($programa['subtitol'] != "") $xml_txt.=$programa['subtitol'] ;
        	$xml_txt.=";";
            if ($programa['detall'] != "") $xml_txt.=$programa['detall'] ;
            $xml_txt.="\n";
        endif;
        return $xml_txt;
	}
}