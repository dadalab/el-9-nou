<div id="PC-Overlay" class="overlay pc hidden-print">
    <div class="overlayInner">

        <button class="hamburger2 hamburger--squeeze js-hamburger is-active" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
        </button>

        <h1>Digues la teva</h1>
        <p>Fes-nos arribar suggeriments, notícies, esmenes...</p>

        <div id="PC-buttons" v-on:click="setTipus('esmenes')">
            <div class="PC-button">
                <i class="fa fa-envelope-o icon" aria-hidden="true"></i>
                <span>Esmenes</span>
            </div>
            <div class="PC-button" v-on:click="setTipus('fotografies')">
                <i class="fa fa-camera icon" aria-hidden="true"></i>
                <span>Fotografies</span>
            </div>
            <div class="PC-button" v-on:click="setTipus('cartes')">
                <i class="fa fa-paper-plane-o icon" aria-hidden="true"></i>
                <span>Cartes al director</span>
            </div>
            <div class="PC-button" v-on:click="setTipus('altres')">
                <i class="fa fa-rocket icon" aria-hidden="true"></i>
                <span>Altres</span>
            </div>
        </div>

        <form method="post" name="periodisme_ciutada" id="form_periodisme_ciutada" class="pc-form" v-on:submit.prevent="enviaFormulari" enctype="multipart/form-data">
            <input type="hidden" name="action" value="enviar_periodisme_ciutada">
            <input type="hidden" name="tipus" v-model="tipus">
            <input type="hidden" name="edicio" v-model="edicio">
            <input type="text" name="nom" v-model="nom" placeholder="Nom i cognoms" style="margin-top: 40px;">
            <input type="text" name="email" v-model="email" placeholder="E-mail">
            <textarea type="text" name="comentari" v-model="comentari" placeholder="Text" maxlength="775"></textarea>
            Puja un fitxer
            <input type="file" name="fitxer" v-model="fitxer" style="background:none">

            <p v-show="formSent">El formulari s'ha enviat correctament. Moltes gràcies.</p>

            <button type="submit" class="btn-send" :disabled="isLoading">
                Enviar <i class="fa fa-spinner fa-spin fa-3x fa-fw" v-bind:class="{ 'hide' : ! isLoading }"></i>
            </button>

        </form>
    </div>
</div>

<script>
    $('.PC-button').click( function() {
        $('.PC-button').removeClass('active');
        $(this).addClass('active');
        $('#form_periodisme_ciutada').slideDown();
    });

    new Vue({
        el: '#PC-Overlay',
        data: {
            isLoading: false,
            formSent: false,
            tipus: null,
            edicio: "<?php echo edicio_actual() ?>",
            nom : null,
            email : null,
            comentari : null,
            fitxer : null
        },
        methods: {
            setTipus: function(tipus) {
                this.tipus = tipus;
            },
            enviaFormulari: function() {
                this.isLoading = true;
                var formdata = new FormData();
                formdata.append('action', 'enviar_periodisme_ciutada');
                formdata.append('tipus', this.tipus);
                formdata.append('edicio', "<?php echo edicio_actual() ?>");
                formdata.append('nom', this.nom);
                formdata.append('email', this.email);
                formdata.append('comentari', this.comentari);
                formdata.append('fitxer', $('input[type=file]')[0].files[0]);

                that = this;
                jQuery.post({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        that.isLoading = false;
                        that.formSent = true;
                        that.nom = that.email = that.comentari = that.fitxer = "";
                    }
                });
            }
        },
    })
</script>
<!-- / Periodisme ciutadà -->