<?php
/**
* Plugin Name: Periodisme Ciutadà
* Description: Plugin per gestionar el periodisme ciutadà i les funcions per executar-lo.
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

add_action( 'wp_ajax_nopriv_enviar_periodisme_ciutada', 'envia_informacio' );
add_action( 'wp_ajax_enviar_periodisme_ciutada', 'envia_informacio' );

/**
 * Override the default upload path.
 **/
function wpse_141088_upload_dir( $dir ) {
    return array(
        'path'   => $dir['basedir'] . '/digues-la-teva',
        'url'    => $dir['baseurl'] . '/digues-la-teva',
        'subdir' => '/digues-la-teva',
    ) + $dir;
}

function getEmailPerTipusValles($tipus) {
	return ['direccio@gra.el9nou.com', 'noticies@gra.el9nou.com', 'bcodina@el9nou.com', 'jmolet@vic.el9nou.com'];

/**	if ($tipus == 'esmenes') :
		return ['arnau.jaumira@gmail.com', 'direccio@gra.el9nou.com'];
	elseif ($tipus == 'fotografies') :
		return ['arnau.jaumira@gmail.com', 'direccio@gra.el9nou.com'];
	elseif ($tipus == 'cartes') :
		return ['arnau.jaumira@gmail.com', 'direccio@vic.el9nou.com', 'direccio@gra.el9nou.com'];
	elseif ($tipus == 'altres') :
		return ['arnau.jaumira@gmail.com', 'direccio@gra.el9nou.com'];
	endif;**/
}
function getEmailPerTipus($tipus) {
	return ['vpalomar@el9tv.com', 'direccio@vic.el9nou.com', 'bcodina@el9nou.com', 'jmolet@vic.el9nou.com'];

	/**if ($tipus == 'esmenes') :
		return ['vpalomar@el9tv.com', 'arnau.jaumira@gmail.com', 'marc@dadacomunica.com'];
	elseif ($tipus == 'fotografies') :
		return ['vpalomar@el9tv.com', 'arnau.jaumira@gmail.com'];
	elseif ($tipus == 'cartes') :
		return ['vpalomar@el9tv.com', 'arnau.jaumira@gmail.com', 'direccio@vic.el9nou.com'];
	elseif ($tipus == 'altres') :
		return ['vpalomar@el9tv.com', 'arnau.jaumira@gmail.com'];
	endif;**/
}

function envia_informacio() {
	global $wpdb;
 	$upload_overrides = array( 'test_form' => false );

 	/** Canviem la URL per defecte on Wordpress puja els fitxer només per aquest cas. **/
	add_filter( 'upload_dir', 'wpse_141088_upload_dir' );
 	$file = wp_handle_upload($_FILES['fitxer'], $upload_overrides);
	remove_filter( 'upload_dir', 'wpse_141088_upload_dir' );
	$attachments = array( $file['url'] );
	$headers = 'From: ' . $_POST['nom'] . ' <' . $_POST['email'] . '>' . "\r\n";
	$headers .= 'Reply-To: ' . $_POST['nom'] . ' <' . $_POST['email'] . '>' . "\r\n";
	$missatge = "Nom: " . $_POST['nom'] . "\nEmail: " . $_POST['email'] . "\nComentari: " . $_POST['comentari'];

	$email = null;
	if ($_POST['edicio'] == 'valles-oriental') :
		$email = getEmailPerTipusValles($_POST['tipus']);
	else :
		$email = getEmailPerTipus($_POST['tipus']);
	endif;
	
	wp_mail( $email, 'Digues la teva', $missatge, $headers, $file['file'] );
	wp_mail( 'marc@dadacomunica.com', 'Digues la teva', $missatge, $headers, $file['file'] );

	wp_die();
	die();
}

function p_periodisme_ciutada_form() {
	include('templates/form.php');
	if ($_GET['digues-la-teva']) : ?>
		<script>
			jQuery( document ).ready( function() {
				$('.periodisme-ciutada').click();
			});
		</script>
	<?php endif;
}
add_action( 'wp_footer', 'p_periodisme_ciutada_form' );

function p_periodisme_ciutada_button() {
	include('templates/button.php');
}