<?php
/**
* Plugin Name: Custom taxonomies for news
* Description: Plugin que permet registrar les categories i classificacions extres de les notícies
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/


function registrar_edicio_taxonomy() {
	register_taxonomy('edicio', ['post', 'hemeroteca', 'esqueles'], array(
			'label' 		=> __( 'Edició' ),
			'rewrite' 		=> array('slug' => 'edicio' ),
			'hierarchical'  => true,
		)
	);
}
add_action( 'init', 'registrar_edicio_taxonomy' );

function registrar_poblacio_taxonomy() {
	register_taxonomy('poblacio', 'post', array(
			'label' 		=> __( 'Població' ),
			'rewrite' 		=> array('slug' => 'poblacio' ),
		)
	);
}
add_action( 'init', 'registrar_poblacio_taxonomy' );