<?php
/**
* Plugin Name: Customize the comments form
* Description: Millora de la secció comentaris a les notícies
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

function disable_comment_url($fields) { 
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','disable_comment_url');

add_filter( 'comment_form_fields', 'move_comment_field' );
function move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

/* Add Placehoder in comment Form Fields (Name, Email, Website) */

add_filter( 'comment_form_default_fields', 'help4cms_comment_placeholders' );
function help4cms_comment_placeholders( $fields )
{
    $fields['author'] = str_replace(
        '<input',
        '<input placeholder="Nom i cognoms"',
        $fields['author']
    );
    $fields['email'] = str_replace(
        '<input',
        '<input placeholder="E-mail"',
        $fields['email']
    );

    return $fields;
}

/* Add Placehoder in comment Form Field (Comment) */
add_filter( 'comment_form_defaults', 'help4cms_textarea_placeholder' );

function help4cms_textarea_placeholder( $fields )
{
    $fields['comment_field'] = str_replace(
        '<textarea',
        '<textarea placeholder="Comentari"',
        $fields['comment_field']
    );
    return $fields;
}

function theme_queue_js(){
if ( is_singular() )
  wp_enqueue_script( 'comment-reply' );
}
add_action('wp_print_scripts', 'theme_queue_js');

function custom_comment_template($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<!-- comment -->
<div class="row">
    <div class="comment clearfix">
        <div class="col-md-12"><hr></div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <i class="icon-nickname"></i>
            <div class="nickname">
                <span><?php echo get_comment_author() ?></span><br>
                <?php echo get_comment_date() ?> | <?php echo get_comment_time() ?>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
        	<?php if ($comment->comment_approved == '0') : ?>
				<span class="error">El teu comentari està pendent de moderació.</span>
			<?php endif; ?>
            <?php comment_text() ?>
            <ul class="actions">
                <!--<li><a class='comment-reply-link'>Respon</a></li>-->
                <!--<li><a href="#">M'agrada</a></li> -->
            </ul>                                               
        </div>
    </div>
</div><!-- / comment -->

<?php } ?>