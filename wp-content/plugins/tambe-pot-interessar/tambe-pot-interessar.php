<?php
/**
* Plugin Name: També et pot interessar...
* Description: Funció per mostrar notícies relacionades a una notícia
* Version: 1.0
* Author: Dadàcomunica
* Author URI: dadacomunica.com
*/

function the_related_posts() {

    $categories = get_the_category();

    $parentCategory = 0;
    if ($categories) :
        foreach($categories as $category) :
            if ($category->parent != 0) continue;
            $parentCategory = $category->term_id;
        endforeach;
    endif;

    if ( $relacionades_ids = get_field('relacionades') ) : ?>
        <?php $relacionades = new WP_Query( array('post__in' => $relacionades_ids, 'post__not_in' => get_option( 'sticky_posts') ) ) ?>
        <section class="articles-petits">
            <h3>Notícies relacionades</h3><?php
            while ($relacionades->have_posts()) : $relacionades->the_post(); 
                get_template_part('templates/noticies/single', 'relacionades');
            endwhile; ?>
        </section><?php
    else :
        $relacionades_ids = array();
    endif;
    wp_reset_query();

    // Afegim a l'array relacionades, el propi ID perquè no es vegi a 'També et pot interessar'.
    if (  count($relacionades_ids)) $relacionades_ids = implode(',', $relacionades_ids) . ', ' . get_the_ID();
    else $relacionades_ids = get_the_ID();

    $args = array(
        'posts__not_in'          => $relacionades_ids,
        'post_type'             => 'post',
        'posts_per_page'        => 12,
        'ignore_sticky_posts'   => 1,
        'tax_query' => edicio_query_per_llistats(),
    );
    $tambe = new WP_Query($args);
    $ids = [];
    if ( $tambe->have_posts() ) :        
        while ($tambe->have_posts() && $count < 3) : $tambe->the_post();
            array_push($ids, get_the_ID());
        endwhile;
    endif;
    $args = array(
        'post__in'          => $ids,
        'posts_per_page'        => 3,
        'orderby'                 => 'rand',
    );
    $tambe = new WP_Query($args);
    if ( $tambe->have_posts() ) : ?>
        <section class="articles-petits">
            <h3>També et pot interessar</h3><?php
            $count = 0;
            while ($tambe->have_posts() && $count < 3) : $tambe->the_post();
                get_template_part('templates/noticies/single', 'tambe-interessa');
                $count++;
            endwhile; ?>
        </section><?php
    endif;
    wp_reset_query();
}