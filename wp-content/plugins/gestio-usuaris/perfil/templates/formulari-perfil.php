<?php $currentUser = $current_user = wp_get_current_user(); ?>

<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
    <div>
    <input type="text" name="r_name" value="<?php echo $currentUser->user_firstname ?>" placeholder="Nom*">
    <input type="text" name="r_email" value="<?php echo $currentUser->user_email ?>" placeholder="Email*">
    <input type="text" name="r_alies" value="<?php echo get_user_meta($currentUser->ID, 'nickname', true) ?>" placeholder="Àlies">
    <input type="password" name="r_password" value="" placeholder="Contrasenya*">
    <input type="password" name="r_password_confirm" value="" placeholder="Repeteix la contrasenya*">
    <div class="row">
	    <div class="col-md-4">
			<label><input type="radio" name="r_genere" value="dona" <?php if (get_user_meta($currentUser->ID, 'genere', true) == 'dona') : ?>checked<?php endif ?>>Dona</label>
			<label style="margin-left:25px"><input type="radio" name="r_genere" value="home" <?php if (get_user_meta($currentUser->ID, 'genere', true) == 'home') : ?>checked<?php endif ?>>Home</label>
	    </div>
	    <div class="col-md-4">
			<input type="text" name="r_data" placeholder="dd/mm/aaaa" value="<?php echo get_user_meta($currentUser->ID, 'data', true);  ?>">
		</div>
		<div class="col-md-4">
			<input type="text" name="r_cp" placeholder="Codi postal" value="<?php echo get_user_meta($currentUser->ID, 'cp', true);  ?>">
		</div>
	</div>

	<input type="hidden" name="submitted" value="1">
    <button type="submit">Actualitzar perfil</button>
    </form>
    <hr>