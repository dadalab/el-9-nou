<?php

function profile_form($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica)
{
    require_once('templates/formulari-perfil.php');
}

function profile_validation($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica)
{
    global $reg_errors;
    $reg_errors = new WP_Error;
    if (empty($name) || empty($email)) {
        $reg_errors->add('field', 'Has d\'omplir tots els camps obligatoris.');
    }
    
    if (5 > strlen($password)) {
        $reg_errors->add('password', 'La contrasenya ha de tenir més de 5 caràcters.');
    }
    if (!is_email($email)) {
        $reg_errors->add('email_invalid', 'El correu electrònic no sembla ser vàlid.');
    }

    if (is_wp_error($reg_errors)) {
        foreach ($reg_errors->get_error_messages() as $error) { ?>
            <div><strong style="color:red">ERROR</strong>: <?php echo $error ?><br/></div><?php
        }
    }
}

function profile_complete()
{
    global $reg_errors, $name, $email, $alies, $password, $password_confirm, $genere, $data, $cp;
    if (1 > count($reg_errors->get_error_messages())) {
        $userdata = array(
        'user_login'    =>   $email,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'first_name'    =>   $name,
        //'last_name'     =>   $last_name,
        'nickname'      =>   $alies,
        );
        
        update_user_meta( $user, 'genere', $genere );
        update_user_meta( $user, 'data', $data );
        update_user_meta( $user, 'cp', $cp );
        return true;
    }
    return false;
}

function custom_profile_function()
{
    $donatAlta = false;
    global $name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica;
    if (isset($_POST['submitted'])) {
        profile_validation(
        $_POST['r_name'],
        $_POST['r_email'],
        $_POST['r_alies'],
        $_POST['r_password'],
        $_POST['r_password_confirm'],
        $_POST['r_genere'],
        $_POST['r_data'],
        $_POST['r_cp'],
        $_POST['r_politica']
        );

        $name               =   sanitize_user($_POST['r_name']);
        $email              =   sanitize_email($_POST['r_email']);
        $alies              =   esc_attr($_POST['r_alies']);
        $password           =   sanitize_text_field($_POST['r_password']);
        $password_confirm   =   sanitize_text_field($_POST['r_password_confirm']);
        $genere             =   sanitize_text_field($_POST['r_genere']);
        $data               =   sanitize_text_field($_POST['r_data']);
        $cp                 =   esc_textarea($_POST['r_cp']);
        $politica           =  $_POST['r_politica'];

        // call @function profile_complete to create the user
        // only when no WP_error is found
        $donatAlta = profile_complete($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica);
    }
    if (! $donatAlta)
        profile_form($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica);
}

add_shortcode('ru_profile_form', 'custom_profile_shortcode');
 
function custom_profile_shortcode()
{
    ob_start();
    custom_profile_function();
    return ob_get_clean();
}
