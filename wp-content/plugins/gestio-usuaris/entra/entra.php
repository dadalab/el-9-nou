<?php 

global $reg_errors;
$reg_errors = new WP_Error;
function check_login()
{
    global $l_email, $l_password, $reg_errors;

    if (isset($_POST['submitted_login'])) {
        $l_email = sanitize_email($_POST['r_email']);
        $l_password = sanitize_text_field($_POST['r_password']);

        if (empty($l_email) || empty($l_password)) {
            $reg_errors->add('field', 'Has d\'omplir tots els camps.');
        }

        $user = get_user_by('email', $l_email);

        if (! $user) {
            $reg_errors->add('field', 'Aquest usuari no existeix');
        } elseif (! get_user_meta((int) $user->id, "wcemailverified", true)) {
            $link = add_query_arg(array("reenvia_codi" => base64_encode($l_email)));
            $reg_errors->add('field', 'El correu no està validat. <a href="' . $link . '">Reenvia codi de confirmació</a>');
        } elseif (! wp_check_password($l_password, $user->user_pass, $user->ID)) {
            $reg_errors->add('field', 'La contrasenya és incorrecta');
        }
        
        if (! count($reg_errors->get_error_messages())) {
            $success = wp_signon(['user_login' => $l_email, 'user_password' => $l_password, 'remember' => true], false);
            wp_set_current_user($success->ID);
            if (isset($_GET['r'])) {
                wp_redirect($_GET['r']);
                exit;
            }
            wp_redirect(site_url() . '/perfil');
            exit;
        }
    }
}
add_action('init', 'check_login');

function custom_login_form()
{
    global $reg_errors;

    if (count($reg_errors->get_error_messages())) {
        foreach ($reg_errors->get_error_messages() as $error) {
            echo '<div class="error-form">';
            echo '<strong style="color:red">ERROR</strong>: ';
            echo $error . '<br/>';
            echo '</div>';
        }
    }
    echo $_SESSION['login_missatge'];
    //$valorEmail = 
    ?>
    
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
		<input type="text" name="r_email" value="<?php (isset($_POST['r_email']) ? $_POST['r_email'] : null) ?>" placeholder="Email">
    	<input type="password" name="r_password" value="<?php (isset($_POST['r_password']) ? $_POST['r_password'] : null) ?>" placeholder="Contrasenya">
		<input type="hidden" name="submitted_login" value="1">
        <!--<div class="remember">Has oblidat la <a href="#">contrasenya?</a></div>-->
    	<button type="submit">Accedir</button>
        <hr>
    </form>   
    <?php
}