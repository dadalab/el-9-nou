<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
    <div>
    <input type="text" name="r_name" value="<?php echo ( isset($_POST['r_name']) ) ? $name : null ?>" placeholder="Nom*">
    <input type="text" name="r_email" value="<?php echo ( isset($_POST['r_email']) ) ? $email : null ?>" placeholder="Email*">
    <input type="text" name="r_alies" value="<?php echo ( isset($_POST['r_alies']) ) ? $alies : null ?>" placeholder="Àlies">
    <input type="password" name="r_password" value="<?php echo ( isset($_POST['r_password']) ) ? $password : null ?>" placeholder="Contrasenya*">
    <input type="password" name="r_password_confirm" value="<?php echo ( isset($_POST['r_password_confirm']) ) ? $password_confirm : null ?>" placeholder="Repeteix la contrasenya*">
    <div class="row">
	    <div class="col-md-4">
			<label><input type="radio" name="r_genere" value="dona" <?php if ($genere == 'dona') : ?>checked<?php endif ?>>Dona</label>
			<label style="margin-left:25px"><input type="radio" name="r_genere" value="home" <?php if ($genere == 'home') : ?>checked<?php endif ?>>Home</label>
	    </div>
	    <div class="col-md-4">
			<input type="text" name="r_data" placeholder="dd/mm/aaaa" value="<?php echo isset( $_POST['r_data'] ) ? $data : null ?>">
		</div>
		<div class="col-md-4">
			<input type="text" name="r_cp" placeholder="Codi postal" value="<?php echo isset( $_POST['r_cp'] ) ? $cp : null ?>">
		</div>
	</div>

	<p><label><input type="checkbox" name="r_politica" value="1" <?php if ($politica) : ?>checked<?php endif ?>></input> Accepto les <a href="<?php echo site_url() ?>/grup/avis-legal">condicions d'ús</a> del servei i la <a href="<?php echo site_url() ?>/grup/politica-de-privacitat">política de privacitat</a></label></p>
	<input type="hidden" name="submitted" value="1">
    <button type="submit">Registra't</button>
    </form>
    <hr>
	<div class="accedir">Ja tens un compte? <a href="<?php echo site_url() ?>/entra">Entra aquí</a></div>