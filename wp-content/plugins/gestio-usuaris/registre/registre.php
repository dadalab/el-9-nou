<?php

require_once('verificar-usuari.php');

function registration_form($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica)
{
    require_once('templates/formulari-registre.php');
}

function registration_validation($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica)
{
    global $reg_errors;
    $reg_errors = new WP_Error;
    if (empty($name) || empty($email) || empty($password) || empty($password_confirm)) {
        $reg_errors->add('field', 'Has d\'omplir tots els camps obligatoris.');
    }
    if ($alies && username_exists($alies)) {
        $reg_errors->add('user_name', 'Aquest àlies ja està en ús.');
    }
    if (5 > strlen($password)) {
        $reg_errors->add('password', 'La contrasenya ha de tenir més de 5 caràcters.');
    }
    if (!is_email($email)) {
        $reg_errors->add('email_invalid', 'El correu electrònic no sembla ser vàlid.');
    }
    if (email_exists($email)) {
        $reg_errors->add('email', 'Aquest correu electrònic ja està registrat.');
    }

    if (! $politica) {
        $reg_errors->add('politica', 'Has d\'acceptar la política de privacitat i les condicions d\'ús.');
    }

    if (is_wp_error($reg_errors)) {
        foreach ($reg_errors->get_error_messages() as $error) { ?>
            <div><strong style="color:red">ERROR</strong>: <?php echo $error ?><br/></div><?php
        }
    }
}

function complete_registration()
{
    global $reg_errors, $name, $email, $alies, $password, $password_confirm, $genere, $data, $cp;
    if (1 > count($reg_errors->get_error_messages())) {
        $userdata = array(
        'user_login'    =>   $email,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'first_name'    =>   $name,
        'nickname'      =>   $alies,
        );
        
        $user = wp_insert_user($userdata);
        update_user_meta( $user, 'genere', $genere );
        update_user_meta( $user, 'data', $data );
        update_user_meta( $user, 'cp', $cp );
        wp_new_user_notification($user);
        echo '<div class="well missatge-registrat">
                T\'hem enviat un correu per completar l\'alta.<br>Quan confirmis l\'adreça, podràs entrar <a href="' . site_url() . '/entra">aquí.</a>
            </div>';
        return true;
    }
    return false;
}

function custom_registration_function()
{
    $donatAlta = false;
    global $name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica;
    if (isset($_POST['submitted'])) {
        registration_validation(
        $_POST['r_name'],
        $_POST['r_email'],
        $_POST['r_alies'],
        $_POST['r_password'],
        $_POST['r_password_confirm'],
        $_POST['r_genere'],
        $_POST['r_data'],
        $_POST['r_cp'],
        $_POST['r_politica']
        );

        $name               =   sanitize_user($_POST['r_name']);
        $email              =   sanitize_email($_POST['r_email']);
        $alies              =   esc_attr($_POST['r_alies']);
        $password           =   sanitize_text_field($_POST['r_password']);
        $password_confirm   =   sanitize_text_field($_POST['r_password_confirm']);
        $genere             =   sanitize_text_field($_POST['r_genere']);
        $data               =   sanitize_text_field($_POST['r_data']);
        $cp                 =   esc_textarea($_POST['r_cp']);
        $politica           =  $_POST['r_politica'];

        // call @function complete_registration to create the user
        // only when no WP_error is found
        $donatAlta = complete_registration($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica);
    }
    if (! $donatAlta)
        registration_form($name, $email, $alies, $password, $password_confirm, $genere, $data, $cp, $politica);
}

add_shortcode('ru_custom_form', 'custom_registration_shortcode');
 
function custom_registration_shortcode()
{
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
