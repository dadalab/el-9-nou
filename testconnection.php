<?php
$cwd = getcwd();
$path = substr($cwd, 0, strpos($cwd, 'wp-content/'));

require $path . 'wp-blog-header.php'; ?>

<?php  if (is_user_logged_in()) :
    $current_user = wp_get_current_user(); ?>
    <li><a href="<?php echo site_url() ?>/perfil">Hola <?php echo $current_user->user_firstname ?>!</a></li>
    <li><a href="<?php echo wp_logout_url( site_url() ); ?>">Sortir</a></li>
<?php else : ?>
    <li><a href="<?php echo site_url() ?>/registre">Registra't</a></li>
    <li><a href="<?php echo site_url() ?>/entra">Inicia sessió</a></li>
<?php endif ?>